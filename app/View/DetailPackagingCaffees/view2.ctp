<div class="detailPackagingCaffees view">
<?php 
	  echo $this->Html->script('jquery.min');
	  echo $this->Form->create('WeighingPackagingCaffee', array('url' => 'savePackagingPallet2')); ?>
    <fieldset>
        <legend><?php echo __('Paletizar Remesa'); ?></legend>
	<?php
		echo $this->Form->input('remittances_cafee_id',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'],'type' => 'hidden'));
		echo $this->Form->input('remesa',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'], 'disabled' => 'disabled'));
		echo $this->Form->input('id_oie',array('type' => 'hidden','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']));
                echo $this->Form->input('oie',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'disabled' => 'disabled'));
                echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
                echo $this->Form->input('weight',array( 'label' => 'Peso','type' => 'number', 'disabled' => 'disabled','required' => true) );
                echo $this->Form->input('weight_pallet',array( 'type' => 'hidden') );
                echo $this->Form->input('bags',array( 'label' => 'Cantidad de sacos','type' => 'number', 'max' => 30 ,'min' => 1 ,'required' => true,'default'=>'0') );
                echo $this->Form->input('quantity_bag_pallet',array( 'type' => 'hidden') );
	?>  
<!--        <div class="row actions">
            //<?php 
//                echo $this->Html->link(__('Sumar'),'javascript:void(0)',array('onclick'=> 'addBag();' ) );
//                echo $this->Html->link(__('Restar'),'javascript:void(0)',array('onclick'=> 'removeBag();') );
//             ?>
        </div>-->
        
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('sequence','Secuencia'); ?></th>
                    <th><?php echo $this->Paginator->sort('weigth','Peso'); ?></th>
                    <th><?php echo $this->Paginator->sort('sack','Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('date','Fecha'); ?></th>
                </tr>
            </thead>
            <tbody>
		<?php 
		$quantityPallet = 0;
		$quantityBags = 0;
		foreach ($weighingPackagingCaffee as $weighingCaffee): 
			$quantityPallet = $quantityPallet +1;?>
                <tr>
                    <td><?php echo h($weighingCaffee['WeighingPackagingCaffee']['seq_weight_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingCaffee['WeighingPackagingCaffee']['weight_pallet']); ?>&nbsp;</td>
                    <td><?php 
			$quantityBags = $quantityBags + $weighingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet'];
			echo h($weighingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingCaffee['WeighingPackagingCaffee']['weighing_date']); ?>&nbsp;</td>
                </tr>

		<?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->Form->end(__('Submit')); ?>
	<?php
                echo $this->Form->input('bags_out',array('label' => 'Sacos Rad Out','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'], 'disabled' => 'disabled'));
		echo $this->Form->input('quantity_pallet',array('label' => 'Pallets egresados', 'disabled' => 'disabled', 'value' => $quantityPallet));
		echo $this->Form->input('quantity_bags',array( 'type' => 'hidden', 'value' => $quantityBags ) );
                echo $this->Form->input('total_bags',array( 'label' => 'Sacos egresados','type' => 'number', 'disabled' => 'disabled','value' => $quantityBags ) );
		$companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
		echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
	?>
        <script type="text/javascript">
            var bagWeigth = <?php echo $bagWeigth['UnitsCaffee']['quantity'];?>;
            $("#WeighingPackagingCaffeeBags").keyup(function () {
                var numBags = $(this).val();
                $("#WeighingPackagingCaffeeQuantityBagPallet").val(numBags);
                setValueWeightPallet(numBags);
            });

            function setValueWeightPallet(numBags){
                if (numBags !== 0) {
                    var weightPallet = Math.floor(((numBags)*bagWeigth)+90);
                    //console.log(weightPallet);
                    $("#WeighingPackagingCaffeeWeightPallet").val(weightPallet);
                    $("#WeighingPackagingCaffeeWeight").val(weightPallet);
                }
            }
        </script> 
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Listado Remesas'), array('action' => 'packaging_processing')); ?></li>
    </ul>
</div>