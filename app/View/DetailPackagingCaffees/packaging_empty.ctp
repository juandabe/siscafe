<div class="detailPackagingCaffees index">
    <h2><?php echo __('Listado vaciados de contenedores en proceso - (Retorno de Café a Bodega)'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('Ubic Café'); ?></th>
                <th><?php echo $this->Paginator->sort('Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('Contenedor'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos In'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos Rad Out'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos Out'); ?></th>
                <th><?php echo $this->Paginator->sort('Tara In'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($detailPackagingCaffees as $detailPackagingCaffee): ?>
            <tr>
                <td><?php 
                //debug($detailPackagingCaffee);exit;
                echo h($detailPackagingCaffee['RemittancesCaffee']['id']);?></td>
                <td><?php echo ($detailPackagingCaffee['RemittancesCaffee']['slot_store_id'] != null) ? h($detailPackagingCaffee['RemittancesCaffee']['SlotStore']['name_space']):"S/U"; ?></td>
                <td style="background-color: #a9b1ef;"><span style="font-size: 15px ;color: black;"><?php echo h('3-'.$detailPackagingCaffee['RemittancesCaffee']['Client']['exporter_code'].'-'.$detailPackagingCaffee['RemittancesCaffee']['lot_caffee']);?></span></td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['id']); ?></td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['ReadyContainer']['bic_container']); ?></td>
                <td style="background-color: #1e39ff;"><span style="font-size: 15px ;color: black;"><?php echo h($detailPackagingCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</span></td>
                <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']); ?>&nbsp;</td>
                <td style="background-color: #ffba1e;"><span style="font-size: 15px ;color: black;"><?php echo h($detailPackagingCaffee['RemittancesCaffee']['quantity_bag_out_store']); ?>&nbsp;</span></td>
                <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['tara_packaging']); ?>&nbsp;</td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td class="actions">
			<?php 
                            echo $this->Html->link(__('Paletizar'), array('action' => 'view3', '?' => array('OIE' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'remesa' => $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'])));
                            echo $this->Html->link(__('Tara'), array('controller'=>'RemittancesCaffees','action' => 'tare2', '?'=> array('oie' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'remesa' => $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'])));
                            echo $this->Html->link(__('Finalizar'), array('action' => 'packaging_empty', '?' => array('OIE' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'remesa' => $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'])),array('confirm'=>('Desea finalizar proceso de retorno de la remesa '.$detailPackagingCaffee['RemittancesCaffee']['id'].'?'))); 
                            ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=> 'Pages','action' => 'basculero')); ?></li>
        <li><?php echo $this->Html->link(__('Salir'), array('controller' => 'users', 'action' => 'logout',$this->Session->read('User.id'))); ?> </li>
    </ul>
</div>
