<div class="detailPackagingCaffees form">
<?php echo $this->Html->script('jquery.min');
	  echo $this->Form->create('DetailPackagingCaffee', array('url' => 'savePackagingTare')); ?>
    <fieldset> 
        <legend><?php echo __('Ingresar Tara Embalaje'); ?></legend>
	<?php
            echo $this->Form->input('remittances_caffee_id',array('type' => 'hidden','value'=>$this->request->data['RemittancesCaffeeId']));
            echo $this->Form->input('remesa',array('value'=>$this->request->data['RemittancesCaffeeId'], 'disabled' => 'disabled'));
            echo $this->Form->input('packaging_caffee_id',array('type' => 'hidden','value'=>$this->request->data['OIE']));
            echo $this->Form->input('oie',array('value'=>$this->request->data['OIE'], 'disabled' => 'disabled'));
            echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'Seleccione...', 'required' => true,'options' => array('http://192.168.35.168:8080/SISCAFE-BasculeWS/WS/Weight' => 'Bascula Entrada Bodega 1', 'http://192.168.35.169:8080/SISCAFE-BasculeWS/WS/Weight' => 'Bascula Entrada Bodega 2')));
            echo $this->Form->input('weigth',array( 'label' => 'Peso','type' => 'number' , 'required' => true,'disabled' => 'disabled') );
            echo $this->Form->input('tara_packaging',array( 'type' => 'hidden') );
            echo $this->Form->input('pallets',array( 'label' => 'Cantidad de estibas','type' => 'number', 'max' => 15 , 'required' => true,'disabled' => 'disabled','default'=>'0') );    
            echo $this->Form->input('pallet_tare_download',array( 'type' => 'hidden') );    
	?>
        <div class="row actions">
            <?php 
                echo $this->Html->link(__('Agregar'),'javascript:void(0)',array('onclick'=> 'addPallet();', 'action' => '#'  ) );
                echo $this->Html->link(__('Quitar'),'javascript:void(0)',array('onclick'=> 'removePallet();') );
             ?>
        </div>
    </fieldset>
    <script type="text/javascript">
        
        var weigthPallet = 0;
        
        function addPallet() {
            console.log("sumar");
            var pallet = parseInt($("#DetailPackagingCaffeePallets").val());
            $("#DetailPackagingCaffeePallets").val(pallet + 1);
            $("#DetailPackagingCaffeePalletTareDownload").val(pallet + 1);
        }

        function removePallet() {
            console.log("restar");
            var pallet = parseInt($("#DetailPackagingCaffeePallets").val());
            $("#DetailPackagingCaffeePallets").val(pallet - 1);
            $("#DetailPackagingCaffeePalletTareDownload").val(pallet - 1);
        }

        $("#DetailPackagingCaffeeBascule").change(function () {
            setInterval(function () {
                $.ajax({
                    url: $("#DetailPackagingCaffeeBascule").val(),
                    crossDomain: true,
                    success: function (response) {
                        $("#DetailPackagingCaffeeWeigth").val(parseInt(response));
                        $("#DetailPackagingCaffeeTaraPackaging").val(parseInt(response));
                        setValueQuantityPallets(parseInt(response));
                    }
                });
            }, 500); //medio segundo
        });

        function setValueQuantityPallets(weigthBascule) {
            if (weigthBascule !== weigthPallet) {
                weigthPallet = weigthBascule;
                $("#DetailPackagingCaffeePallets").val(Math.floor(weigthPallet / 70));
                $("#DetailPackagingCaffeePalletTareDownload").val(Math.floor(weigthPallet / 70));
            }
        }

    </script>    
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Listado Remesas'), array('action' => 'packaging_processing')); ?></li>
    </ul>
</div>