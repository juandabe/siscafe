<div class="viewPackagings index">
	<h2><?php echo __('Resultado de busqueda'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('oie'); ?></th>
			<th><?php echo $this->Paginator->sort('bic_ctn'); ?></th>
			<th><?php echo $this->Paginator->sort('estado_embalaje'); ?></th>
                        <th><?php echo $this->Paginator->sort('lotes'); ?></th>
			<th><?php echo $this->Paginator->sort('exportador'); ?></th>
			<th><?php echo $this->Paginator->sort('total_sacos'); ?></th>
			<th><?php echo $this->Paginator->sort('packaging_mode'); ?></th>
			<th><?php echo $this->Paginator->sort('packaging_date'); ?></th>
			<th><?php echo $this->Paginator->sort('iso_ctn'); ?></th>
			<th><?php echo $this->Paginator->sort('total_cafe_empaque'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($viewPackagings as $viewPackaging): ?>
	<tr>
		<td><?php echo h($viewPackaging['ViewPackaging']['oie']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['bic_ctn']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['estado_embalaje']); ?>&nbsp;</td>
                <td><?php echo h($viewPackaging['ViewPackaging']['lotes']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['exportador']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['total_sacos']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['packaging_mode']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['packaging_date']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['iso_ctn']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging['ViewPackaging']['total_cafe_empaque']); ?>&nbsp;</td>

		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $viewPackaging['ViewPackaging']['oie']),array('target'=>'_blank')); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo ($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) ? $this->Html->link(__('Bajar a excel'), array('action' => 'download','?' => array('dateIni'=>$dateIni,'dateFin'=>$dateFin))):""; ?></li>
                <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Buscar CTN'), array('controller' => 'PackagingCaffees', 'action' => 'index')); ?></li></li>
                <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') == 6) echo $this->Html->link(__('Buscar CTN'), array('controller' => 'PackagingCaffees', 'action' => 'all')); ?></li></li>
	</ul>
</div>
