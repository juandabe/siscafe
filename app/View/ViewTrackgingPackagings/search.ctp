<div class="coffeeSamples index">
	<h2><?php echo ('Busqueda de Conts (Lote, CargoLot) - Trazabilidad Embalaje') ?></h2>
        <table>
            <tr>
                <td><?php echo $this->Form->input('lotCoffee',array('label' => 'Expo-Lote Ej.: 001-02235', 'type'=>'text')); ?></td>
                <td style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="findOieByLot();">Buscar...</a></h3></td>
                <td><?php echo $this->Form->input('user_profile',array('type'=>'hidden','value'=>$this->Session->read('User.profiles_id'))); ?></td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
			<th><?php echo $this->Paginator->sort('oie'); ?></th>
			<th><?php echo $this->Paginator->sort('state_packaging'); ?></th>
			<th><?php echo $this->Paginator->sort('date_oie'); ?></th>
			<th><?php echo $this->Paginator->sort('iso'); ?></th>
			<th><?php echo $this->Paginator->sort('cta_coffee'); ?></th>
			<th><?php echo $this->Paginator->sort('lotes'); ?></th>
			<th><?php echo $this->Paginator->sort('cargolot'); ?></th>
			<th><?php echo $this->Paginator->sort('remesas'); ?></th>
			<th><?php echo $this->Paginator->sort('seal_1'); ?></th>
			<th><?php echo $this->Paginator->sort('seal_2'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
       </table>
        
	<p>
</div>
<script type="text/javascript">
    
    function findOieByLot() {
        var lotCoffee = $("#lotCoffee").val();
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/ViewTrackgingPackagings/findUnits/" + lotCoffee,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var packagingUnits = JSON.parse(data);
                        //console.log(packagingUnits);
                        addRemittanceDataToTable(packagingUnits);
                    }
                    else{
                        alert("No se encontro NINGUN registro");
                        $('#tableRemittances').children().remove();
                    }
                }
            });
    }
    
    function addRemittanceDataToTable(remittancesData) {
            $('#tableRemittances').children().remove();
            $.each(remittancesData, function (i, value) {
                trHTML = '<tr>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['oie'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['state_packaging'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['date_oie'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['iso'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['cta_coffee'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['lotes'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['cargolot'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['remesas'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['seal_1'] + '</td>';
                trHTML += '<td>' + value['ViewTrackgingPackaging']['seal_2'] + '</td>';
                trHTML += '<td class="actions">'+'<a href="/OperationTrackings/addctns/'+value['ViewTrackgingPackaging']['oie']+'" target="_blank">Fotografiar Ctns</a></td>';
                trHTML += '</tr>';
                $('#tableRemittances').append(trHTML);
            });
        }
    </script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Listar Conts'), array('controller' => 'ViewTrackgingPackagings', 'action' => 'index')); ?> </li>
        </ul>
</div>
