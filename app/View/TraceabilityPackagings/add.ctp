<div class="traceabilityPackagings form">
<?php echo $this->Form->create('TraceabilityPackaging',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Realizar trazabilidad Embalaje '.$packagingCoffee['ReadyContainer']['bic_container']); ?></legend>
	<h3><?php echo __('Sequencia fotografica del embalaje'); ?></h3>
	<?php
		if($packagingCoffee['TraceabilityPackaging']['img1'] == null){
		  echo $this->Form->label('img1','(Imagen 1 )');
                  echo $this->Form->file('img1');
		}
                if($packagingCoffee['TraceabilityPackaging']['img2'] == null){
		  echo $this->Form->label('img2','(Imagen 2 )');
		  echo $this->Form->file('img2');
		}
                if($packagingCoffee['TraceabilityPackaging']['img3'] == null){
		  echo $this->Form->label('img3','(Imagen 3 )');
		  echo $this->Form->file('img3');
		}
		if($packagingCoffee['TraceabilityPackaging']['img4'] == null){
                  echo $this->Form->label('img4','(Imagen 4 )');
		  echo $this->Form->file('img4');
		}
		if($packagingCoffee['TraceabilityPackaging']['img5'] == null){
                  echo $this->Form->label('img5','(Imagen 5 )');
		  echo $this->Form->file('img5');
		}
		if($packagingCoffee['TraceabilityPackaging']['img6'] == null){
                  echo $this->Form->label('img6','(Imagen 6 )');
		  echo $this->Form->file('img6');
		}
		if($packagingCoffee['TraceabilityPackaging']['img7'] == null){
                  echo $this->Form->label('img7','(Imagen 7 )');
		  echo $this->Form->file('img7');
		}
		if($packagingCoffee['TraceabilityPackaging']['img8'] == null){
                  echo $this->Form->label('img8','(Imagen 8 )');
		  echo $this->Form->file('img8');
		}
                if($packagingCoffee['TraceabilityPackaging']['img9'] == null){
                  echo $this->Form->label('img9','(Imagen 9 )');
		  echo $this->Form->file('img9');
                }
                if($packagingCoffee['TraceabilityPackaging']['img10'] == null){
                  echo $this->Form->label('img10','(Imagen 10 )');
		  echo $this->Form->file('img10');
                }
		if($packagingCoffee['TraceabilityPackaging']['img11'] == null){
                  echo $this->Form->label('img11','(Imagen 11 )');
		  echo $this->Form->file('img11');
                }
		if($packagingCoffee['TraceabilityPackaging']['img12'] == null){
                  echo $this->Form->label('img12','(Imagen 12 )');
		  echo $this->Form->file('img12');
                }
                ?>
                <p>
                <h3><?php echo __('Información de cierre y sellos del contenedor'); ?></h3>
                <p>
                <?php echo $this->Form->input('close_container', array('label' => 'Cerrar contenedor?', 'type' => 'checkbox'));?>
		<div class="row actions">
                    <?php echo $this->Form->input('time_start',array('required' => true, 'type'=>'datetime','timeFormat' => 24, 'label'=>'Hora de Inicio'));?>
                    <?php echo $this->Form->input('time_end',array('required' => true, 'type'=>'datetime','timeFormat' => 24, 'label'=>'Hora de Final'));?>
                </div>
                <?php
                    echo $this->Form->input('seal1',array('label'=>'Número del Sello #1'));
                    echo $this->Form->input('seal2',array('label'=>'Número del Sello #2'));
                    echo $this->Form->input('seal3',array('label'=>'Número del Sello #3'));
                    echo $this->Form->input('seal4',array('label'=>'Número del Sello #4'));
                ?>
	
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'),$this->request->referer() ); ?></li>
	</ul>
</div>
