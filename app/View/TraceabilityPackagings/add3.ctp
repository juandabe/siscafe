<div class="traceabilityPackagings form">
<?php echo $this->Form->create('TraceabilityPackaging',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Realizar trazabilidad Embalaje '.$packagingCoffee['ReadyContainer']['bic_container']); ?></legend>
	<h3><?php echo __('Sequencia fotografica del embalaje 3'); ?></h3>
	<?php
		echo $this->Form->label('img7','(Sacos y número de lote de café - Imagen 7 )');
                echo $this->Form->file('img7');
		echo $this->Form->label('img8','(Puerta contenedor cerrado y Lote de café lleno - Imagen 8 )');
		echo $this->Form->file('img8');
		echo $this->Form->label('img9','(Cierre del contenedor - Imagen 9 )');
		echo $this->Form->file('img9');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'),$this->request->referer() ); ?></li>
	</ul>
</div>
