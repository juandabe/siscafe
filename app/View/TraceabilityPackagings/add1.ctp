<div class="traceabilityPackagings form">
<?php echo $this->Form->create('TraceabilityPackaging',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Realizar trazabilidad Embalaje '.$packagingCoffee['ReadyContainer']['bic_container']); ?></legend>
	<h3><?php echo __('Sequencia fotografica del embalaje 1'); ?></h3>
	<?php
		echo $this->Form->label('img1','(Contendor vacio - Imagen 1 )');
                echo $this->Form->file('img1');
		echo $this->Form->label('img2','(Empapelado del contenedor- Imagen 2 )');
		echo $this->Form->file('img2');
		echo $this->Form->label('img3','(Suministros del contenedor- Imagen 3 )');
		echo $this->Form->file('img3');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'),$this->request->referer() ); ?></li>
	</ul>
</div>
