<div class="traceabilityPackagings form">
<?php echo $this->Form->create('TraceabilityPackaging',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Realizar trazabilidad Embalaje '.$packagingCoffee['ReadyContainer']['bic_container']); ?></legend>
	<h3><?php echo __('Sequencia fotografica del embalaje 4'); ?></h3>
	<?php
		echo $this->Form->label('img10','(Sellos de contenedor - Imagen 10 )');
                echo $this->Form->file('img10');
		echo $this->Form->label('img11','(Sellos de contenedor - Imagen 11 )');
		echo $this->Form->file('img11');
		echo $this->Form->label('img12','(Sellos de contenedor - Imagen 12 )');
		echo $this->Form->file('img12');
        ?>
                <p>
                <h3><?php echo __('Información de cierre y sellos del contenedor'); ?></h3>
                <p>
		<div class="row actions">
                    <?php echo $this->Form->input('time_start',array('required' => true, 'type'=>'datetime','timeFormat' => 24, 'label'=>'Hora de Inicio'));?>
                    <?php echo $this->Form->input('time_end',array('required' => true, 'type'=>'datetime','timeFormat' => 24, 'label'=>'Hora de Final'));?>
                </div>
                <?php
                    echo $this->Form->input('seal1',array('label'=>'Número del Sello #1'));
                    echo $this->Form->input('seal2',array('label'=>'Número del Sello #2'));
                    echo $this->Form->input('seal3',array('label'=>'Número del Sello #3'));
                    echo $this->Form->input('seal4',array('label'=>'Número del Sello #4'));
                ?>

	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'),$this->request->referer() ); ?></li>
	</ul>
</div>
