<div class="slotStores view">
<h2><?php echo __('Slot Store'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($slotStore['SlotStore']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name Space'); ?></dt>
		<dd>
			<?php echo h($slotStore['SlotStore']['name_space']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Stores Caffee Id'); ?></dt>
		<dd>
			<?php echo h($slotStore['SlotStore']['stores_caffee_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slot Store'), array('action' => 'edit', $slotStore['SlotStore']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Slot Store'), array('action' => 'delete', $slotStore['SlotStore']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $slotStore['SlotStore']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Slot Stores'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slot Store'), array('action' => 'add')); ?> </li>
	</ul>
</div>
