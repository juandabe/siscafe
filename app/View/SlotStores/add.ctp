<div class="slotStores form">
<?php echo $this->Form->create('SlotStore'); ?>
	<fieldset>
		<legend><?php echo __('Add Slot Store'); ?></legend>
	<?php
		echo $this->Form->input('name_space');
		echo $this->Form->input('stores_caffee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Slot Stores'), array('action' => 'index')); ?></li>
	</ul>
</div>
