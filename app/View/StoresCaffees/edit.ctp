<div class="storesCaffees form">
<?php echo $this->Form->create('StoresCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Stores Caffee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('store_name');
		echo $this->Form->input('city_location');
		echo $this->Form->input('created_date');
		echo $this->Form->input('updated_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('StoresCaffee.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('StoresCaffee.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Stores Caffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
