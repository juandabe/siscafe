<div class="storesCaffees index">
	<h2><?php echo __('Stores Caffees'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('store_name'); ?></th>
			<th><?php echo $this->Paginator->sort('city_location'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_date'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($storesCaffees as $storesCaffee): ?>
	<tr>
		<td><?php echo h($storesCaffee['StoresCaffee']['id']); ?>&nbsp;</td>
		<td><?php echo h($storesCaffee['StoresCaffee']['store_name']); ?>&nbsp;</td>
		<td><?php echo h($storesCaffee['StoresCaffee']['city_location']); ?>&nbsp;</td>
		<td><?php echo h($storesCaffee['StoresCaffee']['created_date']); ?>&nbsp;</td>
		<td><?php echo h($storesCaffee['StoresCaffee']['updated_date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $storesCaffee['StoresCaffee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $storesCaffee['StoresCaffee']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $storesCaffee['StoresCaffee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $storesCaffee['StoresCaffee']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Stores Caffee'), array('action' => 'add')); ?></li>
	</ul>
</div>
