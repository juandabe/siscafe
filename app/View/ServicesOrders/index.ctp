
<?php 
    
    function getCtaCoffee($servicesOrder){
        $total=0;
        foreach($servicesOrder['ServicesOrdersBilling'] as $row){
            $total = $total + $row['qty'];
        }
        return $total;
    }

    function getLotsWtithConexos($servicesOrder){
        $msg="<ul>";
        foreach($servicesOrder['ServicesOrdersBilling'] as $row){
            if(strlen($row['lot_caffee'])>1){
                $msg = $msg . "<li>" . $row['observation'] ."</li><br>";
            }
        }
        $msg.="</ul>";
        return $msg;
    }

    function getLotCrossdocking($servicesOrder){
        $msg="<ul>";
        foreach($servicesOrder['ServicesOrdersBilling'] as $row){
            if($row['lot_caffee'] == 9){
                $msg = $msg . "<li>" .  $row['observation'] . "</li><br>";
            }
        }
        $msg.="</ul>";
        return $msg;
    }

    function getSupplyOtherServices($servicesOrder){
        $msg="<ul>";
        foreach($servicesOrder['ServicesOrdersBilling'] as $row){
            if($row['lot_caffee'] == 0){
                $msg = $msg . "<li>" .  $row['observation2'] . "</li><br>";
            }
        }
        $msg.="</ul>";
        return $msg;
    }
?>

<div class="servicesOrders index">
<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
    
</style>
    <h2><?php echo __('Listado de servicios conexos'); ?></h2>
    <?php echo $this->Html->link(__('Registrar orden de servicio'), array('action' => 'add'));?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('ID'); ?></th>
                <th><?php echo $this->Paginator->sort('Exportador'); ?></th>
                <th><?php echo $this->Paginator->sort('Estado'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha reg'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha cierre'); ?></th>
                <th><?php echo $this->Paginator->sort('Lotes descargardo'); ?></th>
                <th><?php echo $this->Paginator->sort('Lotes preavisados'); ?></th>
                <th><?php echo $this->Paginator->sort('Suministros - Otro servicios'); ?></th>
                <th><?php echo $this->Paginator->sort('Cta Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('Puerto'); ?></th>
                <th class="actions"><?php echo __('--'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($servicesOrders as $servicesOrder): ?>
                <tr>
                    <td><?php echo h($servicesOrder['ServicesOrder']['id']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrder['Client']['business_name']); ?>&nbsp;</td>
                    <?php  if($servicesOrder['HookupStatus']['id'] == 2) {
                            echo "<td style=\"background: red\"><span>".$servicesOrder['HookupStatus']['status_name']."</span></td>";
                        }
                        else if($servicesOrder['HookupStatus']['id'] == 1) {
                            echo "<td style=\"background: blue\"><span>".$servicesOrder['HookupStatus']['status_name']."</span></td>";
                        }
                        else if($servicesOrder['HookupStatus']['id'] == 3) {
                            echo "<td style=\"background: yellow\"><span>".$servicesOrder['HookupStatus']['status_name']."</span></td>";
                        }
                        else if($servicesOrder['HookupStatus']['id'] == 4) {
                            echo "<td style=\"background: green\"><span>".$servicesOrder['HookupStatus']['status_name']."</span></td>";
                        } 
                        else if($servicesOrder['HookupStatus']['id'] == 5) {
                            echo "<td style=\"background: #bbbbbb\"><span>".$servicesOrder['HookupStatus']['status_name']."</span></td>";
                        } 
                    ?>
                    <td><?php echo h($servicesOrder['ServicesOrder']['created_date']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrder['ServicesOrder']['closed_date']); ?>&nbsp;</td>
                    <td><?php 
                            echo getLotsWtithConexos($servicesOrder); 
                        ?>&nbsp;</td>
                    <td><?php echo getLotCrossdocking($servicesOrder); ?>&nbsp;</td>
                    <td><?php 
                            echo getSupplyOtherServices($servicesOrder); 
                        ?>&nbsp;</td>
                    <td><?php echo getCtaCoffee($servicesOrder); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrder['Departament']['name']); ?>&nbsp;</td>
                    <td class="actions">
                    <div class="dropdown">
                    <button class="dropbtn"><strong>Menú</strong></button>
                    <div class="dropdown-content">
                        <?php 
                              echo $this->Html->link(__('Ver'), 'javascript:void(0)',array('onclick'=> 'view_modal('.$servicesOrder['ServicesOrder']['id'].')'));
                              echo $this->Html->link(__('Fotografias'), array('controller' => 'OperationTrackings','action' => 'addConexos', $servicesOrder['ServicesOrder']['id']));
             	        ?>
                        <?php  ?>
                        </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Fumicafe'), array('controller' => 'FumigationServices', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('Buscar'), array('controller' => 'ServicesOrders', 'action' => 'search')); ?> </li>
        <li><?php //echo $this->Html->link(__('Paquetes Servicios'), array('controller' => 'ServicePackages', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('Conexos Completados'), array('controller' => 'ServicesOrders', 'action' => 'completed')); ?> </li>
        <li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
    </ul>
</div>

<div id="modal">
    <div class="modal-body"></div>
</div>

<script type="text/javascript">

    function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'ServicesOrders', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

    function add_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'DetailsServicesToCaffees', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

    $(function() {
            $('#modal').dialog({
                autoOpen: false,
                modal: true,
                height: "auto",
                width: "auto",
                resizable: false,
                show: {
                  effect: "blind",
                  duration: 100
                },
                hide: {
                  effect: "explode",
                  duration: 100
                }
              });
	});

</script>
