
<?php 

function getTotalBilling($servicesOrdersBillings){
    $total = 0;
    foreach ($servicesOrdersBillings as $item) {
        $total = $total + $item['ServicesOrdersBilling']['value'];
    }
    return $total;
}

?>

<style>
    dd{
        width: 70%;
    }
</style>
<span class="actions">
    <?php echo (in_array($servicesOrder['ServicesOrder']['hookup_status_id'],[2,1])) ? $this->Html->link(__('Gestionar'), 'javascript:void(0)',array('onclick'=> 'add_modal('.$servicesOrder['ServicesOrder']['id'].')')):"";?>
    <?php echo (in_array($servicesOrder['ServicesOrder']['hookup_status_id'],[2,1])) ? $this->Html->link(__('Completar'), 'javascript:void(0)',array('onclick'=> 'complete_all('.$servicesOrder['ServicesOrder']['id'].')')):"";?>
</span>
    <p>
    <h2><?php 
    echo __('Resumen servicios del conexo'); 
    ?></h2>
    
    <dl>
        <dt><?php echo __('ID'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['ServicesOrder']['id']); ?>
        </dd>
        <dt><?php echo __('Cliente'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['Client']['exporter_code'] ." - ".$servicesOrder['Client']['business_name']); ?>
        </dd>
        <dt><?php echo __('Estado'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['HookupStatus']['status_name']); ?>
        </dd>
        <dt><?php echo __('Fecha de Creación'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['ServicesOrder']['created_date']); ?>
        </dd>
        
        <dt><?php echo __('Valor Total'); ?></dt>
        <dd><?php echo $this->Number->currency(getTotalBilling($dataServicesOrdersBillings), 'USD', ['precision' => 0]); ?></dd>
        <dt><?php echo __('Fecha de Cierre'); ?></dt>
        <dd>
            <?php echo h(($servicesOrder['ServicesOrder']['closed_date']=='')?"Por definir":$servicesOrder['ServicesOrder']['closed_date']); ?>
        </dd>
        <dt><?php echo __('Instrucciones del Cliente: '); ?></dt>
        <dd>
            <?php echo h(($servicesOrder['ServicesOrder']['instructions']=='')?"N/A":$servicesOrder['ServicesOrder']['instructions']); ?>
        </dd>


        <dt><?php echo __('Archivo PDF: '); ?></dt>
        <dd>
             <?php echo ($servicesOrder['ServicesOrder']['pdfica'] == '')?"N/A": $this->Html->link(__('Descargar'), array('controller' => 'ServicesOrders','action' => 'getTrackingRequest', $servicesOrder['ServicesOrder']['id'])) ; ?>
        </dd>

</br>

    </dl>
    <br>
    
    <h2><?php echo __('Detalles del servicio'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Id'); ?></th>
                <th><?php echo $this->Paginator->sort('Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('Lote Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('Cant. Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('ID Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Precio Unit'); ?></th>
                <th><?php echo $this->Paginator->sort('Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Valor'); ?></th>
                <th class="actions"><?php echo __('--'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detailsServicesToCaffees as $detailsServicesToCaffee): ?>
                <tr>
                <td><?php 
                //debug($detailsServicesToCaffees);exit;
                echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['id']); ?>&nbsp;</td>
                    <td><?php echo ($detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id']) ? $detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id']:"N/A"; ?>&nbsp;</td>
                    <td><?php echo ($detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id']) ? $detailsServicesToCaffee['Client']['exporter_code'] . " - " . $detailsServicesToCaffee['RemittancesCaffee']['lot_caffee']:"N/A"; ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['qta_bag_to_work']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['ItemsUnoeeConexo']['ITEMS_UNOEE']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['ItemsUnoeeConexo']['TARIFA']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['observation']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['value']); ?>&nbsp;</td>
                    <td class="actions">
					<?php if($detailsServicesToCaffee['DetailsServicesToCaffee']['completed'] == 0){
                            echo $this->Html->link(__('Completar'), 'javascript:void(0)',array('onclick'=> 'completar('.$detailsServicesToCaffee['DetailsServicesToCaffee']['id'].')',array('confirm' => __('¿Esta seguro de completar este trabajo ?'))));
                            }
                            echo (in_array($servicesOrder['ServicesOrder']['hookup_status_id'],[1,2])) ?  $this->Html->link(__('--'), 'javascript:void(0)',array('onclick'=> 'borrar('.$detailsServicesToCaffee['DetailsServicesToCaffee']['id'].')',array('confirm' => __('¿Esta seguro de completar este trabajo ?')))):"";
                             ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
    <h2><?php echo __('Detalles del suministros solicitados:'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Nombre'); ?></th>
                <th><?php echo $this->Paginator->sort('Valor total'); ?></th>
                <th><?php echo $this->Paginator->sort('Cantidad'); ?></th>
                <th><?php echo $this->Paginator->sort('Observación'); ?></th>
                <th><?php echo $this->Paginator->sort('Se translada?'); ?></th>
                <th><?php echo $this->Paginator->sort('Terminal'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha reg'); ?></th>
                <th class="actions"><?php echo __('--'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataServicesOrdersSupply as $servicesOrdersSupply): ?>
                <tr>
                    <td><?php echo h($servicesOrdersSupply['ItemsUnoeeConexo']['DESCRIPCION_ITEM']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrdersSupply['ItemsUnoeeConexo']['TARIFA']*$servicesOrdersSupply['ServicesOrdersSupply']['cantidad']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrdersSupply['ServicesOrdersSupply']['cantidad']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrdersSupply['ServicesOrdersSupply']['observation']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrdersSupply['ServicesOrdersSupply']['is_move'] ? "SI": "NO"); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrdersSupply['Departament']['name']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrdersSupply['ServicesOrdersSupply']['date_reg']); ?>&nbsp;</td>
                    <td class="actions">
					<?php echo (in_array($servicesOrder['ServicesOrder']['hookup_status_id'],[1,2])) ? $this->Html->link(__('--'), 'javascript:void(0)',array('onclick'=> 'delete_supply('.$servicesOrdersSupply['ServicesOrdersSupply']['id'].')',array('confirm' => __('¿Esta seguro de eliminar este registro ?')))):""; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
    <h2><?php echo __('Detalles del Cafe Programados SIN descargar:'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Id'); ?></th>
                <th><?php echo $this->Paginator->sort('Lote Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('Cantidad'); ?></th>
                <th><?php echo $this->Paginator->sort('Item UnoEE'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha reg'); ?></th>
                <th class="actions"><?php echo __('--'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($servicesOrder['DetailServicesOrderCrossdocking'] as $dataRow): 
                //var_dump($dataRow);exit;
                ?>
                <tr>
                <td><?php echo h($dataRow['id']); ?>&nbsp;</td>
                    <td><?php echo h($dataRow['lot_coffee']); ?>&nbsp;</td>
                    <td><?php echo h($dataRow['qta_coffee']); ?>&nbsp;</td>
                    <td><?php echo h($dataRow['observation']); ?>&nbsp;</td>
                    <td><?php echo h($dataRow['reg_date']); ?>&nbsp;</td>
                    <td class="actions">
					<?php echo (in_array($servicesOrder['ServicesOrder']['hookup_status_id'],[1,2])) ? $this->Html->link(__('--'), 'javascript:void(0)',array('onclick'=> 'delete_crossdocking('.$dataRow['id'].')',array('confirm' => __('¿Esta seguro de eliminar este registro ?')))):""; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>

    <script type="text/javascript">

    function completar(id) {
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'DetailsServicesToCaffees', 'action' => 'completed']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    alert(data);
                    view_modal(JSON.parse(data)['para la orden de servicio #']);
                },
                type: "post"
                
            });
            return false;
    }

    function complete_all(id) {
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'ServicesOrders', 'action' => 'complete_all']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    if(JSON.parse(data)['result']){
                        view_modal(id);
                    }
                    else{
                        alert("No se pude completar. Falta de permisos asociados");
                    }
                },
                type: "post"
                
            });
            return false;
    }

    function delete_supply(id) {
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'DetailsServicesToCaffees', 'action' => 'delete_supply']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    if(JSON.parse(data)['result']){
                        view_modal(JSON.parse(data)['services_order']);
                    }
                    else{
                        alert("No se pude eliminar. Intento nuevamente.");
                    }
                },
                type: "post"
                
            });
            return false;
    }

    function delete_crossdocking(id) {
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'DetailsServicesToCaffees', 'action' => 'delete_crossdocking']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    if(JSON.parse(data)['result']){
                        view_modal(JSON.parse(data)['services_order']);
                    }
                    else{
                        alert("No se pude eliminar. Intento nuevamente.");
                    }
                },
                type: "post"
                
            });
            return false;
    }

    function borrar(id) {
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'DetailsServicesToCaffees', 'action' => 'delete']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    view_modal(JSON.parse(data)['service_order']);
                    alert(JSON.parse(data)['msj']);
                },
                type: "post"
                
            });
            return false;
    }

    function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'ServicesOrders', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

    function add_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'DetailsServicesToCaffees', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

    $( "#modal" ).dialog({
        close: function( event, ui ) {
            window.location.href = '<?= Router::url($this->request->referer()) ?>';
        }
    }).dialog({
        position: {
            my: "center center",
            at: "center center",
            of: window
        }
    });

    

    </script>



