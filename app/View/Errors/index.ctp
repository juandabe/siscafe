<div class="remittancesCaffees index">
    <h2><?php echo __('Listado de remesas radicadas'); ?></h2>
    <?php echo $this->Html->link(__('Registrar descargue de café'), array('action' => 'add'));?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id','C.O-Remesa ID'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_caffee','Número Lote'); ?></th>
                <th><?php 
                if($this->Session->read('User.centerId') == 1)
                    echo $this->Paginator->sort('slotStore','Ubica'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                <th><?php echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                <th><?php echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                <th><?php 
                if($this->Session->read('User.centerId') == 1)
                echo $this->Paginator->sort('state_operation_id','Estado Operativo'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Fecha Radicado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                //debug($remittancesCaffee);exit;
                echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                <td><?php echo h("3-".$remittancesCaffee['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php 
                if($this->Session->read('User.centerId') == 1)
                    echo h($remittancesCaffee['SlotStore']['name_space']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_nominal']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                <td><?php 
                if($this->Session->read('User.centerId') == 1)
                echo h($remittancesCaffee['StateOperation']['name']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>

                <td class="actions">
			<?php 
                        //debug($remittancesCaffees);exit;
                        //debug(Configure::read('siscafe.url_pro'));exit;
                          if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profile') == "Colaborador" || $this->Session->read('User.profile') == "Funcionario FNC")
                            if(Configure::read('siscafe.mode_app') == '1')
                              echo $this->Html->link(__('Papeleta'), Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=1&toPdf=true&idPrinter=1");
                            else
                              echo $this->Html->link(__('Papeleta'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=1&toPdf=true&idPrinter=1");
                          if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profile') == "Colaborador" || $this->Session->read('User.profile') == "Funcionario FNC" || $this->Session->read('User.profile') == "Invitado")
                            echo $this->Html->link(__('Ver Pesos'), array('action' => 'weights', $remittancesCaffee['RemittancesCaffee']['id']));
                          if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profile') == "Colaborador")
                            echo $this->Html->link(__('Stiker'), "http://localhost:8080/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=4&toPdf=true&idPrinter=1"); 
                          if($this->Session->read('User.profile') == "Colaborador" && $remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 1 || $remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 8)
                            //echo $this->Html->link(__('Editar'), array('action' => 'edit', $remittancesCaffee['RemittancesCaffee']['id']));
                          if($this->Session->read('User.centerId') != 1){
                              echo $this->Html->link(__('Ver'), array('action' => 'view', $remittancesCaffee['RemittancesCaffee']['id']));
                              echo $this->Html->link(__('Editar'), array('controller' => 'RemittancesCaffee','action' => 'edit', $remittancesCaffee['RemittancesCaffee']['id']));
                          }
                          if($this->Session->read('User.profile') == "Colaborador"){
                            if($remittancesCaffee['RemittancesCaffee']['novelty_in_caffee_id'] == null){
                                echo $this->Html->link(__('Novedad'), array('controller' => 'NoveltyInCaffees','action' => 'add', $remittancesCaffee['RemittancesCaffee']['id']));
                            }
                            else{
                                if(Configure::read('siscafe.mode_app') == 1)
                                    $url = Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=5&toPdf=true&idPrinter=1";
                                else{
                                    $url = Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=5&toPdf=true&idPrinter=1";
                                    echo $this->Html->link(__('Ver Novedad'),$url);
                                    echo $this->Html->link(__('F.N'), array('action' => 'zip', $remittancesCaffee['RemittancesCaffee']['id']));
                                }
                            }
                          }
                        ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php 
        if($this->Session->read('User.profile') == "Colaborador"){
            echo $this->Html->link(__('Lotes descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1));
            echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'colaborador')); 
        }
        else if($this->Session->read('User.profile') == "Invitado")
            echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'invitado')); 
        else if($this->Session->read('User.profile') == "Funcionario FNC")
            echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'fnc')); 
        ?></li>
    </ul>
</div>
