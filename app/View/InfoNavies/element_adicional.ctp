<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: white;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
    
</style>

<div class="infoNavies form">

    <fieldset>
        <legend><?php echo __('Agregar Elementos Adicionales a la Proforma (Información Naviera)'); ?></legend>
        <div class="dropdown">
            <button class="dropbtn"><strong>Suministros</strong></button>
                <div class="dropdown-content">
                <?php 
                        echo $this->Html->link(__('MANTAS V-DRY BUN 20'), 'javascript:void(0)',array('onclick'=> 'view_modal('.$infoNavy['InfoNavy']['id'].')'));
                    ?>
                <?php  ?>
                </div>
            </div>
        <?php echo $this->Form->create('InfoNavy'); ?>
        <table>
        
            <tr>
                <?php echo $this->Form->input('id'); ?>
                <td><?php echo $this->Form->input('proforma',array('disabled' => 'disabled')); ?></td>
                <td><?php echo $this->Form->input('booking',array('disabled' => 'disabled')); ?></td>
                <td><?php echo $this->Form->input('motorship_name',array('disabled' => 'disabled'));  ?></td>              
            </tr>
        </table>
        
        <?php echo $this->Form->input('type_papper',array('label'=>'Tipo de Papel','required' => true, 'type' => 'select',
           'options' =>array('KRAFT'=>'PAPEL KRAFT','CARTON'=>'PAPEL CARTON', 'REF-CTN'=>'CTN REF'),
           'empty'=>'Seleccione un  tipo de papel...','style'=>'width:100%; font-size:16px; height:40px; color:#E32;'));  ?>
		<?php echo $this->Form->input('num_ctn_real',array('label'=>'Ctns Embalados','type'=>'number','required' => true,'default'=>1));  ?>
        <?php echo $this->Form->input('adictional_dry_bags',array('label'=>'Cantidad de Dry Bags','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_kraft_20',array('label'=>'Cantidad de capa kraft de 20','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_kraft_40',array('label'=>'Cantidad de capa kraft de 40','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_carton_20',array('label'=>'Cantidad de capa carton de 20','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_carton_40',array('label'=>'Cantidad de capa carton de 40','required' => true));  ?>
        <?php echo $this->Form->input('adictional_num_label',array('label'=>'Número de etiqueta','required' => true));  ?>
        <?php echo $this->Form->input('adictional_num_stiker',array('label'=>'Número de stiker','required' => true));  ?>
        <?php echo $this->Form->input('adictional_num_manta',array('label'=>'Número V-DRY 20','required' => true));  ?>
        <?php echo $this->Form->input('mantas_vdry_40',array('label'=>'Número V-DRY 40','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('tsl',array('label'=>'TSL','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('hc_qta',array('label'=>'HC','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('seals',array('label'=>'Sellos','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('tls_desiccant',array('label'=>'TSL con Desecantes','required' => true,'default'=>'0'));  ?>
        <?php echo $this->Form->input('rejillas',array('label'=>'Rejillas','required' => true));  ?>
	<?php echo $this->Form->input('dataloger',array('label'=>'Data Logger','required' => true));  ?>
	<?php echo $this->Form->input('absortes',array('label'=>'Absorbentes','required' => true));  ?>
	<?php echo $this->Form->input('pallets',array('label'=>'Pallets','required' => true));  ?>
        
    </fieldset>
<?php echo $this->Form->end(__('Actualizar')); ?>
</div>

<div id="modal">
    <div class="modal-body"></div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
<script type="text/javascript">

$(function() {
    $('#modal').dialog({
        autoOpen: false,
        modal: true,
        height: "auto",
        width: "auto",
        resizable: false,
        show: {
            effect: "blind",
            duration: 100
        },
        hide: {
            effect: "explode",
            duration: 100
        }
        });
	});

    function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'SuppliesOperations', 'action' => 'addajax']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                //type: "post"
                
            });
            return false;
    }

</script>
