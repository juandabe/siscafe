<div class="infoNavies index">
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('InfoNavy');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda de Navieras'); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('codigo',array('label' => 'Código'));?></td>
                <td><?php echo $this->Form->input('proforma',array('label' => 'Proforma'));?></td>
                <td><?php echo $this->Form->input('booking',array('label' => 'Número de Booking'));?></td>
                <td><?php echo $this->Form->input('motorship_name',array('label' => 'Motonave'));?></td>
            </tr>  
        </table>
        <?php echo $this->Form->end(__('Buscar')); ?>
                    <br>

    </fieldset>
    <table>
        <tr>
            <td>
                <table id="tbDataOie">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('', 'Código'); ?></th>
                            <th><?php echo $this->Paginator->sort('proforma','Proforma'); ?></th>
                            <th><?php echo $this->Paginator->sort('booking','Booking'); ?></th>
                            <th><?php echo $this->Paginator->sort('motorship_name','Motonave'); ?></th>
                            <th><?php echo $this->Paginator->sort('packaging_type','Tipo'); ?></th>
                            <th><?php echo $this->Paginator->sort('packaging_mode','Modo Embalaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('travel_num','Viaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fceha Embalaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('elements_adicional','Elementos Adicionales'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($searchInfoList){
                        foreach ($searchInfoList as $searchInfo): ?>
                        <tr>
                            <td><?php echo h($searchInfo['InfoNavy']['id']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['proforma']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['booking']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['motorship_name']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['packaging_type']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['packaging_mode']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['travel_num']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['created_date']); ?>&nbsp;</td>
                            <td><?php 
                                echo h('Papel '.$searchInfo['InfoNavy']['type_papper'].
                                        ', DRY BAGS ('.$searchInfo['InfoNavy']['adictional_dry_bags'].')'.
                                        ', kraft de 20 ('.$searchInfo['InfoNavy']['adictional_layer_kraft_20'].')'.
                                        ', kraft de 40 ('.$searchInfo['InfoNavy']['adictional_layer_kraft_40'].')'.
                                        ', carton de 20 ('.$searchInfo['InfoNavy']['adictional_layer_carton_20'].')'.
                                        ', carton de 40 ('.$searchInfo['InfoNavy']['adictional_layer_carton_40'].')'.
                                        ', etiquetas ('.$searchInfo['InfoNavy']['adictional_num_label'].')'.
                                        ', stiker ('.$searchInfo['InfoNavy']['adictional_num_stiker'].')'.
                                        ', mantas ('.$searchInfo['InfoNavy']['adictional_num_manta'].')')
                            ?>&nbsp;</td>    
                            <td class="actions">
                                    <?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $searchInfo['InfoNavy']['id'])); ?>
                                    <!--?php echo $this->Html->link(__('Elementos adicionales'), array('controller' => 'AdictionalElementsHasPackagingCaffees', 'action' => 'addelementadicional','?' => ['info_navy_id' => $searchInfo['InfoNavy']['id'],'proforma' => $searchInfo['InfoNavy']['proforma']])); ?-->
                                    <?php echo $this->Html->link(__('Adicionales'), array('action' => 'elementAdicional', $searchInfo['InfoNavy']['id'])); ?>
                            </td>
                        </tr>
                    <?php endforeach;} ?>
                    </tbody>
                </table>
            </td>
            <td>
 
            </td>
        </tr>
    </table>

</div>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
