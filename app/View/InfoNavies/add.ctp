<div class="infoNavies form">
<?php 
    echo $this->Html->script('jquery.min');
    echo $this->Form->create('InfoNavy'); ?>
    <fieldset>
        <legend><?php echo __('Registrar Proforma (Información de aduanas)'); ?></legend>
	<?php
            echo $this->Form->input('proforma',array('required' => true));
            echo $this->Form->input('packaging_type',array('label' => 'Tipo Embalaje','empty'=>'Seleccione...', 'required' => true,'options' => array('FCL' => 'FCL', 'LCL' => 'LCL')));
            echo $this->Form->input('packaging_mode',array('label' => 'Modo Embalaje','empty'=>'Seleccione...', 'required' => true,'options' => array('Sacos' => 'Sacos','Granel' => 'Granel','CACAO' => 'CACAO','COMBINADO(SACOS - CAJAS)' => 'COMBINADO(SACOS - CAJAS)','COMBINADO(SACOS - BIG BAGS)' => 'COMBINADO(SACOS - BIG BAGS)','BIG BAGS' => 'BIG BAGS','Cajas' => 'Cajas')));
            echo $this->Form->input('long_container',array('label' => 'Longitud Contenedor','empty'=>'Seleccione...', 'required' => true,
                'options' => array('20' => '22G1', '40' => '42G1', '20-RF' => '20 RF', '40-RF' => '40 RF')));
            echo $this->Form->input('motorship_name',array('label' => 'Motonave','required' => true));
            echo $this->Form->input('booking',array('label' => 'Número Booking'));
            echo $this->Form->input('travel_num',array('label' => 'Número Viaje'));
            echo $this->Form->input('destiny',array('label' => 'Destino'));
            echo $this->Form->input('navy_agent_id',array('label' => 'Código Agente Marítimo', 'type'=>'text', 'required' => true));
            echo $this->Form->input('navy_agent_name',array('label' => 'Nombre Agente Marítimo','disabled' => 'disabled'));
            echo $this->Form->input('shipping_lines_id',array('label' => 'Código Línea Naviera', 'type'=>'text', 'required' => true));
            echo $this->Form->input('shipping_lines_name',array('label' => 'Nombre Línea Naviera','disabled' => 'disabled'));
            echo $this->Form->input('customs_id',array('label' => 'Código Agencia de Aduanas', 'type'=>'text', 'required' => true));
            echo $this->Form->input('customs_name',array('label' => 'Nombre Agencia de Aduanas','disabled' => 'disabled'));
            echo $this->Form->input('observation',array('label' => 'Observaciones'));
	?>
    </fieldset>
    <script type="text/javascript">
        $("#InfoNavyNavyAgentId").keyup(function () {
            findNavyAgent($(this).val());
        });

        $("#InfoNavyShippingLinesId").keyup(function () {
            findShippingLine($(this).val());
        });

        $("#InfoNavyCustomsId").keyup(function () {
            findCustom($(this).val());
        });

	$("#InfoNavyProforma").keyup(function () {
            findNavyProforma($(this).val());
        });
        
        function findNavyProforma(proforma){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/InfoNavies/findInfoNavyByProforma/" + proforma,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        alert("Proforma YA existe. Por favor revisar");
                    }
                }});
        }

        function findNavyAgent(id) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/NavyAgents/findById/" + id,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#InfoNavyNavyAgentName").val(json.NavyAgent.name);
                    } else {
                        $("#InfoNavyNavyAgentName").val("NO ARROJO RESULTADOS");
                    }
                }});
        }
        
        function findShippingLine(id) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/ShippingLines/findById/" + id,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#InfoNavyShippingLinesName").val(json.ShippingLine.business_name);
                    } else {
                        $("#InfoNavyShippingLinesName").val("NO ARROJO RESULTADOS");
                    }
                }});
        }
        
        function findCustom(id) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/Customs/findById/" + id,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#InfoNavyCustomsName").val(json.Custom.cia_name);
                    } else {
                        $("#InfoNavyCustomsName").val("NO ARROJO RESULTADOS");
                    }
                }});
        }
    </script>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('Listado proformas'), array('action' => 'index')); ?></li>
    </ul>
</div>
