<?php

App::uses('ConnectionManager', 'Model');
App::uses('AppHelper', 'View/Helper');

class UtilHelper extends AppHelper {
  
    public static function getAwsAccess(){
        $db = ConnectionManager::getDataSource('default');
        $dataAccess = $db->query("select aws_key, aws_secret from aws_access_data where username = 'siscafe-webapp'");
        return $dataAccess[0]['aws_access_data'];
    }

}

?>
