<div class="checkingCtns view">
<h2><?php echo __('Chequeo Ctn '.$findPackagingCaffee['ReadyContainer']['bic_container']) ?></h2>
	<dl>
		<dt><?php 
                echo __('Fecha Chequeo'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['date_check']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Estado del chequeo'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('1. Realización de prueba de luz'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['question1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('2. Identificación de oleres extraños'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['question2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('3. Verificación del estado de cierres, maniguetas y cauchos en puertas'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['question3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('4. Revisión del estado del piso del contendor'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['question4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('5. Verificación de limpieza general del contenedor.'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['question5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('6. Carga inspeccionada por antinarcóticos'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['question6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Imagen frente Ctnr'); ?></dt>
		<dd>
			<?php 
			  $path_img1 = $checkingCtn['CheckingCtn']['imagen1'];
			  echo $this->Html->image($path_img1);
                       ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Imagen fondo Ctnr'); ?></dt>
		<dd>
			<?php 
			  $path_img2 = $checkingCtn['CheckingCtn']['imagen2'];
			  echo $this->Html->image($path_img2);
                       ?>
		</dd>
		<dt><?php echo __('Imagen diagonal Ctnr'); ?></dt>
		<dd>
			<?php 
			  $path_img3 = $checkingCtn['CheckingCtn']['imagen3'];
			  echo $this->Html->image($path_img3);
                       ?>
		</dd>
		
		<dt><?php echo __('Observación'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['CheckingCtn']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php echo h($checkingCtn['User']['first_name']." ".$checkingCtn['User']['last_name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Embalaje'), array('controller'=>'PackagingCaffees','action' => 'index')); ?></li>
	</ul>
</div>
