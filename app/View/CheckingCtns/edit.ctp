<div class="checkingCtns form">
<?php echo $this->Form->create('CheckingCtn'); ?>
	<fieldset>
		<legend><?php echo __('Edit Checking Ctn'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('date_check');
		echo $this->Form->input('question1');
		echo $this->Form->input('question2');
		echo $this->Form->input('question3');
		echo $this->Form->input('question4');
		echo $this->Form->input('question5');
		echo $this->Form->input('question6');
		echo $this->Form->input('imagen1');
		echo $this->Form->input('imagen2');
		echo $this->Form->input('imagen3');
		echo $this->Form->input('status');
		echo $this->Form->input('observation');
		echo $this->Form->input('packaging_caffee_id');
		echo $this->Form->input('users_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CheckingCtn.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('CheckingCtn.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Checking Ctns'), array('action' => 'index')); ?></li>
	</ul>
</div>
