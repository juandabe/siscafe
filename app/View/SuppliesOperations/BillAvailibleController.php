<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * BillAvailible Controller
 *
 * @property \App\Model\Table\BillAvailibleTable $BillAvailible
 * @method \App\Model\Entity\BillAvailible[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillAvailibleController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $billsAvailible=[];

        $this->loadModel('Clients');
        $this->loadModel('BillOverdue');

        $this->loadModel('StaffMaster');
        $session = $this->request->getSession();

        if(!$session->read('StaffMaster.id')){
            return $this->redirect(['controller'=>'staffmaster','action' => 'login']);
        }

        $userSigned = $this->StaffMaster->find('all',[
            'conditions' => ['StaffMaster.id'=>$session->read('StaffMaster.id')]])
            ->first();

        $client = $this->Clients->get($userSigned->id_client);

        if ($this->request->is('post')) {
            $nitCustomerOwnerBill = $this->request->getData()['nitCustomerOwnerBill'];
            $idBill = $this->request->getData()['idBill'];
            
            if($nitCustomerOwnerBill){
                $options['conditions'] = 'BillAvailible.nit_client='.$client->nit;
                if($idBill){
                    $options['conditions'] = 'BillAvailible.num_factura='.$idBill.' AND BillAvailible.nit_client='.$client->nit;
                }
                
            }
            
            //$billsAvailible = $this->paginate($this->billAvailible);
            $billsAvailible = $this->BillAvailible->find('all', $options)->toArray();
        }

        

        $this->set(compact('billsAvailible','client'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function modal($id = null)
    {
        $this->loadModel('Checkout');
        $checkout = $this->Checkout->get($id, [
            'contain' => [],
        ]);

        $this->loadModel('Clients');
        $this->loadModel('BillOverdue');

        $this->loadModel('StaffMaster');
        $session = $this->request->getSession();

        if(!$session->read('StaffMaster.id')){
            return $this->redirect(['controller'=>'staffmaster','action' => 'login']);
        }

        $userSigned = $this->StaffMaster->find('all',[
            'conditions' => ['StaffMaster.id'=>$session->read('StaffMaster.id')]])
            ->first();

        $client = $this->Clients->get($userSigned->id_client);

        $options = [];
        if($checkout->type_checkout_id == 1){
            $nitCustomerOwnerBill = $client->nit;
            $options['conditions'] = 'BillAvailible.nit_client='.$nitCustomerOwnerBill;
        } 

        $billsAvailible = $this->BillAvailible->find('all', $options)->toArray();

        $this->set(compact('billsAvailible','checkout','client'));
    }

    /**
     * View method
     *
     * @param string|null $id Bill Availible id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $billAvailible = $this->BillAvailible->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('billAvailible'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $billAvailible = $this->BillAvailible->newEmptyEntity();
        if ($this->request->is('post')) {
            $billAvailible = $this->BillAvailible->patchEntity($billAvailible, $this->request->getData());
            if ($this->BillAvailible->save($billAvailible)) {
                $this->Flash->success(__('The bill availible has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill availible could not be saved. Please, try again.'));
        }
        $this->set(compact('billAvailible'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bill Availible id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $billAvailible = $this->BillAvailible->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $billAvailible = $this->BillAvailible->patchEntity($billAvailible, $this->request->getData());
            if ($this->BillAvailible->save($billAvailible)) {
                $this->Flash->success(__('The bill availible has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill availible could not be saved. Please, try again.'));
        }
        $this->set(compact('billAvailible'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bill Availible id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $billAvailible = $this->BillAvailible->get($id);
        if ($this->BillAvailible->delete($billAvailible)) {
            $this->Flash->success(__('The bill availible has been deleted.'));
        } else {
            $this->Flash->error(__('The bill availible could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
