<div class="suppliesOperations index">
	<h2><?php echo __('Movimientos del suministro '.$supply['Supply']['name']); ?></h2>
        <h3><?php echo __('Saldo actual: '.$supply['Supply']['saldo']); ?></h3>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('operation','Operación'); ?></th>
			<th><?php echo $this->Paginator->sort('qta','Cant. Unidades'); ?></th>
                        <th><?php echo $this->Paginator->sort('qta_before','Saldo Anterior'); ?></th>
			<th><?php echo $this->Paginator->sort('observation','Notas del movimiento'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date','Fecha del movimiento'); ?></th>
			<th><?php echo $this->Paginator->sort('supplies_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('info_navy_id','Id Proforma'); ?></th>
			<th><?php echo $this->Paginator->sort('users_id'); ?></th>
			<th class="actions"><?php echo __('--'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($suppliesOperations as $suppliesOperation): ?>
	<tr>
		<td><?php 
                //debug($suppliesOperation);exit;
                echo h($suppliesOperation['SuppliesOperation']['id']); ?>&nbsp;</td>
		<td><?php echo h($suppliesOperation['SuppliesOperation']['operation']); ?>&nbsp;</td>
		<td><?php echo h($suppliesOperation['SuppliesOperation']['qta']); ?>&nbsp;</td>
                <td><?php echo h($suppliesOperation['SuppliesOperation']['saldo_before']); ?>&nbsp;</td>
		<td><?php echo h($suppliesOperation['SuppliesOperation']['observation']); ?>&nbsp;</td>
		<td><?php echo h($suppliesOperation['SuppliesOperation']['created_date']); ?>&nbsp;</td>
		<td>
			<?php echo h($suppliesOperation['Supply']['name']); ?>
		</td>
                <td><?php echo ($suppliesOperation['SuppliesOperation']['info_navy_id'] == true) ?  h($suppliesOperation['SuppliesOperation']['info_navy_id']): "N/A" ; ?>&nbsp;</td>
		<td><?php echo h($suppliesOperation['User']['first_name']." ".$suppliesOperation['User']['last_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $suppliesOperation['SuppliesOperation']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $suppliesOperation['SuppliesOperation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'deleteOperation', $suppliesOperation['SuppliesOperation']['id']), array('confirm' => __('Esta seguro eliminar el registro # %s?', $suppliesOperation['SuppliesOperation']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Menu'), array('controller'=>'Pages','action' => 'colaborador')); ?></li>
		<li><?php echo $this->Html->link(__('Agregar movimiento'), array('action' => 'add',$supply['Supply']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Listado Suministros'), array('controller' => 'supplies', 'action' => 'index')); ?> </li>
	</ul>
</div>
