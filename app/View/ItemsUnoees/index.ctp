<div class="itemsUnoees index">
	<h2><?php echo __('Listado de Items Conexos'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('company'); ?></th>
			<th><?php echo $this->Paginator->sort('codigo'); ?></th>
			<th><?php echo $this->Paginator->sort('descripcion'); ?></th>
			<th><?php echo $this->Paginator->sort('tipo_servicio'); ?></th>
			<th><?php echo $this->Paginator->sort('unidad_medida'); ?></th>
			<th><?php echo $this->Paginator->sort('costo_estandar'); ?></th>
			<th><?php echo $this->Paginator->sort('grupo_impositivo'); ?></th>
			<th><?php echo $this->Paginator->sort('des_grupo_impositivo'); ?></th>
			<th><?php echo $this->Paginator->sort('cod_lista_precio'); ?></th>
			<th><?php echo $this->Paginator->sort('tipo_invent'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($itemsUnoees as $itemsUnoee): ?>
	<tr>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['id']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['company']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['codigo']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['descripcion']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['tipo_servicio']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['unidad_medida']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['costo_estandar']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['grupo_impositivo']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['des_grupo_impositivo']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['cod_lista_precio']); ?>&nbsp;</td>
		<td><?php echo h($itemsUnoee['ItemsUnoee']['tipo_invent']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $itemsUnoee['ItemsUnoee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $itemsUnoee['ItemsUnoee']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $itemsUnoee['ItemsUnoee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $itemsUnoee['ItemsUnoee']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Items Unoee'), array('action' => 'add')); ?></li>
	</ul>
</div>
