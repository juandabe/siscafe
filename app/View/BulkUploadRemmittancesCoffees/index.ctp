<div class="bulkUploadRemmittancesCoffees index">
	<h2><?php echo __('Cargue Masivo Descargues (Ingresos Lotes de Café Por Bloques) '); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name_file','Nombre Archivo'); ?></th>
			<th><?php echo $this->Paginator->sort('date_reg','Fecha registro'); ?></th>
			<th><?php echo $this->Paginator->sort('count_insert','Registros insertados'); ?></th>
			<th><?php echo $this->Paginator->sort('user_reg','Usuario Reg'); ?></th>
			<th><?php echo $this->Paginator->sort('msg_error','Msj Sistema'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($bulkUploadRemmittancesCoffees as $bulkUploadRemmittancesCoffee): ?>
	<tr>
		<td><?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['id']); ?>&nbsp;</td>
                <td><?php echo $this->Html->link(__($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['name_file']), array('action' => 'getTrackingRequest', $bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['name_file'])); ?></td>
		<td><?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['date_reg']); ?>&nbsp;</td>
		<td><?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['count_insert']); ?>&nbsp;</td>
		<td><?php echo h($bulkUploadRemmittancesCoffee['Users']['username']); ?>&nbsp;</td>
		<td><?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['msg_error']); ?>&nbsp;</td>
		<td class="actions">
                    <?php echo $this->Html->link(__('Ver solicitudes'), array('action' => 'view', $bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Registrar'), array('action' => 'add')); ?></li>
	</ul>
</div>
