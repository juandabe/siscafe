<div class="vehiculeInspections form">
<?php echo $this->Form->create('VehiculeInspection'); ?>
	<fieldset>
		<legend><?php echo __('Nueva Inspección de Vehiculo - Descargue de Café'); ?></legend>
	<?php
		echo $this->Form->input('jetty',array('label'=>'Puerto','required'=>'true','empty' => '(seleccione una...)','options'=>array('SPRC'=>'SPRC','CONTECAR'=>'CONTECAR','COMPAS'=>'COMPAS')));
		echo $this->Form->input('mark_vehicule',array('label'=>'Marca Vehiculo','required'=>'true'));
		echo $this->Form->input('place_vehicule',array('label'=>'Placa Vehiculo','required'=>'true'));
		echo $this->Form->input('color_vehicule',array('label'=>'Color Vehiculo','required'=>'true','type'=>'text'));
		echo $this->Form->input('driver_vehicule',array('label'=>'Conductor Vehiculo','required'=>'true'));
		echo $this->Form->input('identification_driver',array('label'=>'Identificación conductor','required'=>'true'));
                echo "\n";
		echo $this->Form->input('q1',array('label'=>'1- ¿Carpa en buen estado?','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q2',array('label'=>'2- ¿Asegurada con sellos de seguridad?','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q3',array('label'=>'3- Escriba los de Sellos de seguridad del vehiculo','required'=>'true'));
		echo $this->Form->input('q4',array('label'=>'4- Corresponde los registrados en los documentos de despacho','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q5',array('label'=>'5- Llegaron todos en buen estado','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q6',array('label'=>'6- Llantas de chasis y remolque','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q7',array('label'=>'7- Tanque de aire','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q8',array('label'=>'8- Tanques de combustibles','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q9',array('label'=>'9- Cabina del motor','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('q10',array('label'=>'10- Escriba los de Sellos de seguridad del vehiculo','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
                echo $this->Form->input('date_in_reten',array('label'=>'Fecha entrada Retén','required'=>'true'));
		echo $this->Form->input('time_check_vehicule',array('label'=>'Hora Inspección Vehiculo','required'=>'true'));
		echo $this->Form->input('date_exit_vehicule',array('label'=>'Fecha salida Vehiculo','required'=>'true'));
		echo $this->Form->input('observation',array('label'=>'Observaciones','required'=>'true'));
		echo $this->Form->input('user_id',array('label'=>'Usuario revisión','type'=>'hidden','required'=>'true'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado de inspecciones'), array('action' => 'index')); ?></li>
	</ul>
</div>
