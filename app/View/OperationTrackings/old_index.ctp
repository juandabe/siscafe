<div class="operationTrackings index">
	<h2><?php echo __('Listado de registros fotograficos - Descargue Café'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Ref Operativa'); ?></th>
			<th><?php echo $this->Paginator->sort('Observacion'); ?></th>
			<th><?php echo $this->Paginator->sort('Trazabilidad FNC?'); ?></th>
                        <th><?php echo $this->Paginator->sort('Sellos vehiculo?'); ?></th>
			<th><?php echo $this->Paginator->sort('Tipo registro'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha Registro'); ?></th>
			<th><?php echo $this->Paginator->sort('Puerto'); ?></th>
			<th><?php echo $this->Paginator->sort('Exportador'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($operationTrackings as $operationTracking): ?>
	<tr>
		<td><?php echo h($operationTracking['OperationTracking']['id']); ?>&nbsp;</td>
		<td><?php echo h($operationTracking['OperationTracking']['ref_text']); ?>&nbsp;</td>
		<td><?php echo h($operationTracking['OperationTracking']['observation']); ?>&nbsp;</td>
		<td><?php echo h($operationTracking['OperationTracking']['trackaing_fnc']); ?>&nbsp;</td>
        <td ><?php echo h($operationTracking['OperationTracking']['break_seal']); ?>&nbsp;</td>
		<td><?php 
			if($operationTracking['OperationTracking']['type_tracking'] ==1){
			  echo "DESCARGUE TRAZABILIDAD";
			}
			else if($operationTracking['OperationTracking']['type_tracking'] ==2){
			  echo "EMBALAJE CAFE";
			}
			else if($operationTracking['OperationTracking']['type_tracking'] ==4){
			  echo "DESCARGUE NOVEDAD";
			}
			else if($operationTracking['OperationTracking']['type_tracking'] ==6){
			  echo "DESCARGUE GRAIN PRO";
			}
			else if($operationTracking['OperationTracking']['type_tracking'] ==7){
			  echo "DESCARGUE SELLOS";
			}
		?>&nbsp;</td>
		<td><?php echo h($operationTracking['OperationTracking']['created_date']); ?>&nbsp;</td>
		<td><?php echo h($operationTracking['OperationTracking']['jetty']); ?>&nbsp;</td>
		<td><?php echo h($operationTracking['Client']['business_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver fotografias'), array('action' => 'view', $operationTracking['OperationTracking']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $operationTracking['OperationTracking']['id'])); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $operationTracking['OperationTracking']['id']), array('confirm' => __('Desea borrar este registro # %s?', $operationTracking['OperationTracking']['id']))); ?>		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Registrar'), array('action' => 'add')); ?></li>
                <li><?php echo $this->Html->link(__('Registros Descargues'), array('action' => 'index',1)); ?> </li>
                <li><?php echo $this->Html->link(__('Registro Embalajes'), array('action' => 'index',2)); ?> </li>
                <li><?php echo $this->Html->link(__('Buscar'), array('action' => 'search')); ?></li>
	</ul>
</div>
