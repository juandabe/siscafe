<div class="operationTrackings form">
<?php echo $this->Form->create('OperationTracking'); ?>
	<fieldset>
		<legend><?php echo __('Nuevo registro fotografico'); ?></legend>
	<?php
		echo $this->Form->input('expo_id',array('label'=>'Exportador','class' => 'chosen-select','options'=>$clients,'required' =>true,'empty' => 'Seleccionar'));
        echo $this->Form->input('lot_caffee', array('label' => 'Lote cafe Ej.: (03450)', 'disabled' => 'enable', 'type' => 'text', "pattern" => "[^ \x22]+[0-9]", "placeholder"=>"Ingrese el Codigo del Lote sin espacios y separado por coma", 'title'=>'Ingrese el Codigo del Lote sin espacios y separado por coma' ));
		echo $this->Form->input('ref_text',array('label'=>'Referencia operativa (Placa Vehiculos)'));
		echo $this->Form->input('observation',array('label'=>'Observaciones Ej.: (001-23450 x 275)','type'=>'textarea','value'=>'NA'));
		echo $this->Form->input('jetty',array('label'=>'Puerto','class' => 'chosen-select','options'=>$portList,'required' =>true,'empty' => 'Seleccionar'));
		echo $this->Form->input('driver_name',array('label'=>'Conductor','value'=>'NA'));
		echo $this->Form->input('driver_id',array('label'=>'Cedula','type'=>'text','value'=>'NA'));
		echo $this->Form->input('item1',array('label'=>'1- � Carpa en buen estado?','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('item2',array('label'=>'2- � Asegurada con sellos de seguridad?','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('seals',array('label'=>'3- Escriba los de Sellos de seguridad del vehiculo'));
		echo $this->Form->input('item4',array('label'=>'4- Corresponde los registrados en los documentos de despacho','options'=>array('NA'=>'NA')));
		echo $this->Form->input('item5',array('label'=>'5- Llegaron todos en buen estado','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('trackaing_fnc',array('label'=>'Trazabilidad FNC?','options'=>array('NO'=>'NO','SI'=>'SI')));
		echo $this->Form->input('type_tracking',array('label'=>'Referencia operativa','options'=>array('1'=>'DESCARGUE TRAZABILIDAD','4'=>'DESCARGUE NOVEDAD','5'=>'DESCARGUE NOVEDAD COPCSA','6'=>'DESCARGUE GRAIN PRO','7'=>'DESCARGUE SELLOS','8'=>'CONEXOS')));
                echo $this->Form->input('ref_send',array('label'=>'Se envio notificacion?','options'=>array('NO'=>'NO','SI'=>'SI')));
		echo $this->Form->input('break_seal',array('label'=>'Sellos vehiculos?','options'=>array('NO'=>'NO','SI'=>'SI')));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado Descargue'), array('action' => 'index',1)); ?></li>
	</ul>
</div>
<script>
	$(".chosen-select").chosen();

document.querySelector("#OperationTrackingLotCaffee").addEventListener("keyup", function(event){
	if (event.keyCode == 32 || event.keyCode == 190 || event.keyCode == 189 || event.keyCode == 55 || event.keyCode == 220){  alert("Error: letra, simbolo o espacio en blanco no es valido."); } }
, false);


            $('form').submit(function() {
                $( ".submit" ).hide();
            });

</script>