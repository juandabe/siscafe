<div class="operationTrackings form">
    <table>
        <h2><?php echo __('Busqueda Trazabilidad Vehiculos Descargue - (Registro fotografico)'); ?></h2>
        <tr>
            <td><?php echo $this->Form->input('placaCar',array('label' => 'Placa Vehiculo Ej.: SXE567', 'type'=>'text')); ?></td>
            <td style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="findByPlateCar();">Buscar...</a></h3></td>
        </tr>
        <tr>
            <td><?php echo $this->Form->input('lotCoffee',array('label' => 'Lote Café Ej.: 001-00054', 'type'=>'text')); ?></td>
            <td style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="findByLotCoffee();">Buscar...</a></h3></td>
        </tr>
    </table>
    <div>
        <h2><?php echo __('Resultado de busqueda'); ?></h2>
            <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('Ref Operativa'); ?></th>
                            <th><?php echo $this->Paginator->sort('Observación'); ?></th>
                            <th><?php echo $this->Paginator->sort('Sellos'); ?></th>
                            <th><?php echo $this->Paginator->sort('Conductor'); ?></th>
                            <th><?php echo $this->Paginator->sort('Cedula'); ?></th>
                            <th><?php echo $this->Paginator->sort('Fecha Registro'); ?></th>
                            <th><?php echo $this->Paginator->sort('Puerto Maritimo'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
            </thead>
            <tbody id="tableTrackings">
                </tbody>
            </table>
    </div>
</div>

<script type="text/javascript">
    
    function findByPlateCar() {
        var plateCar = $("#placaCar").val();
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/OperationTrackings/findByPlate/" + plateCar,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var trackingData = JSON.parse(data);
                        //console.log(trackingData);
                        addTrackingDataToTable(trackingData);
                    }
                    else{
                        alert("No se encontro NINGUN registro");
                        $('#tableTrackings').children().remove();
                    }
                }
            });
    }
    
    function findByLotCoffee() {
        var lotCoffee = $("#lotCoffee").val();
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/OperationTrackings/findBylotCoffee/" + lotCoffee,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var trackingData = JSON.parse(data);
                        //console.log(trackingData);
                        addTrackingDataToTable(trackingData);
                    }
                    else{
                        alert("No se encontro NINGUN registro");
                        $('#tableTrackings').children().remove();
                    }
                }
            });
    }
    
    function addTrackingDataToTable(trackingData) {
            $('#tableTrackings').children().remove();
            $.each(trackingData, function (i, value) {
                trHTML = '<tr>';
                trHTML += '<td>' + value['OperationTracking']['id'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['ref_text'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['observation'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['seals'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['driver_name'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['driver_id'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['created_date'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['jetty'] + '</td>';
                trHTML += '<td class="actions">'+'<a href="/OperationTrackings/view/'+value['OperationTracking']['id']+'" target="_blank">Fotografias </a></td>';
                trHTML += '</tr>';
                $('#tableTrackings').append(trHTML);
            });
        }
 </script>

<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Registrar'), array('action' => 'add')); ?></li>
                <li><?php echo $this->Html->link(__('Registros Descargues'), array('action' => 'index',1)); ?> </li>
                <li><?php echo $this->Html->link(__('Buscar'), array('action' => 'search')); ?></li>
	</ul>
</div>

