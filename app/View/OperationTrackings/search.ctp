<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>
<div class="operationTrackings form">
    <table>
        <h2><?php echo __('Busqueda Trazabilidad Vehiculos Descargue - (Registro fotografico)'); ?></h2>
        <tr>
            <td><?php echo $this->Form->input('placaCar',array('label' => 'Placa Vehiculo Ej.: SXE567', 'type'=>'text')); ?></td>
            <td style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="findByPlateCar();">Buscar...</a></h3></td>
        </tr>
        <tr>
            <td><?php echo $this->Form->input('lotCoffee',array('label' => 'Lote Café Ej.: 001-00054', 'type'=>'text')); ?></td>
            <td style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="findByLotCoffee();">Buscar...</a></h3></td>
        </tr>
    </table>
    <div>
        <h2><?php echo __('Resultado de busqueda'); ?></h2>
            <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('Ref Operativa'); ?></th>
                            <th><?php echo $this->Paginator->sort('Observación'); ?></th>
                            <th><?php echo $this->Paginator->sort('Sellos'); ?></th>
                            <th><?php echo $this->Paginator->sort('Conductor'); ?></th>
                            <th><?php echo $this->Paginator->sort('Cedula'); ?></th>
                            <th><?php echo $this->Paginator->sort('Fecha Registro'); ?></th>
                            <th><?php echo $this->Paginator->sort('Puerto Maritimo'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
            </thead>
            <tbody id="tableTrackings">
                </tbody>
            </table>
    </div>
</div>

<script type="text/javascript">
    
    function findByPlateCar() {
        var plateCar = $("#placaCar").val();
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/OperationTrackings/findByPlate/" + plateCar,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var trackingData = JSON.parse(data);
                        //console.log(trackingData);
                        addTrackingDataToTable(trackingData);
                    }
                    else{
                        alert("No se encontro NINGUN registro");
                        $('#tableTrackings').children().remove();
                    }
                }
            });
    }
    
    function findByLotCoffee() {
        var lotCoffee = $("#lotCoffee").val();
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/OperationTrackings/findBylotCoffee/" + lotCoffee,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var trackingData = JSON.parse(data);
                        //console.log(trackingData);
                        addTrackingDataToTable(trackingData);
                    }
                    else{
                        alert("No se encontro NINGUN registro");
                        $('#tableTrackings').children().remove();
                    }
                }
            });
    }
    
    function addTrackingDataToTable(trackingData) {
            $('#tableTrackings').children().remove();
            $.each(trackingData, function (i, value) {
                trHTML = '<tr>';
                trHTML += '<td>' + value['OperationTracking']['id'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['ref_text'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['observation'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['seals'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['driver_name'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['driver_id'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['created_date'] + '</td>';
                trHTML += '<td>' + value['OperationTracking']['jetty'] + '</td>';
                trHTML += '<td class=\"actions\">' + 
                    '<div class=\"dropdown\">' + 
                    '<button class=\"dropbtn\"><strong>Opciones</strong></button>'+
                    '<div class=\"dropdown-content\">';
                trHTML += '<a href="/OperationTrackings/view/'+value['OperationTracking']['id']+'" target="_blank">FT </a><';
                trHTML += '<a href="https://siscafe.copcsa.com/SISCAFE-PrintWS/ws/print?remittancesCaffeeId='+value['OperationTracking']['id']+'&idCertificado=0&idReport=5&toPdf=true&idPrinter=1\' target="_blank">PDF</a>';
                trHTML += '</div></div>';
                $('#tableTrackings').append(trHTML);
            });
        }
 </script>

<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Registrar'), array('action' => 'add')); ?></li>
                <li><?php echo $this->Html->link(__('Registros Descargues'), array('action' => 'index',1)); ?> </li>
                <li><?php echo $this->Html->link(__('Buscar'), array('action' => 'search')); ?></li>
	</ul>
</div>

