<div class="coffeeSamples index">
	<h2><?php echo ('Listado de Valores de solicitudes (Muestras, Trazabilidades)') ?></h2>
   
        <table id="tableData" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id','Id Muestra'); ?></th>
                        <th><?php echo $this->Paginator->sort('lot_coffee_ref','Ref lote'); ?></th>
                        <th><?php echo $this->Paginator->sort('qta_bag','Cant. Sacos'); ?></th>
                        <th><?php echo $this->Paginator->sort('client_id','Exportador'); ?></th>
			
			<th><?php echo $this->Paginator->sort('type_sample_coffe_id','Tipo muestra'); ?></th>
			<th><?php echo $this->Paginator->sort('status','Estado'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
                        <th><?php echo $this->Paginator->sort('bill','Valor *(COP)'); ?></th>
			<th><?php //echo $this->Paginator->sort('register_users_id','Registro usuario'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($coffeeSamples as $coffeeSample): ?>
	<tr>
		<td><?php 
                //debug($coffeeSample);exit;
                echo h($coffeeSample['CoffeeSample']['id']); ?>&nbsp;</td>
                <td><?php echo h('3-'.$coffeeSample['CoffeeSample']['lot_coffee_ref']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['CoffeeSample']['qta_bag']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['Client']['business_name']); ?>&nbsp;</td>
		
		<td>
			<?php 
                        $dataTypeCoffeeSample="";
                        foreach($coffeeSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                            $key = $typeSampleCoffe['type_sample_coffee_id'];
                            $dataTypeCoffeeSample .= $typeSampleCoffes[$key]." ";
                        }
                        echo $dataTypeCoffeeSample; ?>
		</td>
                
                <td id="tdStatus"><?php echo h($coffeeSample['CoffeeSample']['status']); ?>&nbsp;</td>
		<td><?php echo h($coffeeSample['CoffeeSample']['created_date']); ?>&nbsp;</td>
                <td><?php echo "$ ".number_format($coffeeSample['CoffeeSample']['qta_bag']*170,0); ?>&nbsp;</td>
		<td><?php //echo h($coffeeSample['CoffeeSample']['register_users_id']); ?>&nbsp;</td>

	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
        
        
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<script type="text/javascript">
    
    tdChangeColor();
    refreshPage();
    
    function tdChangeColor(){
        var col;
        $("tr td:nth-child(6)").each(function(i) {
            col = $(this);
            if(col.html() == "COMPLETADA&nbsp;"){
                col.css('background', 'yellow');
            }
            else if(col.html() == "REGISTRADA&nbsp;"){
                col.css('background', 'red');
            }
            else if(col.html() == "CERRADA&nbsp;"){
                col.css('background', 'green');
            }
        })
    }
    
    function refreshPage(){
        var interval = setInterval(function() {
            location.reload();
        }, 30000);
    }
</script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Registrar solicitudes'), array('controller'=>'ChargeMassiveSampleCoffees','action' => 'index', $this->Session->read('User.id'))); ?></li>
                <li><?php echo $this->Html->link(__('Buscar solicitud'), array('controller'=>'CoffeeSamples','action' => 'search')); ?></li>
                <li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',$this->Session->read('User.id'))); ?> </li>
	</ul>
</div>
