    <?php
    
    echo $this->Form->create('DetailsServicesToCaffee',array('type' => 'file'));
    ?>
    <fieldset>
        <legend><?php echo __('Agregar servicios a la orden ' . $serviceOrder['ServicesOrder']['id']); ?></legend>
            <table>
                <tr>
                    <td><?php echo $this->Form->input('remittance_id',array('label' => 'Remesa', 'type'=>'text')); ?></td>
                    <td><?php echo $this->Form->input('lot',array('label' => 'Lote', 'type'=>'text')); ?></td>
                    <td><?php echo $this->Form->input('cargolot_id',array('label' => 'Cargo Lot Id', 'type'=>'text')); ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><?php //echo $this->Form->input('exporter_code',array('label' => 'Exportador Codigo', 'type'=>'text')); ?></td>
                    <td><?php //echo $this->Form->input('exporter_name',array('label' => 'Exportador', 'disabled' => 'disabled')); ?></td>
                </tr>
            </table>
        <h2><?php echo __('Resultado de la busqueda'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date','Fecha Creación'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags','Sacos Rad In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
        </table>
    </fieldset>
        <table>
            <tr>
                <td><?php
                    echo $this->Form->input('services_orders_id', array('type' => 'hidden', 'value' => $serviceOrder['ServicesOrder']['id']));
                    echo $this->Form->input('remittances_caffee_id', array('type' => 'hidden'));
                    echo $this->Form->input('expo_code', array('type' => 'hidden', 'required' => true, 'value' => $serviceOrder['Client']['exporter_code']));
                    echo $this->Form->input('remittances_caffee', array('label' => 'Remesa asociada', 'disabled' => 'disabled', 'type' => 'text'));
                    ?></td>
                <td><?php echo $this->Form->input('qta_bag_to_work', array('label' => 'Cantidad de unidades', 'required' => true)); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('service_package', array('label' => 'Servicio', 'required' => true, 'options'=>$servicePackages, 'empty'=>'Seleccione', 'class' => 'chosen-select')); ?></td>
                <td><?php echo $this->Form->input('lock_remesa', array('label' => 'Bloquear CargoLotId (Remesa)','type' => 'checkbox')); ?></td>
            </tr>
            <tr>
                <td class="actions">
                    <?php echo $this->Html->link(__('Guardar'), 'javascript:void(0)',array('onclick'=> 'add_detail('.$serviceOrder['ServicesOrder']['id'].')'));?>
                    <?php echo $this->Html->link(__('Ver'), 'javascript:void(0)',array('onclick'=> 'view_modal('.$serviceOrder['ServicesOrder']['id'].')'));?>
                </td>
            </tr>
            
        </table>
    </fieldset>
    <?php echo $this->Form->end(); ?>
    
<script type="text/javascript">
    
    function add_detail(id) {
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'detailsServicesToCaffees', 'action' => 'add']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    alert(JSON.parse(data).msj);
                },
                type: "post",
                data: $("#DetailsServicesToCaffeeViewForm").serialize()
        });
    }

    function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'ServicesOrders', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }
    
    var config = {
     '.chosen-select'           : {},
     '.chosen-select-deselect'  : {allow_single_deselect:true},
     '.chosen-select-no-single' : {disable_search_threshold:10},
     '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
     '.chosen-select-width'     : {width:"95%"}
    };

    $(".chosen-select").chosen(config);
    
    $("#DetailsServicesToCaffeeRemittanceId").keyup(function () {
        findRemittancesByRemittanceIdByClient($(this).val());
    });
    
    $("#DetailsServicesToCaffeeLot").keyup(function () {
        findRemittancesByLotByClient($(this).val());
    });
    
    $("#DetailsServicesToCaffeeCargolotId").keyup(function () {
        findRemittancesByCargoLotByClient($(this).val());
    });
        
    function findRemittancesByCargoLotByClient(cargolot){
        var client = $("#DetailsServicesToCaffeeExpoCode").val();
        var clientCargoLot = client+"_"+cargolot;
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByCargoLotIdClient/" + clientCargoLot,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function findRemittancesByLotByClient(lotCoffee){
        var client = $("#DetailsServicesToCaffeeExpoCode").val();
        var clientLotCoffee = client+"-"+lotCoffee;
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findByLotClient/" + clientLotCoffee,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function findRemittancesByRemittanceIdByClient(remittance){
        var client = $("#DetailsServicesToCaffeeExpoCode").val();
        var remittanceClient = remittance+"-"+client;
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findByRemittancesClient/" + remittanceClient,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function addRemittanceDataToTable(remittancesData) {
        $('#tableRemittances').children().remove();
        $.each(remittancesData, function (i, value) {
            trHTML = '<tr>';
            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
            trHTML += '<td>' + '3-'+value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee'] + '</td>';
            trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
            trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_in_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_out_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            var remittanceId = value['RemittancesCaffee']['id'];
            trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="addRemittance(\'' + remittanceId.toString() + '\');">Asociar</a></td>';
            trHTML += '</tr>';
            $('#tableRemittances').append(trHTML);
        });
    }
    
    function addRemittance(remittanceId) {
        $('#DetailsServicesToCaffeeRemittancesCaffee').val("");
        $('#DetailsServicesToCaffeeRemittancesCaffeeId').val("");
        $('#DetailsServicesToCaffeeQuantityRadicatedBagOut').val("");
        $('#DetailsServicesToCaffeeRemittancesCaffee').val(remittanceId);
        $('#DetailsServicesToCaffeeRemittancesCaffeeId').val(remittanceId);
    }

</script>

