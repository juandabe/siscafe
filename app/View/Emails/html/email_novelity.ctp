<p>Estimados, Cordial saludo. </p>
</br>
</br>
<p> Me permito informar novedad presentada durante el descargue del vehículo con placa <?= $vehicle_plate ?> lote 3-<?= $exporter ?>-<?= $lot_coffee ?>: </p>
</br>
<textarea style="border: 0px; height: 100px; width: 90%;" disabled>Observación: <?= $observation ?></textarea>
</br></br>
<p style="padding-top: 30px"><strong> NOTA: REVISAR REGISTRO FOTOGRAFICO EN NUESTRO PORTAL PILCAF, ID <?= $id ?></strong></p>
<a href="https://siscafe.copcsa.com/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=<?= $id ?>&idCertificado=0&idReport=5&toPdf=true&idPrinter=1">Descargar Novedad PDF</a>
<p>Nota: Este reporte se construye en tiempo real, por lo tanto, las fotografias de la operacion pueden ir variando. Para mayor disponibilidad consulte el anterior enlace una hora despues a este mensaje.
</br>
<p style="padding-top: 50px">Quedamos atentos a sus comentarios. </p>
</br>
Equipo de operaciones nacional.
<a href="https://www.copcsa.com">www.copcsa.com</a>
