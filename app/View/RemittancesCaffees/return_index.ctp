<div class="remittancesCaffees index">
    <h2><?php echo __('Listado de Remesas a Devolver'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id','CO-Remesa'); ?></th>
				<th><?php echo $this->Paginator->sort('jetty','PUERTO'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('return_id','Id devolución'); ?></th>
                <th><?php echo $this->Paginator->sort('date_return','Fecha devolución'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos Bod'); ?></th>
                <th><?php echo $this->Paginator->sort('qta_bags','Sacos X Devolver'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                <th><?php echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees['data'] as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                //debug($remittancesCaffee);exit;
                echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
				<td><?php 
                    echo h($remittancesCaffee['ReturnsCaffee']['Departament']['name']);
                ?>&nbsp;</td>
                <td><?php echo h('3-'.$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php 
                    echo h($remittancesCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id']);
                ?>&nbsp;</td>
                <td><?php 
                    echo h($remittancesCaffee['ReturnsCaffee']['return_date']);
                ?>&nbsp;</td>
                <td style="background-color: #266ef1;"><span style="font-size: 15px ;color: black;"><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</span></td>
                <td style="background-color: #eef126;"><span style="font-size: 15px ;color: black;"><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</span></td>
                <td style="background-color: #f16526;"><span style="font-size: 15px ;color: black;"><?php 
                    echo h($remittancesCaffee['RemittancesCaffeeReturnsCaffee']['qta_bags']);
                ?>&nbsp;</span></td>
                <td style="background-color: #3cd641;"><span style="font-size: 15px ;color: black;"><?php 
                $id_detalle=0;
                $bag_return;
                foreach($remittancesCaffees['cantBag'] as $cantBagsByIdDetalle){
                    if($id_detalle != $remittancesCaffee['RemittancesCaffeeReturnsCaffee']['id']){
                        $id_detalle = $remittancesCaffee['RemittancesCaffeeReturnsCaffee']['id'];
                    }
                    echo isset($cantBagsByIdDetalle[$id_detalle]) ? $cantBagsByIdDetalle[$id_detalle] : null;
                }
                ?>&nbsp;</span></td>
                <td><?php 
                    echo h($remittancesCaffee['ReturnsCaffee']['vehicle_plate']);
                ?>&nbsp;</td>
                <td class="actions">
			<?php 
                        echo $this->Html->link(__('Pesar'), array('action' => 'return_pallet', '?'=>['detalle_id' => $remittancesCaffee['RemittancesCaffeeReturnsCaffee']['id']])); ?>
			<?php 
                        echo $this->Html->link(__('Tara'), array('action' => 'tare3', '?' =>['return_id'=>$remittancesCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id'],'remittances_coffee_id'=>$remittancesCaffee['RemittancesCaffee']['id']])); ?>
			<?php echo $this->Html->link(__('Finalizar'), array('action' => 'return_final', $remittancesCaffee['RemittancesCaffeeReturnsCaffee']['id']),array('confirm'=>('Desea finalizar proceso de devolución de la remesa '.$remittancesCaffee['RemittancesCaffee']['id'].'?'))); ?>
            </tr>
<?php endforeach;?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'basculero')); ?></li>
        <li><?php //echo $this->Html->link(__('Buscar Remesa'), array('controller'=>'pages','action' => 'motorista')); ?></li>
    </ul>
</div>
