<div class="remittancesCaffees index">
    <h2><?php echo __('Asignación de Muestras - lotes descargados'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('remittances_count','Cant. fracciones'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos In'); ?></th>
                <th><?php echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                <th><?php echo $this->Paginator->sort('client','Exportador'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Fecha Descargado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                //echo debug($remittancesCaffee);exit;
                echo h("3-".$remittancesCaffee['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee[0]['remittances_count']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee[0]['bags_sum']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee[0]['weight_sum']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['Client']['business_name']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>

                <td class="actions">
			<?php 
                            if($departaments_id ==1){
                                //echo $this->Html->link(__('Ver Trazabilidad'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=2&toPdf=true&idPrinter=1"); 
                            }
                            else if($departaments_id ==2){
                                echo $this->Html->link(__('Asignar muestra'), array('controller'=>'CoffeeSamples','action' => 'add', $remittancesCaffee['RemittancesCaffee']['lot_caffee'].'-'.$remittancesCaffee['RemittancesCaffee']['client_id']."-0"));                                
                            }
                            
                        ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Pre-asignar muestra'), array('controller'=>'CoffeeSamples','action' => 'preassignment')); ?></li>
        <li><?php echo $this->Html->link(__('Listado de muestras'), array('controller'=>'CoffeeSamples','action' => 'index',1)); ?></li>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'colaborador')); ?></li>
    </ul>
</div>
