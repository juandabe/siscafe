<style>
	a.disabled {
		pointer-events: none;
		color: green;
	}
	span.disable-links {
		pointer-events: none;
	}
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
    
</style>
</style>
<div class="fumigationServices index">

	<h2><?php echo __('Listado de servicios de fumigación'); ?></h2>
	<?php //echo $this->Html->link(__('Nueva fumigación'), array('action' => 'add')); ?>
        <table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('Id Certificado'); ?></th>
			<th><?php echo $this->Paginator->sort('ICA?'); ?></th>
            <th><?php echo $this->Paginator->sort('Cafe Solicitado'); ?></th>
			<th><?php echo $this->Paginator->sort('Cafe Descargado'); ?></th>
			<th><?php echo $this->Paginator->sort('Estado'); ?></th>
            <th><?php echo $this->Paginator->sort('Lotes Descargados'); ?></th>
			<th><?php echo $this->Paginator->sort('Lotes Programados'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha inicio'); ?></th>
			<th><?php echo $this->Paginator->sort('Finalizado'); ?></th>
			<th><?php echo $this->Paginator->sort('Modalidad'); ?></th>
			<th><?php echo $this->Paginator->sort('Puerto'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($fumigationServices as $fumigationService): ?>
	<tr>
		<td><?php echo h($fumigationService['FumigationService']['id']); ?>&nbsp;</td>
			
			<?php if($fumigationService['FumigationService']['ica']){
				echo "<td style=\"background: green\"><span>ICA</span></td>";
			}
			else{
				echo "<td style=\"background: red\"><span>SIN ICA</span></td>";
			}
			?>
		<td><?php echo h($fumigationService['FumigationService']['qta_coffee_request']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['qta_bags']); ?>&nbsp;</td>
		<?php  if($fumigationService['HookupStatus']['id'] == 2) {
				echo "<td style=\"background: red\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			}
			else if($fumigationService['HookupStatus']['id'] == 1) {
				echo "<td style=\"background: blue\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			}
			else if($fumigationService['HookupStatus']['id'] == 3) {
				echo "<td style=\"background: yellow\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			}
			else if($fumigationService['HookupStatus']['id'] == 4) {
				echo "<td style=\"background: green\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			} 
			else if($fumigationService['HookupStatus']['id'] == 5) {
				echo "<td style=\"background: #bbbbbb\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			} 
			
			?>
				<td class="actions">
                <?php 
					$tmp;
					$color;
                    $current="";
                    foreach ($fumigationService['RemittancesCaffeeHasFumigationService'] as $remittancesCaffee):
                        $tmp = ($tmp = ($remittancesCaffee['RemittancesCaffee']['id']));
                        if($current != $tmp){
							$row = "3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code'].
							"-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']." x " .$remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'];
							echo $row."<br>";
                            $current = $tmp;
						}
						
                    ?>
				<?php endforeach; 
				if(empty($fumigationService['RemittancesCaffeeHasFumigationService'])){
					echo h("Lotes aun sin descargar");
				}
				?>
				</td>
				<td>
				<?php 
                    $tmp;
                    $current="";
                    foreach ($fumigationService['ScheduleCoffeeFumigation'] as $scheduleCoffeeFumigation):
                        //debug($remittancesCaffee);exit;
                        $tmp = $scheduleCoffeeFumigation['lot_coffee'];
                        if($current != $tmp){
                            echo h($tmp." ; ")."<br>";
                            $current = $tmp;
                        }
                    ?>
				<?php endforeach; 
				if(empty($fumigationService['ScheduleCoffeeFumigation'])){
					echo h("Sin preaviso");
				}
				?>
                </td>
		<td><?php echo h($fumigationService['FumigationService']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['finished_date'] != NULL ? $fumigationService['FumigationService']['finished_date'] : "SIN COMPLETAR"); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['Departament']['name']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['modality']); ?>&nbsp;</td>
		<td class="actions">
            <div class="dropdown">
               <button class="dropbtn"><strong>Menú</strong></button>
                    <div class="dropdown-content">
					<?php
						echo $this->Html->link(__('Ver'), 'javascript:void(0)',array('onclick'=> 'view_modal('.$fumigationService['FumigationService']['id'].')'));
                        echo $this->Html->link(__('Fotos'), array('controller' => 'OperationTrackings','action' => 'addfumigation', $fumigationService['FumigationService']['id'])); 
                        

			if($fumigationService['FumigationService']['ica'] == 1){
                           echo $this->Html->link(__('Archivo PDF'), array('controller' => 'FumigationServices','action' => 'getTrackingRequest', $fumigationService['FumigationService']['id']));
                        }

			if($fumigationService['FumigationService']['hookup_status_id'] == 2){
                           echo $this->Html->link(__('Procesar'), array('controller' => 'FumigationServices','action' => 'process', $fumigationService['FumigationService']['id'])); 
                        }
                        else if($fumigationService['FumigationService']['hookup_status_id'] == 3){
                                echo $this->Html->link(__('Completar'), array('controller' => 'FumigationServices','action' => 'complete', $fumigationService['FumigationService']['id'])); 
                        }
                        else if($fumigationService['FumigationService']['hookup_status_id'] == 4){
                            echo $this->Html->link(__('Certificado'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=0&idCertificado=".$fumigationService['FumigationService']['id']."&idReport=3&toPdf=true&idPrinter=1");
                        }
                    ?>
             </div>
        </div>
      </td>
	</tr>
<?php endforeach;?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->first('' . __('Primero'));
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
		echo $this->Paginator->last(__('Ultimo') . '', array(), null, array('class' => ''));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Ordenes Servicios'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar Fumigacion'), array('controller'=>'FumigationServices', 'action' => 'searchServices')); ?></li>
                <li><?php echo $this->Html->link(__('Fumigacion Completada'), array('controller'=>'FumigationServices', 'action' => 'completed')); ?></li>
		<li><?php echo $this->Html->link(__('Cafe a fumigar'), array('controller'=>'ScheduleCoffeeFumigations', 'action' => 'index')); ?></li>
	</ul>
</div>
<div id="modal">
    <div class="modal-body"></div>
</div>
<script type="text/javascript">

    function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'FumigationServices', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

	$(function() {
            $('#modal').dialog({
                autoOpen: false,
                modal: true,
                height: "auto",
                width: "auto",
                resizable: false,
                show: {
                  effect: "blind",
                  duration: 100
                },
                hide: {
                  effect: "explode",
                  duration: 100
                }
              });
	});

</script>