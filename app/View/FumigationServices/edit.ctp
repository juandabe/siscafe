<div class="fumigationServices form">
<?php echo $this->Form->create('FumigationService'); ?>
	<fieldset>
		<legend><?php echo __('Edit Fumigation Service'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('product_fumigation');
		echo $this->Form->input('quantity_poison_used');
		echo $this->Form->input('start_date');
		echo $this->Form->input('finished_date');
		echo $this->Form->input('remittances_caffee_id');
		echo $this->Form->input('volumen_fumigated');
		echo $this->Form->input('resquest_service_date');
		echo $this->Form->input('resquest_certificate_date');
		echo $this->Form->input('observation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FumigationService.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('FumigationService.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Fumigation Services'), array('action' => 'index')); ?></li>
	</ul>
</div>
