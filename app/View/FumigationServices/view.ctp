

<div class="fumigationServices">
<h2><?php echo __('Ficha fumigacion de Cafe'); ?></h2>
	<?php echo $this->Form->create('FumigationService'); ?>
	<fieldset>
		<?php echo $this->Form->input('client',array('label' => 'Exportador','disabled' => 'disabled','value'=>$FumigationService['Client']['exporter_code']."-".$FumigationService['Client']['business_name']));?>
	        <?php echo $this->Form->input('dosis',array('label' => 'Dosis','disabled' => 'disabled','value'=>$FumigationService['FumigationService']['dosis']));?>
		<?php echo $this->Form->input('hour_elevator',array('label' => 'Hora elevador','value'=>$FumigationService['FumigationService']['hour_elevator']));?>
	
	<div class="actions">
	<?php echo $this->Html->link(__('Guardar'), 'javascript:void(0)',['onclick'=> 'setHour('.$FumigationService['FumigationService']['id'].')']);?>
	</div>
	</fieldset>
	<?php echo $this->Form->end(); ?>
</div>

<div class="fumigationServices">
<h2><?php echo __('Detalles fumigacion'); ?></h2>
	<form id=formFinishedDate>
	<fieldset>
		<?php echo $this->Form->input('finished_date',array('label' => 'Fecha finalizacion','type'=>'datetime','value'=>$FumigationService['FumigationService']['finished_date']));?>
	<div class="actions">
	<?php echo $this->Html->link(__('Actualizar'), 'javascript:void(0)',['onclick'=> 'changeFinishedDate('.$FumigationService['FumigationService']['id'].')']);?>
	</div>
	</fieldset>
    </form>
</div>

<p>
<h2><?php echo __('Cafe descargado'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Id'); ?></th>
                <th><?php echo $this->Paginator->sort('Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('Lote Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('Cant. Cafe'); ?></th>
                <th class="actions"><?php echo __('--'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($FumigationService['RemittancesCaffeeHasFumigationService'] as $detailFumigationService): ?>
                <tr>
                <td><?php 
                //debug($detailsServicesToCaffees);exit;
                	echo h($detailFumigationService['id']); ?>&nbsp;</td>
                    <td><?php echo h($detailFumigationService['remittances_caffee_id']); ?>&nbsp;</td>
                    <td><?php echo h($FumigationService['Client']['exporter_code'] . "-" . $detailFumigationService['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                    <td><?php echo h($detailFumigationService['qta_coffee']); ?>&nbsp;</td>
                    <td class="actions">
					<?php 
						if(!$detailFumigationService['status']){
                        	echo $this->Html->link(__('Completar'), 'javascript:void(0)',array('onclick'=> 'complete('.$detailFumigationService['id'].')',array('confirm' => __('¿Esta seguro de completar este trabajo ?'))));
						}
                        else{
                            echo "Finalizado";
                        }
                    ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
</table>

<script>

    $(".datetimepicker").each(function () {
        $(this).datetimepicker();
    })

function setHour(id) {
    $.ajax({
        async: true,
            url: '<?= Router::url(['controller' => 'FumigationServices', 'action' => 'setHour']) ?>'+'/'+id,
            dataType: "html",
            success: function (data, textStatus) {
                alert(JSON.parse(data).response);
            },
            type: "post",
            data: $("#FumigationServiceViewForm").serialize()
    });
}

function changeFinishedDate(id) {
    $.ajax({
        async: true,
            url: '<?= Router::url(['controller' => 'FumigationServices', 'action' => 'changeFinishedDate']) ?>'+'/'+id,
            dataType: "html",
            success: function (data, textStatus) {
                alert(JSON.parse(data).response);
            },
            type: "post",
            data: $("#formFinishedDate").serialize()
    });
}

function complete(id) {
    $.ajax({
        async: true,
            url: '<?= Router::url(['controller' => 'FumigationServices', 'action' => 'completeDetail']) ?>'+'/'+id,
            dataType: "html",
            success: function (data, textStatus) {
                alert(JSON.parse(data).response);
                view_modal(JSON.parse(data).id);
            },
            type: "post"
    });
}

function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'FumigationServices', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

$( "#modal" ).dialog({
        close: function( event, ui ) {
            window.location.href = '<?= Router::url($this->request->referer()) ?>';
        }
    }).dialog({
        position: {
            my: "center center",
            at: "center center",
            of: window
        }
    });
</script>