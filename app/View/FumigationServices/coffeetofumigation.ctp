
<style>
a.disabled {
    pointer-events: none;
	color: green;
}
span.disable-links {
    pointer-events: none;
}
</style>
<div class="fumigationServices index">

	<h2><?php echo __('Listado de lotes cafe programados para fumigar'); ?></h2>
        <table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('Id fumigacion'); ?></th>
			<th><?php echo $this->Paginator->sort('Lotes'); ?></th>
			<th><?php echo $this->Paginator->sort('Cta Cafe'); ?></th>
            <th><?php echo $this->Paginator->sort('Fecha reg'); ?></th>
			<th><?php echo $this->Paginator->sort('Cta descargada'); ?></th>
			<th><?php echo $this->Paginator->sort('Cta programada'); ?></th>
			<th><?php echo $this->Paginator->sort('Estado fumigacion'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($fumigationServices as $fumigationService): ?>
	<tr>
	<td><?php 
			debug($fumigationService);exit;
                echo h($fumigationService['FumigationService']['id']); ?>&nbsp;
		</td>
		<td><?php 
                echo h($fumigationService['ScheduleCoffeeFumigation']['lot_coffee']); ?>&nbsp;
		</td>
		<td><?php 
                echo h($fumigationService['ScheduleCoffeeFumigation']['qta_coffee']); ?>&nbsp;
		</td>
		<td><?php 
                echo h($fumigationService['ScheduleCoffeeFumigation']['reg_date']); ?>&nbsp;
		</td>
		<td><?php 
                echo h($fumigationService['FumigationService']['qta_bags']); ?>&nbsp;
		</td>
		<td><?php 
                echo h($fumigationService['FumigationService']['qta_coffee_request']); ?>&nbsp;
		</td>
		<td><?php 
                echo h($fumigationService['HookupStatus']['status_name']); ?>&nbsp;
		</td>
		<td class="actions">
			
		</td>
	</tr>
<?php endforeach;?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Ordenes Servicios'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar Servicios'), array('controller'=>'FumigationServices', 'action' => 'searchServices')); ?></li>
				<li><?php echo $this->Html->link(__('Cafe a fumigar'), array('controller'=>'FumigationServices', 'action' => 'coffeetofumigation')); ?></li>
	</ul>
</div>
