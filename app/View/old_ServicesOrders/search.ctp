
<div class="fumigationServices index">
<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
    
</style>
<?php 
echo $this->Form->create('ServicesOrder');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda de Ordenes de servicio '); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('text_search',array('label' => 'Criterio de busqueda'));?></td>
            </tr>  
        </table>
      <br>

    </fieldset>
    
        <table id="tbDataOie">
            <thead>
                <tr>
                <th><?php echo $this->Paginator->sort('ID'); ?></th>
                <th><?php echo $this->Paginator->sort('Exportador'); ?></th>
                <th><?php echo $this->Paginator->sort('Estado'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha reg'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha cierre'); ?></th>
                <th><?php echo $this->Paginator->sort('Lotes descargardo'); ?></th>
                <th><?php echo $this->Paginator->sort('Lotes preavisados'); ?></th>
                <th><?php echo $this->Paginator->sort('Suministros - Otro servicios'); ?></th>
                <th><?php echo $this->Paginator->sort('Cta Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('Puerto'); ?></th>
                <th class="actions"><?php echo __('--'); ?></th>
                </tr>
            </thead>
            <tbody id="tableResult">
            </tbody>
        </table>
                   

</div>

<div id="modal">
    <div class="modal-body"></div>
</div>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
<script type="text/javascript">

    $(function() {  
        $('#modal').dialog({
            autoOpen: false,
            modal: true,
            height: "auto",
            width: "auto",
            resizable: false,
            show: {
                effect: "blind",
                duration: 100
            },
            hide: {
                effect: "explode",
                duration: 100
            }
        });
	});

    $("#ServicesOrderTextSearch").keyup(function () {
        findServicesOrder($(this).val());
    });

    function findServicesOrder(textSearch) {
        if(textSearch.length>2){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "<?= Router::url(['controller' => 'ServicesOrders', 'action' => 'ajaxSearch']) ?>" + "/" + textSearch,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data != "") {
                        var resultSearch = JSON.parse(data);
                        addDataToTable(resultSearch);
                    }
                    else{
                        $('#tableResult').children().remove();
                        $('#tableResult').append('');
                    }
                }
            });
        }
    }

    function getCtaCoffee(value){
        var total=0;
        $.each(value['ServicesOrdersBilling'], function (i, row) {
            total += Number(row['qty']);
        });
        return total;
    }

    function getLotsWtithConexos(value){
        var msg="<ul>";
        $.each(value['ServicesOrdersBilling'], function (i, row) {
            if(row['lot_caffee'].length > 1){
                msg += "<li>" + row['observation'] +"</li><br>";
            }
        });
        msg+="</ul>"
        return msg;
    }

    function getLotCrossdocking(value){
        var msg="<ul>";
        $.each(value['ServicesOrdersBilling'], function (i, row) {
            if(row['lot_caffee']== 9){
                msg += "<li>" + row['observation'] +"</li><br>";
            }
        });
        msg+="</ul>"
        return msg;
    }

    function getSupplyOtherServices(value){
        var msg="<ul>";
        $.each(value['ServicesOrdersBilling'], function (i, row) {
            if(row['lot_caffee'] == 0){
                msg += "<li>" + row['observation2'] +"</li><br>";
            }
        });
        msg+="</ul>"
        return msg;
    }

    function view_modal(id) {
        $( "#modal" ).dialog( "open" );
        $.ajax({
            async: true,
                url: '<?= Router::url(['controller' => 'ServicesOrders', 'action' => 'view']) ?>'+'/'+id,
                dataType: "html",
                success: function (data, textStatus) {
                    $(".modal-body").html(data);
                },
                type: "post"
                
            });
            return false;
    }

    function addDataToTable(tableResult) {
        $('#tableResult').children().remove();
        $.each(tableResult, function (i, value) {
            trHTML = '<tr>';
            trHTML += '<td>' + value['ServicesOrder']['id'] + '</td>';
            trHTML += '<td>' + value['Client']['business_name'] + '</td>';
            if(value['HookupStatus']['id'] == 2){
                trHTML += "<td style=\"background: red\"><span>" + value['HookupStatus']['status_name'] + '</td>';
            }
            else if(value['HookupStatus']['id'] == 1){
                trHTML += "<td style=\"background: blue\"><span>" + value['HookupStatus']['status_name'] + '</td>';
            }
            else if(value['HookupStatus']['id'] == 3){
                trHTML += "<td style=\"background: yellow\"><span>" + value['HookupStatus']['status_name'] + '</td>';
            }
            else if(value['HookupStatus']['id'] == 4){
                trHTML += "<td style=\"background: green\"><span>" + value['HookupStatus']['status_name'] + '</td>';
            }
            trHTML += '<td>' + value['ServicesOrder']['created_date'] + '</td>';
            trHTML += '<td>' + value['ServicesOrder']['closed_date'] + '</td>';
            trHTML += '<td>' + getLotsWtithConexos(value) + '</td>';
            trHTML += '<td>' + getLotCrossdocking(value)+ '</td>';
            trHTML += '<td>' + getSupplyOtherServices(value)+ '</td>';
            trHTML += '<td>' + getCtaCoffee(value)+ '</td>';
            trHTML += '<td>' + value['Departament']['name'] +'</td>';
            trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="view_modal(\'' + value['ServicesOrder']['id'] + '\');">Editar</a></td>'; 
            trHTML += '</tr>';
            $('#tableResult').append(trHTML);
        });
    }

    

</script>