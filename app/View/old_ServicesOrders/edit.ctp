<div class="servicesOrders form">
    <?php echo $this->Html->script('jquery.min');
    echo $this->Form->create('ServicesOrder'); ?>
    <fieldset>
        <legend><?php echo __('Modificar servicio'); ?></legend>
        <?php
        echo $this->Form->input('id', array('type' => 'hidden'));
        echo $this->Form->input('exporter_code', array('label' => 'Código Cliente', 'type' => 'text'));
        echo $this->Form->input('client_name', array('label' => 'Cliente', 'disabled' => 'disabled'));
        echo $this->Form->input('created_date',array('label' => 'Registrado'));
        echo $this->Form->input('instructions',array('label' => 'Instrucciones - (detalles)'));
        ?>
    </fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<script>
    findInfoRemittancesByClientCode($("#ServicesOrderExporterCode").val());
    
    $("#ServicesOrderExporterCode").keyup(function () {
        findInfoRemittancesByClientCode($(this).val());
    });

    function findInfoRemittancesByClientCode(exportCode) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "<?= Router::url(['controller' => 'RemittancesCaffees', 'action' => 'findRemittancesByExportCode']) ?>" + "/" + exportCode,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    debugger;
                    var clientData = JSON.parse(data)['clientData'];
                    var remittancesData = JSON.parse(data)['remittancesData'];
                    $("#ServicesOrderClientName").val(clientData['Client']['business_name']);
                }
            }
        });
    }
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
