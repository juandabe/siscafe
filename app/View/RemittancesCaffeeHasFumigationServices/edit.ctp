<div class="remittancesCaffeeHasFumigationServices form">
<?php echo $this->Form->create('RemittancesCaffeeHasFumigationService'); ?>
	<fieldset>
		<legend><?php echo __('Edit Remittances Caffee Has Fumigation Service'); ?></legend>
	<?php
		echo $this->Form->input('remittances_caffee_id');
		echo $this->Form->input('fumigation_services_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RemittancesCaffeeHasFumigationService.remittances_caffee_id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RemittancesCaffeeHasFumigationService.remittances_caffee_id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Remittances Caffee Has Fumigation Services'), array('action' => 'index')); ?></li>
	</ul>
</div>
