<div class="remittancesCaffeeHasFumigationServices form">
<?php echo $this->Html->script('jquery.min');?>
<?php echo $this->Form->create('RemittancesCaffeeHasFumigationService'); ?>
	<fieldset>
        <legend><?php echo __('Agregar lotes de café a fumigar - Paso #2'); ?></legend>
	<?php
                echo $this->Form->input('fumigation_services_id',array('label' => '# Servicio fumigación','value'=>$idFumigation,'type' => 'hidden'));
                echo $this->Form->input('orderService',array('label' => '# Orden de servicio','value'=>$orderServiceId,'disabled' => 'disabled'));
                echo $this->Form->input('id_certificado',array('label' => '# Servicio fumigación','value'=>$idFumigation,'disabled' => 'disabled'));
                echo $this->Form->input('hour_elevator',array('label' => 'Horas Elevador?','value' => $fumigationService['FumigationService']['hour_elevator']));
                echo $this->Form->input('quimico',array('label' => 'Producto Quimico', 'value' => 'DETIA GAS (ALUMINIUM PHOSPHIDE)','type' => 'hidden'));
		echo $this->Form->input('product_fumigation',array('label' => 'Producto Quimico', 'value' => 'DETIA GAS (ALUMINIUM PHOSPHIDE)','disabled' => 'disabled'));
		echo $this->Form->input('dosis',array('label' => 'Dosis (gramos PH3/metro cubico)','value' => $fumigationService['FumigationService']['dosis']));
                echo $this->Form->input('quantity_poison_used',array('label' => 'Tabletas totales (Und)','value' => $fumigationService['FumigationService']['quantity_poison_used'],'disabled' => 'disabled'));
                echo $this->Form->input('bandejas_used',array('label' => 'Bandejas (Und)','value' => $fumigationService['FumigationService']['bandejas_used'],'disabled' => 'disabled'));
                echo $this->Form->input('ppm_teory',array('label' => 'PPM teoricos (m3)','value' => $fumigationService['FumigationService']['ppm_teory'],'disabled' => 'disabled'));
		echo $this->Form->input('volumen_fumigated',array('label' => 'Volumen fumigado (m3)','value' => $fumigationService['FumigationService']['volumen_fumigated'],'disabled' => 'disabled'));
                echo $this->Form->input('volumen',array('label' => 'Volumen fumigado (m3)','value' => $fumigationService['FumigationService']['volumen_fumigated'],'type' => 'hidden'));
                echo $this->Form->input('kilos',array('label' => 'Peso fumigado (Kg)','value' => $fumigationService['FumigationService']['weight_caffee'],'disabled' => 'disabled'));
                echo $this->Form->input('weight',array('label' => 'Peso fumigado (Kg)','value' => $fumigationService['FumigationService']['weight_caffee'],'type' => 'hidden'));
                echo $this->Form->input('expo_code',array('value' => $client['Client']['exporter_code'],'type' => 'hidden'));
                echo $this->Form->input('remittances_caffee_id',array('type' => 'hidden'));                
	?>
        <h2><?php echo ('Buscar información de Café - (Id - Lote - Exportador - CargoLot)'); ?></h2>
        <h3><?php echo ("Cliente: ".$client['Client']['business_name']); ?></h3>
            <table>
                <tr>
                    <td><?php echo $this->Form->input('remittance_id',array('label' => 'Remesa', 'type'=>'text')); ?></td>
                    <td><?php echo $this->Form->input('lot',array('label' => 'Lote', 'type'=>'text')); ?></td>
                    <td><?php echo $this->Form->input('cargolot_id',array('label' => 'Cargo Lot Id', 'type'=>'text')); ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><?php //echo $this->Form->input('exporter_code',array('label' => 'Exportador Codigo', 'type'=>'text')); ?></td>
                    <td><?php //echo $this->Form->input('exporter_name',array('label' => 'Exportador', 'disabled' => 'disabled')); ?></td>
                </tr>
            </table>
        <h2><?php echo __('Resultado de la busqueda'); ?></h2>        
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date','Fecha Creación'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('cargolot_id','Cargolot ID'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('client','Cliente'); ?></th>
                    <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
                    <th><?php echo $this->Paginator->sort('total_rad_bag_out','Sacos Rad Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags','Sacos Rad In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
        </table>
        <p>
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
            ));
            ?>	</p>
        <div class="paging">
            <?php
            echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
            ?>
        </div>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id','Remesa'); ?></th>
			<th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                        <th><?php echo $this->Paginator->sort('peso','Peso'); ?></th>
			<th><?php echo $this->Paginator->sort('bag','Sacos'); ?></th>
			<th><?php echo $this->Paginator->sort('date','Fecha ingreso'); ?></th>
                        <th><?php echo $this->Paginator->sort('pallet_coffee','Pallet Cafe'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
        <?php echo $this->Form->input('lote',array('label' => 'Lote café','disabled' => 'disabled'));?>
        <?php echo $this->Form->input('pesaje_sacos',array('label' => 'Pesaje # Sacos','disabled' => 'disabled'));?>
        <?php echo $this->Form->input('date_remesa',array('label' => 'Fecha descargue','disabled' => 'disabled'));?>
        <?php echo $this->Form->input('remesa',array('label' => 'Remesa','disabled' => 'disabled'));?>
        <div class="row actions">
            <?php 
                //echo $this->Form->input('remesa',array('label' => 'Remesa','disabled' => 'disabled'));
                //echo $this->Form->input('lot_caffee',array('label' => 'Lote'));
                //echo $this->Html->link(__('Buscar'),'javascript:void(0)',array('name' => 'findRemesa','onclick'=> 'findRemesa.call(this);') );
             ?>
        </div> 
	<?php foreach ($remittancesCaffeeHasFumigationService as $remittancesCaffee): ?>
	<tr>
		<td><?php 
                //debug($remittancesCaffee);exit;
                echo h($remittancesCaffee["RemittancesCaffee"]['id']); ?>&nbsp;</td>
                <td><?php echo h('3-'.$remittancesCaffee['RemittancesCaffee']["Client"]['exporter_code'].'-'.$remittancesCaffee["RemittancesCaffee"]['lot_caffee']); ?>&nbsp;</td>
		<td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_real']); ?>&nbsp;</td>
		<td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</td>
		<td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee']); ?>&nbsp;</td>
		<td class="actions">
			<li><?php  echo $this->Html->link(__('Borrar'), array('action' => 'delete','?' => ['fumigation_services_id' => $idFumigation, 'remittances_caffee_id' => $remittancesCaffee["RemittancesCaffee"]['id']])); ?></li>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
        
	</fieldset> 
    <script type="text/javascript">
        
        $("#RemittancesCaffeeHasFumigationServiceRemittanceId").keyup(function () {
            findRemittancesByRemittanceIdByClient($(this).val());
        });

        $("#RemittancesCaffeeHasFumigationServiceLot").keyup(function () {
            findRemittancesByLotByClient($(this).val());
        });

        $("#RemittancesCaffeeHasFumigationServiceCargolotId").keyup(function () {
            findRemittancesByCargoLotByClient($(this).val());
        });
        
        function findRemittancesByRemittanceIdByClient(remittance){
            var client = $("#RemittancesCaffeeHasFumigationServiceExpoCode").val();
            var remittanceClient = remittance+"-"+client;
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/findByRemittancesClient/" + remittanceClient,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        var remittancesData = JSON.parse(data);
                        addRemittanceDataToTable(remittancesData);
                    }
                }
            });
        }
        
        function findRemittancesByLotByClient(lotCoffee){
            var client = $("#RemittancesCaffeeHasFumigationServiceExpoCode").val();
            var clientLotCoffee = client+"-"+lotCoffee;
            console.log(clientLotCoffee);
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/findByLotClient/" + clientLotCoffee,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        var remittancesData = JSON.parse(data);
                        addRemittanceDataToTable(remittancesData);
                    }
                }
            });
        }
        
        function findRemittancesByCargoLotByClient(cargolot){
            var client = $("#RemittancesCaffeeHasFumigationServiceExpoCode").val();
            var clientCargoLot = client+"_"+cargolot;
            console.log(clientCargoLot);
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/findRemittancesByCargoLotIdClient/" + clientCargoLot,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data != "") {
                        var remittancesData = JSON.parse(data);
                        addRemittanceDataToTable(remittancesData);
                    }
                }
            });
        }
        
        function findRemesa(){
            clearField();
            $.ajax({
               type: "GET",
               datatype: "json",
               url: "/RemittancesCaffees/findByRemesa/"+$("#RemittancesCaffeeHasFumigationServiceRemesa").val(),
               error: function(msg){alert("error networking");},
               success: function(data){
                    var json = JSON.parse(data);
                    if(JSON.stringify(json) !== "[]"){
                        $("#RemittancesCaffeeHasFumigationServicePesajeSacos").val("SACOS: "+json.RemittancesCaffee.quantity_radicated_bag_in+" PESO NETO (KG): "+json.RemittancesCaffee.total_weight_net_real);
                        $("#RemittancesCaffeeHasFumigationServiceLote").val("3-"+json.Client.exporter_code+"-"+json.RemittancesCaffee.lot_caffee);
                        $("#RemittancesCaffeeHasFumigationServiceDateRemesa").val(json.RemittancesCaffee.download_caffee_date);
                        $("#RemittancesCaffeeHasFumigationServiceRemesa").val(json.RemittancesCaffee.id);
                    }
                    else{
                        $("#RemittancesCaffeeHasFumigationServiceLote").val("NO ARROJO RESULTADOS");
                    }
             }});
        }
            
        function clearField(){
            $("#RemittancesCaffeeHasFumigationServicePesajeSacos").val("");
            $("#RemittancesCaffeeHasFumigationServiceLote").val("");
            $("#RemittancesCaffeeHasFumigationServiceDateRemesa").val("");
        }
        
        function addRemittance(remittanceId) {
            clearField();
            $('#RemittancesCaffeeHasFumigationServiceRemesa').val(remittanceId);
            $('#RemittancesCaffeeHasFumigationServiceRemittancesCaffeeId').val(remittanceId);
            findRemesa();
        }
        
        function addRemittanceDataToTable(remittancesData) {
        $('#tableRemittances').children().remove();
        $.each(remittancesData, function (i, value) {
		console.log(value['RemittancesCaffee']);
            trHTML = '<tr>';
            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['cargolot_id'] + '</td>';
            trHTML += '<td>' + '3-'+value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee'] + '</td>';
            trHTML += '<td>' + value['Client']['business_name'] + '</td>';
            trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['total_rad_bag_out'] + '</td>';
            trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_in_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_out_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            var remittanceId = value['RemittancesCaffee']['id'];
            trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="addRemittance(\'' + remittanceId.toString() + '\');">Asociar</a></td>';
            trHTML += '</tr>';
            $('#tableRemittances').append(trHTML);
        });
    }
        
    </script>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>	
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado fumigación'), array('controller'=>'FumigationServices','action' => 'index')); ?></li>
	</ul>
</div>
