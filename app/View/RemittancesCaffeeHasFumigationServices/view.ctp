<div class="remittancesCaffeeHasFumigationServices view">
<h2><?php echo __('Remittances Caffee Has Fumigation Service'); ?></h2>
	<dl>
		<dt><?php echo __('Remittances Caffee Id'); ?></dt>
		<dd>
			<?php echo h($remittancesCaffeeHasFumigationService['RemittancesCaffeeHasFumigationService']['remittances_caffee_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fumigation Services Id'); ?></dt>
		<dd>
			<?php echo h($remittancesCaffeeHasFumigationService['RemittancesCaffeeHasFumigationService']['fumigation_services_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Remittances Caffee Has Fumigation Service'), array('action' => 'edit', $remittancesCaffeeHasFumigationService['RemittancesCaffeeHasFumigationService']['remittances_caffee_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Remittances Caffee Has Fumigation Service'), array('action' => 'delete', $remittancesCaffeeHasFumigationService['RemittancesCaffeeHasFumigationService']['remittances_caffee_id']), array('confirm' => __('Are you sure you want to delete # %s?', $remittancesCaffeeHasFumigationService['RemittancesCaffeeHasFumigationService']['remittances_caffee_id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Remittances Caffee Has Fumigation Services'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Remittances Caffee Has Fumigation Service'), array('action' => 'add')); ?> </li>
	</ul>
</div>
