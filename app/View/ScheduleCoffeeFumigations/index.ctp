	<div class="scheduleCoffeeFumigations index">
	<h2><?php echo __('Listado lotes de Cafe Preaviaso para Fumigar'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('Id Fumigacion'); ?></th>
			<th><?php echo $this->Paginator->sort('Puerto'); ?></th>
			<th><?php echo $this->Paginator->sort('Estado'); ?></th>
			<th><?php echo $this->Paginator->sort('Exportador'); ?></th>
			<th><?php echo $this->Paginator->sort('Lote Cafe'); ?></th>
			<th><?php echo $this->Paginator->sort('Cta Cafe'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha reg'); ?></th>
			<th class="actions"><?php echo __('--'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($scheduleCoffeeFumigations as $scheduleCoffeeFumigation): ?>
	<tr>
		<td><?php echo h($scheduleCoffeeFumigation['FumigationService']['id']); ?>&nbsp;</td>
		<td><?php echo h($scheduleCoffeeFumigation['Departament']['name']); ?>&nbsp;</td>
		<td>
			<?php echo h($scheduleCoffeeFumigation['HookupStatus']['status_name']); ?>
		</td>
		<td><?php echo h($scheduleCoffeeFumigation['Client']['business_name']); ?>&nbsp;</td>
		<td><?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['lot_coffee']); ?>&nbsp;</td>
		<td><?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['qta_coffee']); ?>&nbsp;</td>
		<td><?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['reg_date']); ?>&nbsp;</td>
		<td class="actions">
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Fumigaciones'), array('controller'=>'FumigationServices','action' => 'index')); ?></li>
	</ul>
</div>
