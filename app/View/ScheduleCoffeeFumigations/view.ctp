<div class="scheduleCoffeeFumigations view">
<h2><?php echo __('Schedule Coffee Fumigation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fumigation Service'); ?></dt>
		<dd>
			<?php echo $this->Html->link($scheduleCoffeeFumigation['FumigationService']['id'], array('controller' => 'fumigation_services', 'action' => 'view', $scheduleCoffeeFumigation['FumigationService']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lot Coffee'); ?></dt>
		<dd>
			<?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['lot_coffee']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reg Date'); ?></dt>
		<dd>
			<?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['reg_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qta Coffee'); ?></dt>
		<dd>
			<?php echo h($scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['qta_coffee']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Schedule Coffee Fumigation'), array('action' => 'edit', $scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Schedule Coffee Fumigation'), array('action' => 'delete', $scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $scheduleCoffeeFumigation['ScheduleCoffeeFumigation']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Schedule Coffee Fumigations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schedule Coffee Fumigation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fumigation Services'), array('controller' => 'fumigation_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fumigation Service'), array('controller' => 'fumigation_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
