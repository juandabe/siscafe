<div class="basculeVerifications index">
	<h2><?php echo __('Verificación bascula - Pesos Patronos'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha verificación'); ?></th>
			<th><?php echo $this->Paginator->sort('Diferencia'); ?></th>
			<th><?php echo $this->Paginator->sort('Tolerancia'); ?></th>
			<th><?php echo $this->Paginator->sort('Bascula ID'); ?></th>
			<th><?php echo $this->Paginator->sort('Peso Bascula'); ?></th>
                        <th><?php echo $this->Paginator->sort('Peso Patron'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($basculeVerifications as $basculeVerification): ?>
	<tr>
		<td><?php 
                //debug($basculeVerification);exit;
                echo h($basculeVerification['BasculeVerification']['id']); ?>&nbsp;</td>
		<td><?php echo h($basculeVerification['BasculeVerification']['datetime_verification']); ?>&nbsp;</td>
		<td><?php echo h($basculeVerification['BasculeVerification']['diferency']); ?>&nbsp;</td>
		<td><?php echo h($basculeVerification['BasculeVerification']['tolerancy'] == 0 ? "REPROBADA":"ACEPTADA"); ?>&nbsp;</td>
		<td><?php echo h($basculeVerification['BasculeVerification']['basc_id']); ?>&nbsp;</td>
		<td><?php echo h($basculeVerification['BasculeVerification']['weight_basc']); ?>&nbsp;</td>
                <td><?php echo h($basculeVerification['BasculeVerification']['weight_patron']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $basculeVerification['BasculeVerification']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $basculeVerification['BasculeVerification']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $basculeVerification['BasculeVerification']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $basculeVerification['BasculeVerification']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Registrar Verificación'), array('action' => 'add')); ?></li>
	</ul>
</div>
