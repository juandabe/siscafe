<div class="basculeVerifications view">
<h2><?php echo __('Patronar bascula'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($basculeVerification['BasculeVerification']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Datetime Verification'); ?></dt>
		<dd>
			<?php echo h($basculeVerification['BasculeVerification']['datetime_verification']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Diferency'); ?></dt>
		<dd>
			<?php echo h($basculeVerification['BasculeVerification']['diferency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tolerancy'); ?></dt>
		<dd>
			<?php echo h($basculeVerification['BasculeVerification']['tolerancy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Basc Id'); ?></dt>
		<dd>
			<?php echo h($basculeVerification['BasculeVerification']['basc_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight Basc'); ?></dt>
		<dd>
			<?php echo h($basculeVerification['BasculeVerification']['weight_basc']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Lista de validaciones'), array('action' => 'index')); ?> </li>
	</ul>
</div>
