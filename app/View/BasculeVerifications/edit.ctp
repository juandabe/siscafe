<div class="basculeVerifications form">
<?php echo $this->Form->create('BasculeVerification'); ?>
	<fieldset>
		<legend><?php echo __('Edit Bascule Verification'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('datetime_verification');
		echo $this->Form->input('diferency');
		echo $this->Form->input('tolerancy');
		echo $this->Form->input('basc_id');
		echo $this->Form->input('weight_basc');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BasculeVerification.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('BasculeVerification.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Bascule Verifications'), array('action' => 'index')); ?></li>
	</ul>
</div>
