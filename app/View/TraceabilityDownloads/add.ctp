<div class="traceabilityDownloads form">
<?php echo $this->Form->create('TraceabilityDownload',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Trazabilidad Descargue'); ?></legend>
	<?php
                echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['Client']['business_name']) );
                echo $this->Form->input('lot_caffee',array( 'label' => 'Lote Café', 'disabled' => 'disabled' , 'value' => "3-".$remittancesCaffee['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
                echo $this->Form->input('remesa',array( 'label' => 'Remesa', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['id']) );
                echo $this->Form->label('img1','(Imagen 1)');
		echo $this->Form->file('img1',array('required'=>true));
                echo $this->Form->label('img2','(Imagen 2)');
		echo $this->Form->file('img2',array('required'=>true));
                echo $this->Form->label('img3','(Imagen 3)');
		echo $this->Form->file('img3',array('required'=>true));
                echo $this->Form->label('img4','(Imagen 4)');
		echo $this->Form->file('img4',array('required'=>true));
                echo $this->Form->label('img5','(Imagen 5)');
		echo $this->Form->file('img5',array('required'=>true));
                //echo $this->Form->label('seals','(Sellos Vehiculo)');
                //echo $this->Form->input('seals',array( 'label' => '','required'=>true) );
		//echo $this->Form->file('img6');
		echo $this->Form->input('observation',array('label' => 'Observaciones'));
		//echo $this->Form->input('remittances_caffee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Descargue'), array('controller'=>'RemittancesCaffees','action' => 'sampler_index')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>
