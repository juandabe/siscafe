<div class="readyContainers index">
	<h2><?php echo __('Ready Containers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('bic_container'); ?></th>
			<th><?php echo $this->Paginator->sort('date_registre'); ?></th>
			<th><?php echo $this->Paginator->sort('shipping_lines_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($readyContainers as $readyContainer): ?>
	<tr>
		<td><?php echo h($readyContainer['ReadyContainer']['id']); ?>&nbsp;</td>
		<td><?php echo h($readyContainer['ReadyContainer']['bic_container']); ?>&nbsp;</td>
		<td><?php echo h($readyContainer['ReadyContainer']['date_registre']); ?>&nbsp;</td>
		<td><?php echo h($readyContainer['ReadyContainer']['shipping_lines_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $readyContainer['ReadyContainer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $readyContainer['ReadyContainer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $readyContainer['ReadyContainer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $readyContainer['ReadyContainer']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ready Container'), array('action' => 'add')); ?></li>
	</ul>
</div>
