<div class="readyContainers form">
<?php echo $this->Form->create('ReadyContainer'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ready Container'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('bic_container');
		echo $this->Form->input('date_registre');
		echo $this->Form->input('shipping_lines_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ReadyContainer.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ReadyContainer.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ready Containers'), array('action' => 'index')); ?></li>
	</ul>
</div>
