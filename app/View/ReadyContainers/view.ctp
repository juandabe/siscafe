<div class="readyContainers view">
<h2><?php echo __('Ready Container'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($readyContainer['ReadyContainer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bic Container'); ?></dt>
		<dd>
			<?php echo h($readyContainer['ReadyContainer']['bic_container']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Registre'); ?></dt>
		<dd>
			<?php echo h($readyContainer['ReadyContainer']['date_registre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Shipping Lines Id'); ?></dt>
		<dd>
			<?php echo h($readyContainer['ReadyContainer']['shipping_lines_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ready Container'), array('action' => 'edit', $readyContainer['ReadyContainer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ready Container'), array('action' => 'delete', $readyContainer['ReadyContainer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $readyContainer['ReadyContainer']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ready Containers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ready Container'), array('action' => 'add')); ?> </li>
	</ul>
</div>
