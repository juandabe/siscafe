<div class="packagingCaffees index">
    <h2><?php echo __('Listado de Embalaje General'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date','Fecha Ordenado'); ?></th>
                <th><?php echo $this->Paginator->sort('state_packaging_id','Estado'); ?></th>
                <th><?php echo $this->Paginator->sort('ready_container_id','Contenedor'); ?></th>
                <th><?php echo $this->Paginator->sort('lotes'); ?></th>
                <th><?php echo $this->Paginator->sort('iso_ctn'); ?></th>
                <th><?php echo $this->Paginator->sort('seal_date','Fecha Sellado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($packagingCaffees as $packagingCaffee): ?>
            <tr>
                <td><?php
                //debug($packagingCaffee);exit;
                echo h($packagingCaffee['PackagingCaffee']['id']); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td><?php 
		  if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 5){
		      echo h("EMBALAJE EN PROCESO");
		  }
                  else if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 1){
                      echo h("EMBALAJE CONFIRMADO");
                  }
                  else if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 2){
                      echo h("EMBALAJE ORDENADO");
                  }
                  else if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3){
                      echo h("EMBALAJE COMPLETADO");
                  }
		   ?>&nbsp;</td>
                <td><?php 
		if($packagingCaffee['ReadyContainer']['bic_container'] != null){
		  echo h($packagingCaffee['ReadyContainer']['bic_container']);
		}
		else{
		  echo h("CONTENEDOR NO ASIGNADO");
		}
		 ?>&nbsp;</td>
                <td><?php 
		  $detailsCaffee = $packagingCaffee['DetailPackagingCaffee'];
		  $tmp;
		  $current="";
                  if($detailsCaffee == NULL)
                  {
                      echo "SIN LOTE ASIGNADO";
                  }
		  foreach($detailsCaffee as $detail){
		    $tmp = h('3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee']);
		    if($current != $tmp){
		      echo '3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee'] . " | \n";
		      $current = $tmp;
		    }
		  }
		?>&nbsp;</td>
                <td><?php if($packagingCaffee['PackagingCaffee']['iso_ctn'] == null){
                            echo "N/A";
                }
                    ?>&nbsp;</td>
  
                <td><?php if($packagingCaffee['PackagingCaffee']['seal_date'] != null)
			    echo h($packagingCaffee['PackagingCaffee']['seal_date']);
			  else
			    echo h("SIN SELLAR");?>&nbsp;</td>
                <td class="actions">
                    <?php
                          echo $this->Html->link(__('Fotografias Ctn'), array('controller'=>'OperationTrackings','action' => 'addctns', $packagingCaffee['PackagingCaffee']['id']));
                          if($packagingCaffee['ReadyContainer']['bic_container'] != null){
                              
                            if($packagingCaffee['PackagingCaffee']['seal_date'] != null){
                                echo $this->Html->link(__('Ver Tarja'), array('controller'=>'ViewPackagings','action' => 'view', $packagingCaffee['PackagingCaffee']['id']));
                            }
                            else{
                                if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3){
                                    echo $this->Html->link(__('Sellar Ctn'), array('action' => 'seal', $packagingCaffee['PackagingCaffee']['id']));
                                }
                            }
                          }
			  
			 ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Embalajes Vaciados'), array('controller' => 'PackagingCaffees', 'action' => 'empty_oie')); ?></li>
        <li><?php echo $this->Html->link(__('Embalajes Completados'), array('controller' => 'PackagingCaffees', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Embalajes Cancelados'), array('controller' => 'PackagingCaffees', 'action' => 'cancelled_oie')); ?></li>
        <li><?php echo $this->Html->link(__('Embalajes Confirmado'), array('controller' => 'PackagingCaffees', 'action' => 'confirm_oie')); ?></li>
        <li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
    </ul>
</div>
