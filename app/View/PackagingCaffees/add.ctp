<div class="packagingCaffees form">
<?php 

echo $this->Html->css('style');
echo $this->Form->create('PackagingCaffee',array('type' => 'file')); ?>
    <fieldset>
        <legend><?php 
            if($this->Session->read('User.opera') == 1){
                if (in_array($this->Session->read('User.jetty'),['MZL','LETRAS','CHN','DOR','BUGA'])) {
                    echo __('Cargue de Vehiculo'); 
                }
                else{
                    echo __('Crear Orden Interna de Entrega'); 
                }
                
            } 
            else 
                echo __('Registro autorización de llenado Café  - Colilla de embalaje'); 
        ?></legend>
    <?php
            if($this->Session->read('User.opera') == 1){
                echo $this->Form->input('id');
                echo $this->Form->input('observation',array('label' => 'Observaciones'));
                echo $this->Form->input('weight_to_out',array( 'label'=>'Pesar a la salida'));
        echo $this->Form->input('consolidate',array( 'label'=>'Es consolidado?'));
                echo $this->Form->input('info_navy_id',array('required' => true, 'type'=>'text', 'label'=>'Proforma'));
            }
            if($this->Session->read('User.opera') != 1):
                echo $this->Form->input('bic_container',array('label'=>'Numero de contenedor'));
                echo $this->Form->input('long_container',array('label' => 'Longitud Contenedor','empty'=>'Seleccione...', 'required' => true,
                'options' => array('20' => '20', '40' => '40', '20-RF' => '20 RF', '40-RF' => '40 RF')));
                echo $this->Form->input('iso_ctn',array('label' => 'ISO Contenedor', 'options' => array('22G1' => '22G1', '44G1' => '44G1', '20-RF' => '20 RF', '40-RF' => '40 RF')));  
        echo $this->Form->input('packaging_date', array('label' => 'Fecha Registro','required' => true,'timeFormat' => 24,'type'=>'datetime'));
                echo $this->Form->input('exporter_id', array('label' => 'Código Exportador', 'type' => 'text', 'required' => false));
                echo $this->Form->input('exporter_name', array('label' => 'Exportador', 'type' => 'text', 'disabled' => 'disabled'));
                echo $this->Form->input('exporter_cod', array('type' => 'hidden'));
            echo $this->Form->input('lot_to_search', array('label' => 'Lote a buscar', 'type' => 'text',"pattern" => "[^ \x22]+[0-9]", "placeholder"=>"Ingrese el Codigo del Lote sin espacios y separado por coma", 'title'=>'Ingrese el Codigo del Lote sin espacios y separado por coma', 'required' => false ));
            echo $this->Form->button('Buscar', array('id' => 'btnToSearch', 'type' => 'button', 'style' => 'background: #62af56;background-image: -webkit-gradient(linear, left top, left bottom, from(#76BF6B), to(#3B8230)); background-image: -webkit-linear-gradient(top, #76BF6B, #3B8230); background-image: -moz-linear-gradient(top, #76BF6B, #3B8230); border-color: #2d6324; color: #fff; text-shadow: rgb(0 0 0 / 50%) 0 -1px 0; padding: 8px 10px;'));
        ?>

            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th></th>
                        <th>Remesa</th>
                        <th>Lote</th>
                        <th>Cantidad</th>
                        <th>Fecha Descargue</th>
                    </tr>
                </thead>
                <tbody id="tableResultLots">
                </tbody>
            </table>
            <br>

            <?php


            echo $this->Form->input('consolidate', array('label' => 'Es consolidado (Contenedor compartido por 2 exportadores)?','onclick'=>'consolidate()'));
            ?>
            <div id="consilidate">
            <?php
            echo $this->Form->input('exporter_id1', array('label' => 'Código Exportador', 'type' => 'text', 'class'=>'consilidate'));
            echo $this->Form->input('exporter_name1', array('label' => 'Exportador', 'type' => 'text', 'disabled' => 'disabled', 'class'=>'consilidate'));
            echo $this->Form->input('exporter_cod_consolidate', array('type' => 'hidden'));
            echo $this->Form->input('lot_to_search_consilidate', array('label' => 'Lote a buscar', 'type' => 'text',"pattern" => "[^ \x22]+[0-9]", "placeholder"=>"Ingrese el Codigo del Lote sin espacios y separado por coma", 'title'=>'Ingrese el Codigo del Lote sin espacios y separado por coma', 'class'=>'consilidate' ));
            echo $this->Form->button('Buscar', array('id' => 'btnToSearch1', 'type' => 'button', 'class'=>'consilidate', 'style' => 'background: #62af56;background-image: -webkit-gradient(linear, left top, left bottom, from(#76BF6B), to(#3B8230)); background-image: -webkit-linear-gradient(top, #76BF6B, #3B8230); background-image: -moz-linear-gradient(top, #76BF6B, #3B8230); border-color: #2d6324; color: #fff; text-shadow: rgb(0 0 0 / 50%) 0 -1px 0; padding: 8px 10px;'));
             ?>

            <table cellpadding="0" cellspacing="0" class="consilidate" id="tableLots1">
                <thead>
                    <tr>
                        <th></th>
                        <th>Remesa</th>
                        <th>Lote</th>
                        <th>Cantidad</th>
                        <th>Fecha Descargue</th>
                    </tr>
                </thead>
                <tbody id="tableResultLots1">
                </tbody>
            </table>
            <br>
            </div>
            <?php


                echo $this->Form->input('observation', array('label' => 'Observaciones'));
                echo $this->Form->input('weight_container',array('label'=>'Peso Neto Café KG - (Peso sin unidad de Kg)', 'type'=>'number','placeholder'=>'E.g: 70 Kg x 300 Sacos = 21000'));
                echo $this->Form->input('qta_unit',array('label'=>'Total Cant. Sacos ó Cajas','type'=>'number'));
                echo $this->Form->input('autorizacion',array('type'=>'number', 'label'=>'Número Autorización'));
        echo $this->Form->input('seal_3',array( 'type'=>'text', 'label'=>'Sello 1'));
        echo $this->Form->input('seal_4',array('type'=>'text', 'label'=>'Sello 2'));
        echo $this->Form->label('document','Copia Autorización Llenado de Café');
                echo $this->Form->file('document');
        echo $this->Form->input('cooperativa_ctg',array('label' => 'Cooperativa (Cuadrilla)', 'options' => array('VINCULAR'=>'VINCULAR','OCUPAR'=>'OCUPAR','SILPORT'=>'SILPORT','COPC'=>'COPC', 'CUAD 1'=>'CUAD 1', 'CUAD 2'=>'CUAD 2')));  
                echo $this->Form->input('info_navy_booking',array('type'=>'text', 'label'=>'Núm reserva - Booking'));
                echo $this->Form->input('booking',array('type'=>'hidden', 'value' => 'false','label'=>'Booking'));
                echo $this->Form->input('info_navy_trip',array('type'=>'text', 'label'=>'Viaje Motonave'));
                echo $this->Form->input('info_navy_motonave',array('type'=>'text', 'label'=>'Nombre Motonave'));
                echo $this->Form->input('info_navy_line',array('class' => 'chosen-select','options' =>$maritimeLines,'label'=>'Linea Naviera'));
                echo $this->Form->input('info_navy_customid',array( 'class' => 'chosen-select', 'label'=>'Cod Aduana','options' =>$listCustom));
                echo $this->Form->input('info_navy_navy',array('class' => 'chosen-select', 'options' =>$listNavyAgent, 'label'=>'Agente Naviero)'));
        echo $this->Form->input('info_navy_pod',array('type'=>'text', 'label'=>'Puerto Destino - POD'));
                echo $this->Form->input('mode_packaging',array('label'=>'Modalidad de embalaje','options' => array('LCL' => 'LCL', 'FCL' => 'FCL')));
                echo $this->Form->input('type_packaging',array('label'=>'Tipo de embalaje','options' => array('SACOS' => 'SACOS', 'GRANEL' => 'GRANEL','BIG BAG' => 'BIG BAG','TABACO'=>'TABACO','CARBON'=>'CARBON','CD_CACAO' => 'CD_CACAO','CACAO'=>'CACAO','CAJAS' => 'CAJAS')));
                echo $this->Form->input('info_navy_adicional_element',array('placeholder'=>'(e.g: KARFT 2 | DRY BAGS 2)','type'=>'textarea', 'label'=>'Elementos adicionales'));
                //echo $this->Form->input('jetty',array('options'=>array('STM'=>'STM','CONTECAR'=>'CONTECAR','SPRC'=>'SPRC','COMPAS'=>'COMPAS','SPIA'=>'SPIA'), 'label'=>'Puerto Maritimo')); 
        echo $this->Form->input('jetty',array('options'=>$citys, 'label'=>'Puerto Maritimo'));
                echo $this->Form->input('user_tarja',array('label'=>'Tarjador','options' => $operator));
        echo $this->Form->input('user_driver',array('label'=>'Operador','options' => $driver));
                ?>
                <div class="row actions">
                    <?php echo $this->Form->input('time_start',array('type'=>'time','timeFormat' => 24, 'label'=>'Hora de Inicio'));?>
                    <?php echo $this->Form->input('time_end',array('type'=>'time','timeFormat' => 24, 'label'=>'Hora de Final'));?>
                </div>
            <?php endif;?>
                <?php
            echo $this->Form->input('created_date',array('label'=>'Fecha de creación', 'type'=>'hidden'));
            
    ?>
    </fieldset>
    <script type="text/javascript">
    $(".chosen-select").chosen();

        var infoNavies = [<?php echo "'".implode("','",$infoNavies)."'";?>];
        $("#PackagingCaffeeInfoNavyId").autocomplete({
            source: infoNavies,
            select:function( event, ui ) {
                var booking = ui.item.label;
                console.log(booking);
                //refillInfoNavyByBooking(booking);
            }
                });



        document.querySelector("#PackagingCaffeeLotToSearch").addEventListener("keyup", function(event){
            if (event.keyCode == 32 || event.keyCode == 190 || event.keyCode == 189 || event.keyCode == 55 || event.keyCode == 220){  alert("Error: letra, simbolo o espacio en blanco no es valido."); } }
        , false);

        $("#btnToSearch").on('click', function() {
            $('#tableResultLots').children().remove();
            $.ajax({
                type: "POST",
                url: "<?= Router::url(['controller' => 'RemittancesCaffees', 'action' => 'findRemittancesByLotClientMultiple']); ?>" + '/' + $("#PackagingCaffeeExporterId").val() + '-' + $("#PackagingCaffeeLotToSearch").val(),
                dataType: 'json',
                error: function(msg) {
                    alert("El Lote no se encuentra en BODEGA estado de (Descargado y Almacenado)");
                },
                success: function(data) {
                    if (data !== "") {
                        $('#tableResultLots').children().remove();
                        $.each(data, function(i, value) {
                            trHTML = '<tr>';
                            trHTML += '<td><input name=data[PackagingCaffee][lot_to_search1][' + value['RemittancesCaffee']['id'] + '-' + value['RemittancesCaffee']['quantity_radicated_bag_in'] + ']  type="checkbox"></td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['lot_caffee'] + '</td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['quantity_radicated_bag_in'] + '</td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
                            trHTML += '</tr>';
                            $('#tableResultLots').append(trHTML);
                        });
                        console.log(data);
                    }
                }
            });
        });


        $(document).ready(function() { 
            $('#consilidate').hide();
        });

        $("#PackagingCaffeeExporterId1").keyup(function() {
            findInfoRemittancesByClientCode1($(this).val());
        });

        function findInfoRemittancesByClientCode1(exportCode) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "<?= Router::url(['controller' => 'RemittancesCaffees', 'action' => 'findRemittancesByExportCode']); ?>" + "/" + exportCode,
                error: function(msg) {
                    alert("Error al consultar cliente");
                },
                success: function(data) {
                    if (data !== "") {
                        var clientData = JSON.parse(data)['clientData'];
                        $("#PackagingCaffeeExporterName1").val(clientData['Client']['business_name']);
                        $("#PackagingCaffeeExporterCodConsolidate").val(clientData['Client']['id']);
                    }
                }
            });
        }
        

        document.getElementById("PackagingCaffeeConsolidate").addEventListener("click", function( event ) {   

            if (window.confirm("Esta seguro que el Contenedor es Consolidado?")) {
                $('#consilidate').show(); 
                document.getElementById("PackagingCaffeeConsolidate").value = "1"; 
            }else{
                $('#consilidate').hide(); 
                document.getElementById("PackagingCaffeeConsolidate").value = "0"; 
            }

        }, false);         

        $("#btnToSearch1").on('click', function() {
            $('#tableResultLots1').children().remove();
            $.ajax({
                type: "POST",
                url: "<?= Router::url(['controller' => 'RemittancesCaffees', 'action' => 'findRemittancesByLotClientMultiple1']); ?>" + '/' + $("#PackagingCaffeeExporterId1").val() + '-' + $("#PackagingCaffeeLotToSearchConsilidate").val(),
                dataType: 'json',
                error: function(msg) {
                    alert("El Lote no se encuentra en BODEGA estado de (Descargado y Almacenado)");
                },
                success: function(data) {
                    if (data !== "") {
                        $('#tableResultLots1').children().remove();
                        $.each(data, function(i, value) {
                            trHTML = '<tr>';
                            trHTML += '<td><input name=data[PackagingCaffee][lot_to_search2][' + value['RemittancesCaffee']['id'] + '-' + value['RemittancesCaffee']['quantity_radicated_bag_in'] + ']  type="checkbox"></td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['lot_caffee'] + '</td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['quantity_radicated_bag_in'] + '</td>';
                            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
                            trHTML += '</tr>';
                            $('#tableResultLots1').append(trHTML);
                        });
                        console.log(data);
                    }
                }
            });
        });





        $("#PackagingCaffeeInfoNavyBooking").keyup(function () {
            if($(this).val() === '')
            {
                $("#PackagingCaffeeBooking").val(false);
                $("#PackagingCaffeeInfoNavyMotonave").val("");
                $("#PackagingCaffeeInfoNavyTrip").val("");
                $("#PackagingCaffeeInfoNavyLinecod").val("");
                $("#PackagingCaffeeInfoNavyId").val("");
                $("#PackagingCaffeeInfoNavyPod").val("");
                $("#PackagingCaffeeInfoNavyCustomid").val("");
                $("#PackagingCaffeeModePackaging").val("");
                $("#PackagingCaffeeTypePackaging").val("");
                $("#PackagingCaffeeInfoNavyAdicionalElement").val("");
                $("#PackagingCaffeeInfoNavyCustomname").val("");
                $("#PackagingCaffeeInfoNavyLine").val("");
            }
        });
        
        $("#PackagingCaffeeExporterId").keyup(function () {
            findInfoRemittancesByClientCode($(this).val());
        });
        
        $("#PackagingCaffeeInfoNavyLinecod").keyup(function () {
            findLineById($(this).val());
        });
        
        $("#PackagingCaffeeInfoNavyCustomid").keyup(function () {
            findCustomsById($(this).val());
        });

    $("#PackagingCaffeeInfoNavyNavy").keyup(function () {
            findNayyAgentById($(this).val());
        });
        
        function refillInfoNavyByBooking(booking) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/InfoNavies/findInfoNavyByBooking/" + booking,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        var InfoNavyData = JSON.parse(data)[0]['InfoNavy'];
                       $("#PackagingCaffeeInfoNavyMotonave").val(InfoNavyData['motorship_name']);
                       $("#PackagingCaffeeInfoNavyTrip").val(InfoNavyData['travel_num']);
                       $("#PackagingCaffeeInfoNavyLinecod").val(InfoNavyData['shipping_lines_id']);
                       $("#PackagingCaffeeBooking").val(true);
                       findLineById(InfoNavyData['shipping_lines_id']);
                       $("#PackagingCaffeeInfoNavyId").val(InfoNavyData['proforma']);
                       $("#PackagingCaffeeInfoNavyPod").val(InfoNavyData['destiny']);
                       $("#PackagingCaffeeInfoNavyCustomid").val(InfoNavyData['customs_id']);
                       findCustomsById(InfoNavyData['customs_id']);
                       $("#PackagingCaffeeModePackaging").val(InfoNavyData['packaging_type']);
                       $("#PackagingCaffeeTypePackaging").val(InfoNavyData['packaging_mode']);
                       $("#PackagingCaffeeInfoNavyAdicionalElement").val(InfoNavyData['elements_adicional']);
                    }
                }
            });
        }

        function findInfoRemittancesByClientCode(exportCode) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/findRemittancesByExportCode/" + exportCode,
                error: function (msg) {
                    alert("Error al consultar cliente");
                },
                success: function (data) {
                    if (data !== "") {
                        var clientData = JSON.parse(data)['clientData'];
                        $("#PackagingCaffeeExporterName").val(clientData['Client']['business_name']);
                        $("#PackagingCaffeeExporterCod").val(clientData['Client']['id']);
                    }
                }
            });
        }

    function findNayyAgentById(idNavyAgent){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/NavyAgents/findById/" + idNavyAgent,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#PackagingCaffeeInfoNavyNavyagent").val(json.NavyAgent.name);
                    } else {
                        $("#PackagingCaffeeInfoNavyNavyagent").val("NO ARROJO RESULTADOS");
                    }
                }});
        }

        
        function findLineById(idLine){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/ShippingLines/findById/" + idLine,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        var ShippingLineData = JSON.parse(data)['ShippingLine'];
                        $("#PackagingCaffeeInfoNavyLine").val(ShippingLineData['business_name']);
                    }
                }
            });
        }
        
        function findCustomsById(idCustom){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/Customs/findById/" + idCustom,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        var customsData = JSON.parse(data)['Custom'];
                        $("#PackagingCaffeeInfoNavyCustomname").val(customsData['cia_name']);
                    }
                }
            });
        }

    </script>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php 
        if($this->Session->read('User.centerId') != 1) 
            echo $this->Html->link(__('Regresar'), array('action' => 'index')); 
        else
            echo $this->Html->link(__('Regresar'), array('action' => 'allOie')); 
        ?></li>
 </ul>
</div>
