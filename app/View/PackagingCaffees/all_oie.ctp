<div class="packagingCaffees index">
    <h2><?php echo __('Ordenes Internas de Entrega'); ?></h2>
    <?php echo $this->Html->link(__('Crear OIE'), array('action' => 'add')); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                
                <th><?php echo $this->Paginator->sort('id', 'OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('info_navy_id', 'Proforma'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Fecha de creación'); ?></th>
                <th><?php echo $this->Paginator->sort('weight_to_out', 'Pesar a la Salida'); ?></th>
                <th><?php echo $this->Paginator->sort('lotes', 'Lotes Cafe'); ?></th>
                <th><?php echo $this->Paginator->sort('long_container', 'Longitud Contenedor'); ?></th>
                <th><?php echo $this->Paginator->sort('state_packaging_id', 'Estado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($packagingCaffees as $packagingCaffee): ?>
            <tr>
                
                <td><?php 
                //debug($packagingCaffee);exit;
                echo h($packagingCaffee['PackagingCaffee']['id']); ?>&nbsp;</td>
                <td><?php echo h($infoNavies[$packagingCaffee['PackagingCaffee']['info_navy_id']]); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td><?php 
                    if($packagingCaffee['PackagingCaffee']['weight_to_out']==1){
                        echo h('SI');
                    } else {
                        echo h('NO');
                    }
                 ?>&nbsp;</td>
                <td><?php
                $lotes="";
                foreach ($packagingCaffee['DetailPackagingCaffee'] as $remittancesCaffee){
                    $lotes .= "| ".$remittancesCaffee['RemittancesCaffee']['id']." - 3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code']."-".
                            $remittancesCaffee['RemittancesCaffee']['lot_caffee']." x ".$remittancesCaffee['quantity_radicated_bag_out']." - ".
                            $remittancesCaffee['RemittancesCaffee']['TypeUnit']['description']."| ";
                }
                echo h($lotes); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['InfoNavy']['long_container']); ?>&nbsp;</td>
                <td><?php echo h($statePackaging[$packagingCaffee['PackagingCaffee']['state_packaging_id']]); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Imprimir'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=1&oie=".$packagingCaffee['PackagingCaffee']['id']); 
			echo $this->Html->link(__('Modificar'), array('action' => 'edit', $packagingCaffee['PackagingCaffee']['id']));?>
                    <?php if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 1 || $packagingCaffee['PackagingCaffee']['state_packaging_id'] == 2){
                        echo $this->Html->link(__('Cancelar'), array('action' => 'allOie', $packagingCaffee['PackagingCaffee']['id'],'cancel'),array('confirm'=>('Desea cancelar la OIE '.$packagingCaffee['PackagingCaffee']['id'].'?'))); 
                    }?>
                    <?php if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3 || $packagingCaffee['PackagingCaffee']['state_packaging_id'] == 5){
                        echo $this->Html->link(__('Vaciar'), array('action' => 'unpack', $packagingCaffee['PackagingCaffee']['id']));
                    }?>
			
                    <?php if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 2){
                        echo $this->Html->link(__('Remesas amparadas'), array('controller'=> 'DetailPackagingCaffees','action' => 'add', '?' => ['oie_id' => $packagingCaffee['PackagingCaffee']['id']]));
                        //echo $this->Html->link(__('Embalar'), array('action' => 'allOie', $packagingCaffee['PackagingCaffee']['id'],'pack'),array('confirm'=>('Desea embalar la OIE '.$packagingCaffee['PackagingCaffee']['id'].'?'))); 
                    }?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'Pages', 'action' => 'colaborador')); ?></li>
        <li><?php echo $this->Html->link(__('Búsqueda'), array('action' => 'search')); ?></li>
        <li><?php echo $this->Html->link(__('Reportes'), array('action' => 'report')); ?></li>
    </ul>
</div>