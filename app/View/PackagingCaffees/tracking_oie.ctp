<div class="packagingCaffees form">
<?php echo $this->Html->script('jquery.min');
		echo $this->Html->script('jquery-ui.min');
		echo $this->Html->css('jquery-ui.min');
		echo $this->Form->create('PackagingCaffee',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Subir Trazabilidad Contenedor '.$findPackagingCaffee['ViewPackaging']['bic_ctn']); ?></legend>
                <dl>
		<dt><?php 
//			debug($packagingCaffee);exit;
			echo __('Oie'); ?></dt>
		<dd>
			<?php echo h($findPackagingCaffee['ViewPackaging']['oie']); ?>
			&nbsp;
		</dd>
                
                <dt><?php echo __('Lotes'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.centerId') == 1){
                            echo h($findPackagingCaffee['ViewPackaging']['lotes']); 
                        }
                        else{
                            echo h($findPackagingCaffee['ViewPackaging']['observation']);
                        }
                        ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Exportador'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.centerId') == 1){
                            echo h($findPackagingCaffee['ViewPackaging']['exportador']);
                        }
                        else{
                            echo h($findPackagingCaffee['ViewPackaging']['client2_name']);
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expotador Code'); ?></dt>
		<dd>
			<?php 
                         if($this->Session->read('User.centerId') == 1){
                             echo h($findPackagingCaffee['ViewPackaging']['expotador_code']); 
                         }
                         else{
                             echo h($findPackagingCaffee['ViewPackaging']['exporter_code2']); 
                         }
                            ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Total Sacos'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.centerId') == 1){
                            echo h($findPackagingCaffee['ViewPackaging']['total_sacos']); 
                        }
                        else{
                            echo h($findPackagingCaffee['ViewPackaging']['qta_bags2']); 
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modalidad embalaje'); ?></dt>
		<dd>
			<?php echo h($findPackagingCaffee['ViewPackaging']['packaging_mode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo embalaje'); ?></dt>
		<dd>
			<?php echo h($findPackagingCaffee['ViewPackaging']['packaging_type']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Fecha Embalaje'); ?></dt>
                <dd>
			<?php echo h($findPackagingCaffee['ViewPackaging']['packaging_date']); ?>
			&nbsp;
		</dd>
                </dl>
	<?php
                echo $this->Form->label('file_track_oie','Cargue archivo trazabilidad');
		echo $this->Form->file('file_track_oie');
	?>
	</fieldset>   
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Tarja'), array('action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>
