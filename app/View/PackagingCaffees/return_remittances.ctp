<div class="detailPackagingCaffees form">
    <?php
    echo $this->Html->script('jquery.min');
    echo $this->Form->create('PackagingCaffee');
    ?>
    <fieldset>
        <legend><?php echo __('Devolución de Remesas de la OIE ' . $oie); ?></legend>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('remittance', 'Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantityBags', 'Cantidad de Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantityPallets', 'Cantidad de Pallets'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($DataToReturn as $data): ?>
                    <tr>
                        <td><?php echo h($data['remittance']); ?>&nbsp;</td>
                        <td><?php echo h($data['quantityBags']); ?>&nbsp;</td>
                        <td><?php echo h($data['quantityPallets']); ?>&nbsp;</td>
                        <td class="actions">
                            <a href="javascript:void(0)" onclick="addToReturn(<?php echo $data['remittance']?>,<?php echo $data['quantityBags']?>,<?php echo $data['quantityPallets']?>);">Agregar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <table>
            <tr>
                <td><?php echo $this->Form->input('remittance', array('type' => 'hidden','required' => true)); ?></td>
                <td><?php echo $this->Form->input('remittanceValue', array('labelValue' => 'Remesa', 'type' => 'text','disabled' => true)); ?></td>                
                <td><?php echo $this->Form->input('warehouse', array('label' => 'Bodega','required' => true,'options' => $warehouse,'empty' => true)); ?></td>
                <td><?php echo $this->Form->input('warehouse_position', array('label' => 'Ubicación','required' => true,'options' => array('' => 'Seleccione...') )); ?></td>
                <td><?php echo $this->Form->input('bags_out', array('required' => true, 'type'=>'hidden')); ?></td>
                <td><?php echo $this->Form->input('pallets_out', array('required' => true, 'type'=>'hidden' )); ?></td>
            </tr>
        </table>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript">
    $('#PackagingCaffeeWarehouse').change(function () {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/siscafe-web/SlotStores/findAvailableSlotStoreByStoreId/" + $('#PackagingCaffeeWarehouse').val(),
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    $('#PackagingCaffeeWarehousePosition').children().remove();
                    $.each(data, function (i, value) {
                        var html = '<option value='+i+'>'+value+'</option>';
                        $('#PackagingCaffeeWarehousePosition').append(html);
                    });
                } else {
                    $('#PackagingCaffeeWarehousePosition').children().remove();
                }
            }
        });
    });

    function addToReturn(remittanceId,bagsOut,palletsOut) {
        $('#PackagingCaffeeRemittance').val(remittanceId);
        $('#PackagingCaffeeRemittanceValue').val(remittanceId);
        $('#PackagingCaffeeBagsOut').val(bagsOut);
        $('#PackagingCaffeePalletsOut').val(palletsOut);
    }
</script>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('controller' => 'PackagingCaffees', 'action' => 'allOie')); ?></li>
    </ul>
</div>
