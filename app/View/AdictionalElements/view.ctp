<div class="adictionalElements view">
<h2><?php echo __('Adictional Element'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($adictionalElement['AdictionalElement']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Ext'); ?></dt>
		<dd>
			<?php echo h($adictionalElement['AdictionalElement']['code_ext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($adictionalElement['AdictionalElement']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($adictionalElement['AdictionalElement']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($adictionalElement['AdictionalElement']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated Date'); ?></dt>
		<dd>
			<?php echo h($adictionalElement['AdictionalElement']['updated_date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Adictional Element'), array('action' => 'edit', $adictionalElement['AdictionalElement']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Adictional Element'), array('action' => 'delete', $adictionalElement['AdictionalElement']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $adictionalElement['AdictionalElement']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Adictional Elements'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adictional Element'), array('action' => 'add')); ?> </li>
	</ul>
</div>
