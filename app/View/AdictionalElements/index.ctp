<div class="adictionalElements index">
	<h2><?php echo __('Adictional Elements'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('code_ext'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_date'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($adictionalElements as $adictionalElement): ?>
	<tr>
		<td><?php echo h($adictionalElement['AdictionalElement']['id']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElement']['code_ext']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElement']['name']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElement']['description']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElement']['created_date']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElement']['updated_date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $adictionalElement['AdictionalElement']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $adictionalElement['AdictionalElement']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $adictionalElement['AdictionalElement']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $adictionalElement['AdictionalElement']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Adictional Element'), array('action' => 'add')); ?></li>
	</ul>
</div>
