<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con,"  SELECT  date(rc.created_date) AS fecha, 
                                DATE_FORMAT(created_date, '%d') AS dias,                               
                                DATE_FORMAT(created_date, '%b') AS mes,
                                count(distinct rc.vehicle_plate) AS total_vehiculos
                        FROM remittances_caffee AS rc
                        WHERE MONTH(rc.created_date) = MONTH(CURDATE()) AND YEAR(rc.created_date) = YEAR(CURDATE()) AND rc.jetty=1
                        GROUP BY date(rc.created_date) 
                        ORDER BY rc.created_date ASC   ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Fecha descargue', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                        );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      
      $temp[] = array('v' => (string) $r['mes'].'-'.(string) $r['dias']); 

      $temp[] = array('v' => (int) $r['total_vehiculos']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          
           var view = new google.visualization.DataView(data);
                      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" }]);

           var options = {
              title: 'Vehiculos Descargados en el MES',
              vAxis: {title: 'Cantidad'},
              legend: { position: "none" },
              //seriesType: 'bars',
              series: {3: {type: 'line'}},
              width: 720,
            };

            
          var chart = new google.visualization.AreaChart(document.getElementById('chart_div_vehicleDownloadMonths'));
          chart.draw(view, options);
      }     


    </script>


    

