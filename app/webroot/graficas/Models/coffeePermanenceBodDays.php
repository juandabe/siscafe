<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con," SELECT ss.stores_caffee_id AS bodega, sum(rc.quantity_bag_in_store) AS ingresados
                        FROM remittances_caffee AS rc
                        INNER JOIN slot_store ss on ss.id=rc.slot_store_id 
			WHERE (rc.state_operation_id =2 or rc.state_operation_id = 3 or rc.state_operation_id = 7) and rc.jetty=1
                        GROUP BY ss.stores_caffee_id  ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Bodega', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      // the following line will be used to slice the Pie chart
      $temp[] = array('v' => 'Bod '.(string) $r['bodega']); 

      // Values of each slice
      $temp[] = array('v' => (int) $r['ingresados']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
          
            var data = new google.visualization.DataTable(<?=$jsonTable?>);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" }]);

            var options = {
                title: 'Existencia por bodega',
                width: 380,
                bar: {groupWidth: "70%"},
                legend: { position: "none" },
            };

            var chart = new google.visualization.BarChart(document.getElementById("chart_div_coffeePermanenceBodDays"));
            chart.draw(view, options);

        }

    </script>

