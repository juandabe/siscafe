<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con," SELECT ss.stores_caffee_id AS bodega, sum(rc.quantity_radicated_bag_in) AS radicados,
                                DATE_FORMAT(rc.created_date, '%d') AS dias,                               
                                DATE_FORMAT(rc.created_date, '%b') AS mes 
                        FROM remittances_caffee AS rc
                        INNER JOIN slot_store ss on ss.id=rc.slot_store_id 
                        WHERE YEARWEEK(rc.created_date) = YEARWEEK(CURDATE()) and rc.jetty=1                        
                        GROUP BY date(rc.created_date), ss.stores_caffee_id
                        ORDER BY rc.created_date ASC  ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Bodega', 'type' => 'string'),
                          array('label' => 'Total', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      // the following line will be used to slice the Pie chart
      $temp[] = array('v' => 'B'.(string) $r['bodega'].' ('.(string) $r['dias'].'-'.(string) $r['mes'].')'); 

      // Values of each slice
      $temp[] = array('v' => (int) $r['radicados']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          
          var view = new google.visualization.DataView(data);
          view.setColumns([0, 1,
                               { calc: "stringify",
                                 sourceColumn: 1,
                                 type: "string",
                                 role: "annotation" }]);

          var options = {
              title: 'Café descargados por bodegas en la SEMANA',
              vAxis: {title: 'Cantidad'},
              seriesType: 'bars',
              legend: { position: "none" },
              series: {3: {type: 'line'}},
              width: 480,
          };

            
          var chart = new google.visualization.ComboChart(document.getElementById('chart_div_coffeeDownloadBodWeek'));
          chart.draw(view, options);
      }     


    </script>

