use siscafe_pro;
/*
	--transaction son los functiones del controller (edit, index, delete, add)
*/
CREATE TABLE transaction (
	id int(10) NOT NULL auto_increment,
	transaction_name text NULL,
    reg_date datetime default CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

/*
	--transaction_log registra los movimientos de las actividades de los usuarios.
*/
CREATE TABLE transaction_log (
	id int(10) NOT NULL auto_increment,
	transaction_id int(10) NOT NULL ,
	module varchar(2) NOT NULL,
	user_id int(10) unsigned NOT NULL,
    reg_date datetime default CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE `category_permits`;
DROP TABLE `permits`;
SET FOREIGN_KEY_CHECKS = 1;


/*
	--permits registra permisos de los usuarios a las transacciones (edit, index, delete, add)
*/
CREATE TABLE `permits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) NOT NULL,
  `created_date` datetime default CURRENT_TIMESTAMP,
  `updated_date` datetime NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_permits_transaction_idx` (`transaction_id`),
  CONSTRAINT `fk_permits_transaction` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  KEY `fk_permits_users_idx` (`user_id`),
  CONSTRAINT `fk_permits_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

ALTER TABLE transaction_log
ADD FOREIGN KEY (transaction_id) REFERENCES transaction(id);

ALTER TABLE transaction_log
ADD FOREIGN KEY (user_id) REFERENCES users(id);



INSERT INTO `transaction` (`transaction_name`) VALUES ('delete - DetailsServicesToCaffees');
INSERT INTO `transaction` (`transaction_name`) VALUES ('add - DetailsServicesToCaffees');
INSERT INTO `transaction` (`transaction_name`) VALUES ('delete_supply - DetailsServicesToCaffees');
INSERT INTO `transaction` (`transaction_name`) VALUES ('delete_crossdocking - DetailsServicesToCaffees');
INSERT INTO `transaction` (`transaction_name`) VALUES ('completed - DetailsServicesToCaffees');



INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (2,1,'2024-01-23 02:42:02',NULL,1);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (3,2,'2024-01-23 02:42:06',NULL,1);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (4,3,'2024-01-23 02:42:08',NULL,1);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (5,4,'2024-01-23 02:42:10',NULL,1);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (7,1,'2024-01-24 09:41:10',NULL,198);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (8,2,'2024-01-24 09:41:10',NULL,198);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (9,3,'2024-01-24 09:41:10',NULL,198);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (10,4,'2024-01-24 09:41:10',NULL,198);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (11,1,'2024-01-24 09:41:11',NULL,33);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (12,2,'2024-01-24 09:41:11',NULL,33);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (13,3,'2024-01-24 09:41:11',NULL,33);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (14,4,'2024-01-24 09:41:11',NULL,33);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (15,1,'2024-01-24 09:41:11',NULL,448);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (16,2,'2024-01-24 09:41:12',NULL,448);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (17,3,'2024-01-24 09:41:12',NULL,448);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (18,4,'2024-01-24 09:41:12',NULL,448);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (19,1,'2024-01-24 09:41:12',NULL,240);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (20,2,'2024-01-24 09:41:12',NULL,240);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (21,3,'2024-01-24 09:41:12',NULL,240);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (22,4,'2024-01-24 09:41:13',NULL,240);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (23,1,'2024-01-24 22:58:00',NULL,104);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (24,2,'2024-01-24 22:58:00',NULL,104);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (25,3,'2024-01-24 22:58:01',NULL,104);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (26,4,'2024-01-24 22:58:01',NULL,104);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (27,5,'2024-01-24 23:04:18',NULL,104);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (28,5,'2024-01-24 23:04:18',NULL,240);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (29,5,'2024-01-24 23:04:19',NULL,448);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (30,5,'2024-01-24 23:04:20',NULL,33);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (31,5,'2024-01-24 23:04:20',NULL,198);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (32,5,'2024-01-24 23:04:20',NULL,1);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (33,1,'2024-01-25 13:51:20',NULL,195);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (34,2,'2024-01-25 13:51:20',NULL,195);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (35,3,'2024-01-25 13:51:20',NULL,195);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (36,4,'2024-01-25 13:51:20',NULL,195);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (37,5,'2024-01-25 13:51:20',NULL,195);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (38,1,'2024-01-25 13:51:20',NULL,317);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (39,2,'2024-01-25 13:51:20',NULL,317);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (40,3,'2024-01-25 13:51:20',NULL,317);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (41,4,'2024-01-25 13:51:20',NULL,317);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (42,5,'2024-01-25 13:51:20',NULL,317);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (43,1,'2024-01-25 13:51:20',NULL,270);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (44,2,'2024-01-25 13:51:20',NULL,270);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (45,3,'2024-01-25 13:51:20',NULL,270);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (46,4,'2024-01-25 13:51:20',NULL,270);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (47,5,'2024-01-25 13:51:20',NULL,270);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (48,1,'2024-01-25 13:51:20',NULL,204);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (49,2,'2024-01-25 13:51:20',NULL,204);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (50,3,'2024-01-25 13:51:20',NULL,204);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (51,4,'2024-01-25 13:51:20',NULL,204);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (52,5,'2024-01-25 13:51:20',NULL,204);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (53,1,'2024-01-25 13:51:20',NULL,314);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (54,2,'2024-01-25 13:51:20',NULL,314);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (55,3,'2024-01-25 13:51:20',NULL,314);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (56,4,'2024-01-25 13:51:20',NULL,314);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (57,5,'2024-01-25 13:51:20',NULL,314);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (58,1,'2024-01-25 13:51:20',NULL,171);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (59,2,'2024-01-25 13:51:20',NULL,171);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (60,3,'2024-01-25 13:51:20',NULL,171);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (61,4,'2024-01-25 13:51:20',NULL,171);
INSERT INTO `` (`id`,`transaction_id`,`created_date`,`updated_date`,`user_id`) VALUES (62,5,'2024-01-25 13:51:20',NULL,171);
