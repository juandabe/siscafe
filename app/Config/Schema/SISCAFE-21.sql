ALTER TABLE `remittances_caffee_has_noveltys_caffee` 
ADD COLUMN `user_register` INT NULL AFTER `active`,
ADD COLUMN `delete_date` DATETIME NULL AFTER `user_register`,
ADD COLUMN `user_delete` INT NULL AFTER `delete_date`,
ADD COLUMN `observation` VARCHAR(250) NULL AFTER `user_delete`;