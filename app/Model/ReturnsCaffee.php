<?php
App::uses('AppModel', 'Model');
/**
 * ReturnsCaffee Model
 *
 * @property RemittancesCaffee $RemittancesCaffee
 */
class ReturnsCaffee extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'joinTable' => 'remittances_caffee_returns_caffees',
			'foreignKey' => 'returns_caffee_id',
			'associationForeignKey' => 'remittances_caffee_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

        public $belongsTo = array(
            'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'client_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
	    'Departament' => array(
			'className' => 'Departament',
			'foreignKey' => 'jetty',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
            );
}
