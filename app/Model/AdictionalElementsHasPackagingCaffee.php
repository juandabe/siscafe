<?php
App::uses('AppModel', 'Model');
/**
 * AdictionalElementsHasPackagingCaffee Model
 *
 */
class AdictionalElementsHasPackagingCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'adictional_elements_has_packaging_caffee';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'adictional_elements_id,info_navy_id';
	
	public $belongsTo = array( 
	  'AdictionalElement' => array( 'className' => 'AdictionalElement', 
	  'foreignKey' => 'adictional_elements_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ),
	  'InfoNavy' => array( 'className' => 'InfoNavy', 
	  'foreignKey' => 'info_navy_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' )); 


}
