<?php
App::uses('AppModel', 'Model');
/**
 * DetailServicesOrderCrossdocking Model
 *
 */
class DetailServicesOrderCrossdocking extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'detail_services_order_crossdocking';

	public $belongsTo = array(
        'ServicesOrder' => array(
            'className' => 'ServicesOrder',
            'foreignKey' => 'services_orders_id'
        )
    );
}
