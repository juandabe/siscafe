<?php
App::uses('AppModel', 'Model');
/**
 * CakeSession Model
 *
 */
class CakeSession extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';
	public $useTable = 'cake_sessions';

}
