<?php
App::uses('AppModel', 'Model');
/**
 * MarkCaffee Model
 *
 */
class MarkCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mark_caffee';

	public $hasMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

/**
 * Display field
 *
 * @var string
 */
        
	public $virtualFields = array('name' => "CONCAT(MarkCaffee.id, ' - ', MarkCaffee.name)");

}
