<?php

App::uses('AppModel', 'Model');

/**
 * ItemsService Model
 *
 */
class ItemsService extends AppModel {

    public $useTable = 'items_services';
    
    public $hasMany = array(
        'ServicePackageHasItemsService' => array(
            'className' => 'ServicePackageHasItemsService',
            'foreignKey' => 'items_services_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
