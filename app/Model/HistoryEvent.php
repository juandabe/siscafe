<?php
App::uses('AppModel', 'Model');
/**
 * HistoryEvent Model
 *
 */
class HistoryEvent extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	var $name = "HistoryEvent";
	public $useTable = 'history_event';
	
}
