<?php
App::uses('AppModel', 'Model');
/**
 * Custom Model
 *
 */
class Custom extends AppModel {
    
    /**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'customs';

        
        
        public $hasMany = array(
		'InfoNavy' => array(
			'className' => 'InfoNavy',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);
        /**
 * Display field
 *
 * @var string
 */
        
}
