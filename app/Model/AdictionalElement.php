<?php
App::uses('AppModel', 'Model');
/**
 * AdictionalElement Model
 *
 */
class AdictionalElement extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'adictional_elements';
	
	public $hasMany = array(
	'AdictionalElementsHasPackagingCaffee' => array(
			'className' => 'AdictionalElementsHasPackagingCaffee',
			'foreignKey' => 'adictional_elements_id',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	));

}
