<?php
App::uses('AppModel', 'Model');
/**
 * SlotStore Model
 *
 */
class SlotStore extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'slot_store';
        
        public $belongsTo = array(
		'StoresCaffee' => array(
			'className' => 'StoresCaffee',
			'foreignKey' => 'stores_caffee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);
        
        public $hasMany = array(
	'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	));

}
