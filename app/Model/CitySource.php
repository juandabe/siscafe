<?php
App::uses('AppModel', 'Model');
/**
 * CitySource Model
 *
 */
class CitySource extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'city_source';

	public $belongsTo = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
