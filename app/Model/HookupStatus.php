<?php
App::uses('AppModel', 'Model');
/**
 * CitySource Model
 *
 */
class HookupStatus extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'hookup_status';

	public $belongsTo = array(
		'FumigationService' => array(
			'className' => 'FumigationService',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ServicesOrder' => array(
			'className' => 'ServicesOrder',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

}
