<?php

App::uses('AppModel', 'Model');

/**
 * ServicesOrder Model
 *
 */
class ServicesOrder extends AppModel {

    public $hasMany = array(
        'DetailsServicesToCaffee' => array(
            'className' => 'DetailsServicesToCaffee',
            'foreignKey' => 'services_orders_id',
            'conditions' => '',
            'dependent' => false,
            'fields' => '',
            'order' => ''
        ),
        'ServicesOrdersBilling' => array(
            'className' => 'ServicesOrdersBilling',
            'foreignKey' => 'id_services_order',
            'conditions' => '',
            'dependent' => false,
            'fields' => '',
            'order' => ''
        ),
        'DetailServicesOrderCrossdocking' => array(
            'className' => 'DetailServicesOrderCrossdocking',
            'foreignKey' => 'services_orders_id',
            'conditions' => '',
            'dependent' => false,
            'fields' => '',
            'order' => ''
        ));

    public $belongsTo = array(
        'HookupStatus' => array(
            'className' => 'HookupStatus',
            'foreignKey' => 'hookup_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'exporter_code',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Departament' => array(
			'className' => 'Departament',
			'foreignKey' => 'departaments_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
    );
}
