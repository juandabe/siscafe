<?php
App::uses('AppModel', 'Model');
/**
 * BasculeVerification Model
 *
 */
class BasculeVerification extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'bascule_verification';

}
