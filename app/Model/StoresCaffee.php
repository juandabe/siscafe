<?php
App::uses('AppModel', 'Model');
/**
 * StoresCaffee Model
 *
 */
class StoresCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'stores_caffee';
        
        public $belongsTo = array(
		'SlotStore' => array(
			'className' => 'SlotStore',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);

}
