<?php
App::uses('AppModel', 'Model');
/**
 * ListPricesUnoee Model
 *
 */
class ListPricesUnoee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'list_prices_unoee';

}
