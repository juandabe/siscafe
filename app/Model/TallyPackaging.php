<?php
App::uses('AppModel', 'Model');
/**
 * TallyPackaging Model
 *
 */
class TallyPackaging extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tally_packaging';
	
	public $belongsTo = array(
		'Users' => array(
			'className' => 'Users',
			'foreignKey' => 'users_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		);
		
	public $hasMany = array(
	'AdictionalElementsHasTallyPackaging' => array(
			'className' => 'AdictionalElementsHasTallyPackaging',
			'foreignKey' => 'tally_packaging_id',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	),
);
	
	
	public $validate = array(
        'bic_container' => array(
            'rule' => array('minLength', '11'),
            'required' => true,
            'message' => 'Por favor ingrese debidamente el número de contenedor.'
        
        ));

}
