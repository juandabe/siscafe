<?php
App::uses('AppModel', 'Model');
/**
 * ServicesOrdersBilling Model
 *
 */
class ServicesOrdersBilling extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'services_orders_billing';

	public $belongsTo = array(
        'ServicesOrder' => array(
            'className' => 'ServicesOrder',
            'foreignKey' => 'id_services_order'
		));

}
