<?php
App::uses('AppModel', 'Model');
/**
 * CitySource Model
 *
 */
class ScheduleCoffeeFumigation extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'schedule_coffee_fumigation';

	public $belongsTo = array(
		'FumigationService' => array(
			'className' => 'FumigationService',
			'foreignKey' => 'fumigation_services_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
