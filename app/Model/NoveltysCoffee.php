<?php

App::uses('AppModel', 'Model');

/**
 * Custom Model
 *
 */
class NoveltysCoffee extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'noveltys_caffee';
    
    public $hasMany = array(
            'RemittancesCaffeeHasNoveltysCaffee' => array(
                            'className' => 'RemittancesCaffeeHasNoveltysCaffee',
                            'foreignKey' => 'noveltys_caffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ));

}
