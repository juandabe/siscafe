<?php
App::uses('AppModel', 'Model');
/**
 * TraceabilityDownload Model
 *
 */
class TraceabilityDownload extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'traceability_download';
        
        public $belongsTo = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
