<?php

App::uses('AppModel', 'Model');

/**
 * DetailsServicesToCaffe Model
 *
 */
class DetailServiceOrderCrossdocking extends AppModel {

    public $useTable = 'detail_services_order_crossdocking';
    
    public $belongsTo = array(
        'ServicesOrder' => array(
            'className' => 'ServicesOrder',
            'foreignKey' => 'services_orders_id'
        )
    );
}
