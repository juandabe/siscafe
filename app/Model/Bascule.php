<?php
App::uses('AppModel', 'Model');
/**
 * Bascule Model
 *
 */
class Bascule extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'bascule';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
        
         public $hasMany = array(
                'User' => array(
			'className' => 'User',
			'foreignKey' => 'id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		));

}
