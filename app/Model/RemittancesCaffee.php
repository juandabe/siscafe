<?php
App::uses('AppModel', 'Model');
/**
 * RemittancesCaffee Model
 *
 */
class RemittancesCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'remittances_caffee';
        
        public $belongsTo = array(
		'SlotStore' => array(
			'className' => 'SlotStore',
			'foreignKey' => 'slot_store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'MaterialCoffee' => array(
			'className' => 'MaterialCoffee',
			'foreignKey' => 'material',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bill' => array(
			'className' => 'Bill',
			'foreignKey' => 'bill_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		,
                'StateOperation' => array(
			'className' => 'StateOperation',
			'foreignKey' => 'state_operation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'Users1' => array(
                            'className' => 'Users',
                            'foreignKey' => 'staff_sample_id',
                            'conditions' => '',
                            'fields' => '',
                            'order' => ''
                    ),
                'Users2' => array(
                            'className' => 'Users',
                            'foreignKey' => 'staff_driver_id',
                            'conditions' => '',
                            'fields' => '',
                            'order' => ''
                    ),
		'Users3' => array(
                            'className' => 'Users',
                            'foreignKey' => 'staff_wt_in_id',
                            'conditions' => '',
                            'fields' => '',
                            'order' => ''
                    ),
                'UnitsCaffee' => array(
			'className' => 'UnitsCaffee',
			'foreignKey' => 'units_cafee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'TypeUnit' => array(
			'className' => 'TypeUnit',
			'foreignKey' => 'type_units_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'NoveltyInCaffee' => array(
			'className' => 'NoveltyInCaffee',
			'foreignKey' => 'novelty_in_caffee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'PackingCaffee' => array(
			'className' => 'PackingCaffee',
			'foreignKey' => 'packing_cafee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Shipper' => array(
			'className' => 'Shipper',
			'foreignKey' => 'shippers_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'TraceabilityDownload' => array(
			'className' => 'TraceabilityDownload',
			'foreignKey' => 'traceability_download_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'client_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'MarkCaffee' => array(
			'className' => 'MarkCaffee',
			'foreignKey' => 'mark_cafee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);
	
	public $hasMany = array(
            'DetailPackagingCaffee' => array(
                            'className' => 'DetailPackagingCaffee',
                            'foreignKey' => 'remittances_caffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ),
            'RemittancesCaffeeHasFumigationService' => array(
                            'className' => 'RemittancesCaffeeHasFumigationService',
                            'foreignKey' => 'remittances_caffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
                    ),
            'RemittancesCaffeeHasNoveltysCaffee' => array(
                            'className' => 'RemittancesCaffeeHasNoveltysCaffee',
                            'foreignKey' => 'remittances_caffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
                    ));
        
        public $hasAndBelongsToMany = array(
		'ReturnsCaffee' => array(
			'className' => 'ReturnsCaffee',
			'joinTable' => 'remittances_caffee_returns_caffees',
			'foreignKey' => 'remittances_caffee_id',
			'associationForeignKey' => 'returns_caffee_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
        
}

