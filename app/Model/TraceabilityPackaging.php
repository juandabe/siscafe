<?php
App::uses('AppModel', 'Model');
/**
 * TraceabilityPackaging Model
 *
 */
class TraceabilityPackaging extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'traceability_packaging';

	public $belongsTo = array(
		'PackagingCaffee' => array(
			'className' => 'PackagingCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
