<?php

App::uses('AppModel', 'Model');

/**
 * DetailsServicesToCaffe Model
 *
 */
class DetailsServicesToCaffee extends AppModel {

    public $useTable = 'details_services_to_caffee';
    
    public $belongsTo = array(
        'ServicesOrder' => array(
            'className' => 'ServicesOrder',
            'foreignKey' => 'services_orders_id'
        ),
        'RemittancesCaffee' => array(
            'className' => 'RemittancesCaffee',
            'foreignKey' => 'remittances_caffee_id'
        )
    );
}
