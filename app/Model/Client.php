<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 */
class Client extends AppModel {
    
    public $useTable = 'clients';
    
    public $hasMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'OperationTracking' => array(
			'className' => 'OperationTracking',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ServicesOrder' => array(
			'className' => 'ServicesOrder',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FumigationService' => array(
			'className' => 'FumigationService',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);

}
