<?php
/**
 * ReadyContainer Fixture
 */
class ReadyContainerFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'ready_container';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'bic_container' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'date_registre' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'shipping_lines_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_ready_container_shipping_lines1_idx' => array('column' => 'shipping_lines_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'bic_container' => 'Lorem ips',
			'date_registre' => '2017-03-15 16:48:22',
			'shipping_lines_id' => 1
		),
	);

}
