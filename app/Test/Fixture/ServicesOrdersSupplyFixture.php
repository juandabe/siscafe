<?php
/**
 * ServicesOrdersSupply Fixture
 */
class ServicesOrdersSupplyFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'id_supply' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'cantidad' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'observation' => array('type' => 'string', 'null' => true, 'length' => 500, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'terminal_arrive' => array('type' => 'string', 'null' => true, 'length' => 200, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'is_move' => array('type' => 'integer', 'null' => true, 'default' => '0', 'length' => 4, 'unsigned' => false),
		'id_services_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'date_reg' => array('type' => 'date', 'null' => false, 'default' => null),
		'date_update' => array('type' => 'date', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'services_orders_supplies_FK' => array('column' => 'id_supply', 'unique' => 0),
			'services_orders_supplies_FK_1' => array('column' => 'id_services_order', 'unique' => 0),
			'services_orders_supplies_FK_2' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'id_supply' => 1,
			'cantidad' => 1,
			'observation' => 'Lorem ipsum dolor sit amet',
			'terminal_arrive' => 'Lorem ipsum dolor sit amet',
			'is_move' => 1,
			'id_services_order' => 1,
			'date_reg' => '2021-05-13',
			'date_update' => '2021-05-13',
			'user_id' => 1
		),
	);

}
