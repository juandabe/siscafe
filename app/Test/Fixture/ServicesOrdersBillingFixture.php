<?php
/**
 * ServicesOrdersBilling Fixture
 */
class ServicesOrdersBillingFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'services_orders_billing';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id_services_order' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => true),
		'lot_caffee' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'items_unoee' => array('type' => 'string', 'null' => false, 'length' => 250, 'collate' => 'utf8mb4_0900_ai_ci', 'charset' => 'utf8mb4'),
		'lista_precio' => array('type' => 'string', 'null' => false, 'length' => 250, 'collate' => 'utf8mb4_0900_ai_ci', 'charset' => 'utf8mb4'),
		'qty' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => 32, 'unsigned' => false),
		'value' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8mb4_0900_ai_ci', 'charset' => 'utf8mb4'),
		'date_reg' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'observation' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'charset' => 'utf8mb4'),
		'observation2' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'charset' => 'utf8mb4'),
		'indexes' => array(
			
		),
		'tableParameters' => array('comment' => 'VIEW')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id_services_order' => 1,
			'lot_caffee' => 'Lorem ip',
			'items_unoee' => 'Lorem ipsum dolor sit amet',
			'lista_precio' => 'Lorem ipsum dolor sit amet',
			'qty' => '',
			'value' => 'Lorem ipsum dolor sit amet',
			'date_reg' => '2021-06-29 05:34:46',
			'observation' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'observation2' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);

}
