<?php
/**
 * DetailServicesOrderCrossdocking Fixture
 */
class DetailServicesOrderCrossdockingFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'detail_services_order_crossdocking';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'services_orders_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'service_package_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'lot_coffee' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'qta_coffee' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'status' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false),
		'reg_date' => array('type' => 'timestamp', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_services_orders_id' => array('column' => 'services_orders_id', 'unique' => 0),
			'service_package_id' => array('column' => 'service_package_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'services_orders_id' => 1,
			'service_package_id' => 1,
			'lot_coffee' => 'Lorem ipsum dolor sit amet',
			'qta_coffee' => 1,
			'status' => 1,
			'reg_date' => 1620849306
		),
	);

}
