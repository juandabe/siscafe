<?php
/**
 * ItemsUnoee Fixture
 */
class ItemsUnoeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'items_unoee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'company' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'codigo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'key' => 'index', 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'descripcion' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 400, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'tipo_servicio' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'unidad_medida' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'costo_estandar' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'grupo_impositivo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'des_grupo_impositivo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'cod_lista_precio' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 4, 'key' => 'index', 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'tipo_invent' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'codigo_idx1' => array('column' => 'codigo', 'unique' => 0),
			'rid_lista_idx2' => array('column' => 'cod_lista_precio', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company' => '',
			'codigo' => 'Lorem ip',
			'descripcion' => 'Lorem ipsum dolor sit amet',
			'tipo_servicio' => 'Lorem ipsum dolor sit amet',
			'unidad_medida' => 'Lorem ipsum dolor sit amet',
			'costo_estandar' => 1,
			'grupo_impositivo' => 'Lorem ipsum dolor sit amet',
			'des_grupo_impositivo' => 'Lorem ipsum dolor sit amet',
			'cod_lista_precio' => 'Lo',
			'tipo_invent' => 'Lorem ipsum dolor '
		),
	);

}
