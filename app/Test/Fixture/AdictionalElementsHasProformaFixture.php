<?php
/**
 * AdictionalElementsHasProforma Fixture
 */
class AdictionalElementsHasProformaFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'adictional_elements_has_proforma';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'adictional_elements_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'tally_adictional_info_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'quantity' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'type_unit' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'observation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('adictional_elements_id', 'tally_adictional_info_id'), 'unique' => 1),
			'fk_adictional_elements_has_proforma_adictional_elements_idx' => array('column' => 'adictional_elements_id', 'unique' => 0),
			'fk_adictional_elements_has_proforma_tally_adictional_info_idx' => array('column' => 'tally_adictional_info_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'adictional_elements_id' => 1,
			'tally_adictional_info_id' => 1,
			'quantity' => 1,
			'type_unit' => 'Lorem ipsum dolor sit amet',
			'observation' => 'Lorem ipsum dolor sit amet'
		),
	);

}
