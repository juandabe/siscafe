<?php
/**
 * ItemsUnoeeConexo Fixture
 */
class ItemsUnoeeConexoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'ITEMS_UNOEE' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DESCRIPCION_ITEM' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 500, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LISTA_PRECIO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DESCRIPCION_LISTA_PRECIO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 500, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'RANGO_CANTIDAD_INICIAL' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'RANGO_CANTIDAD_FINAL' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'TARIFA' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'GRUPO_IMPOSITIVO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'UNIDAD_MEDIDA' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'TIPO_INVENTARIO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'TIPO_SERVICIO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'UNIDAD_FACTURACION' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false),
		'USE_PILCAF' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 4, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'ID', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'ID' => 1,
			'ITEMS_UNOEE' => 'Lorem ipsum dolor sit amet',
			'DESCRIPCION_ITEM' => 'Lorem ipsum dolor sit amet',
			'LISTA_PRECIO' => 'Lorem ipsum dolor sit amet',
			'DESCRIPCION_LISTA_PRECIO' => 'Lorem ipsum dolor sit amet',
			'RANGO_CANTIDAD_INICIAL' => 1,
			'RANGO_CANTIDAD_FINAL' => 1,
			'TARIFA' => 'Lorem ipsum dolor sit amet',
			'GRUPO_IMPOSITIVO' => 'Lorem ipsum dolor sit amet',
			'UNIDAD_MEDIDA' => 'Lorem ipsum dolor sit amet',
			'TIPO_INVENTARIO' => 'Lorem ipsum dolor sit amet',
			'TIPO_SERVICIO' => 'Lorem ipsum dolor sit amet',
			'UNIDAD_FACTURACION' => 1,
			'USE_PILCAF' => 1
		),
	);

}
