<?php
/**
 * TallyExpo Fixture
 */
class TallyExpoFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'tally_expo';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 6, 'unsigned' => true, 'key' => 'primary'),
		'lotes' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'booking' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'destino' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'linea' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'exportador' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cod_exportador' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'total_sacos' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'modalidad_embalaje' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tipo_embalaje' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bic_ctn' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'iso_ctn' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seal1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seal2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'weight_ctn' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_load_net' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_load_total' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'packaging_date' => array('type' => 'timestamp', 'null' => false, 'default' => null),
		'adictional_elements' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'lotes' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'booking' => 'Lorem ipsum dolor sit amet',
			'destino' => 'Lorem ipsum dolor sit amet',
			'linea' => 'Lorem ipsum dolor sit amet',
			'exportador' => 'Lorem ipsum dolor sit amet',
			'cod_exportador' => 'Lorem ipsum dolor sit amet',
			'total_sacos' => 1,
			'modalidad_embalaje' => 'Lorem ipsum dolor sit amet',
			'tipo_embalaje' => 'Lorem ipsum dolor sit amet',
			'bic_ctn' => 'Lorem ipsum dolor sit amet',
			'iso_ctn' => 'Lore',
			'seal1' => 'Lorem ipsum dolor ',
			'seal2' => 'Lorem ipsum dolor ',
			'weight_ctn' => 1,
			'weight_load_net' => 1,
			'weight_load_total' => 1,
			'packaging_date' => 1484320121,
			'adictional_elements' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);

}
