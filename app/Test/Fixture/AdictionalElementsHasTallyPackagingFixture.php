<?php
/**
 * AdictionalElementsHasTallyPackaging Fixture
 */
class AdictionalElementsHasTallyPackagingFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'adictional_elements_has_tally_packaging';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'adictional_elements_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'tally_packaging_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'qty' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'unit' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('adictional_elements_id', 'tally_packaging_id'), 'unique' => 1),
			'fk_adictional_elements_has_tally_packaging_tally_packaging1_idx' => array('column' => 'tally_packaging_id', 'unique' => 0),
			'fk_adictional_elements_has_tally_packaging_adictional_eleme_idx' => array('column' => 'adictional_elements_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'adictional_elements_id' => 1,
			'tally_packaging_id' => 1,
			'qty' => 1,
			'unit' => 'Lorem ipsum dolor sit amet'
		),
	);

}
