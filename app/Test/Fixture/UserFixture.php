<?php
/**
 * User Fixture
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'first_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 300, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'last_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 300, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'extension' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'profiles_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'active' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'departaments_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'cod_ext1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cod_city_operation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cod_ext2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ref_store' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bascule_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'bascule_id1' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'bascule_id2' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'rfid_reader2' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'rfid_reader1' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'city_alt1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'full_permission' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'session_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'ascii_bin', 'charset' => 'ascii'),
		'hash' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_users_profiles1_idx' => array('column' => 'profiles_id', 'unique' => 0),
			'fk_users_departaments1_idx' => array('column' => 'departaments_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'first_name' => 'Lorem ipsum dolor sit amet',
			'last_name' => 'Lorem ipsum dolor sit amet',
			'phone' => 'Lorem ipsum dolor sit amet',
			'extension' => 'Lorem ipsum dolor sit amet',
			'username' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'created_date' => '2024-01-22 02:28:57',
			'updated_date' => '2024-01-22 02:28:57',
			'profiles_id' => 1,
			'active' => 1,
			'departaments_id' => 1,
			'cod_ext1' => 'Lorem ip',
			'cod_city_operation' => 'Lorem ipsum dolor sit amet',
			'cod_ext2' => 'Lorem ipsum dolor sit amet',
			'ref_store' => 'Lorem ipsum dolor ',
			'bascule_id' => 1,
			'bascule_id1' => 1,
			'bascule_id2' => 1,
			'rfid_reader2' => 1,
			'rfid_reader1' => 1,
			'email' => 'Lorem ipsum dolor sit amet',
			'city_alt1' => 'Lorem ipsum dolor sit amet',
			'full_permission' => 1,
			'session_id' => 'Lorem ipsum dolor sit amet',
			'hash' => 'Lorem ipsum dolor sit amet'
		),
	);

}
