<?php
/**
 * ListPricesUnoee Fixture
 */
class ListPricesUnoeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'list_prices_unoee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'company' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'codigo' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index', 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'referencia' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index', 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'id_lista' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 4, 'key' => 'index', 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'descripcion_lista' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 500, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'precio' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'fecha_activacion' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'unidad_medida' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'moneda' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 5, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'codigo_idx1' => array('column' => 'codigo', 'unique' => 0),
			'refrencia_idx2' => array('column' => 'referencia', 'unique' => 0),
			'rid_lista_idx2' => array('column' => 'id_lista', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company' => '',
			'codigo' => 'Lorem ip',
			'referencia' => 'Lorem ip',
			'id_lista' => 'Lo',
			'descripcion_lista' => 'Lorem ipsum dolor sit amet',
			'precio' => 1,
			'fecha_activacion' => '2021-05-18 23:40:33',
			'unidad_medida' => 'Lorem ipsum dolor sit amet',
			'moneda' => 'Lor'
		),
	);

}
