<?php
App::uses('Permit', 'Model');

/**
 * Permit Test Case
 */
class PermitTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.permit',
		'app.transaction',
		'app.user',
		'app.profiles',
		'app.bascule',
		'app.departament',
		'app.info_navy',
		'app.navy_agent',
		'app.shipping_line',
		'app.custom',
		'app.packaging_caffee',
		'app.ready_container',
		'app.checking_ctn',
		'app.state_packaging',
		'app.services_order',
		'app.hookup_status',
		'app.fumigation_service',
		'app.client',
		'app.remittances_caffee',
		'app.slot_store',
		'app.stores_caffee',
		'app.material_coffee',
		'app.bill',
		'app.state_operation',
		'app.users',
		'app.units_caffee',
		'app.type_unit',
		'app.novelty_in_caffee',
		'app.packing_caffee',
		'app.shipper',
		'app.traceability_download',
		'app.mark_caffee',
		'app.detail_packaging_caffee',
		'app.remittances_caffee_has_fumigation_service',
		'app.remittances_caffee_has_noveltys_caffee',
		'app.noveltys_coffee',
		'app.returns_caffee',
		'app.remittances_caffee_returns_caffee',
		'app.operation_tracking',
		'app.schedule_coffee_fumigation',
		'app.details_services_to_caffee',
		'app.services_orders_billing',
		'app.detail_services_order_crossdocking',
		'app.traceability_packaging',
		'app.detail_packakging_crossdocking',
		'app.supplies_operation',
		'app.supply',
		'app.charge_massive_sample_coffee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Permit = ClassRegistry::init('Permit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Permit);

		parent::tearDown();
	}

}
