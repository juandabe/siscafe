<?php
App::uses('TallyExpo', 'Model');

/**
 * TallyExpo Test Case
 */
class TallyExpoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tally_expo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TallyExpo = ClassRegistry::init('TallyExpo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TallyExpo);

		parent::tearDown();
	}

}
