<?php
App::uses('ServicesOrdersSupply', 'Model');

/**
 * ServicesOrdersSupply Test Case
 */
class ServicesOrdersSupplyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.services_orders_supply'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ServicesOrdersSupply = ClassRegistry::init('ServicesOrdersSupply');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ServicesOrdersSupply);

		parent::tearDown();
	}

}
