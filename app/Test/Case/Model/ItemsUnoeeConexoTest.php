<?php
App::uses('ItemsUnoeeConexo', 'Model');

/**
 * ItemsUnoeeConexo Test Case
 */
class ItemsUnoeeConexoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.items_unoee_conexo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ItemsUnoeeConexo = ClassRegistry::init('ItemsUnoeeConexo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ItemsUnoeeConexo);

		parent::tearDown();
	}

}
