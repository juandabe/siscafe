<?php
App::uses('ItemsUnoee', 'Model');

/**
 * ItemsUnoee Test Case
 */
class ItemsUnoeeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.items_unoee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ItemsUnoee = ClassRegistry::init('ItemsUnoee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ItemsUnoee);

		parent::tearDown();
	}

}
