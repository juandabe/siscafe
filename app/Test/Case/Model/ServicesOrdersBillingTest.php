<?php
App::uses('ServicesOrdersBilling', 'Model');

/**
 * ServicesOrdersBilling Test Case
 */
class ServicesOrdersBillingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.services_orders_billing'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ServicesOrdersBilling = ClassRegistry::init('ServicesOrdersBilling');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ServicesOrdersBilling);

		parent::tearDown();
	}

}
