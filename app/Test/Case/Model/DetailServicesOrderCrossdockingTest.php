<?php
App::uses('DetailServicesOrderCrossdocking', 'Model');

/**
 * DetailServicesOrderCrossdocking Test Case
 */
class DetailServicesOrderCrossdockingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.detail_services_order_crossdocking'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DetailServicesOrderCrossdocking = ClassRegistry::init('DetailServicesOrderCrossdocking');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DetailServicesOrderCrossdocking);

		parent::tearDown();
	}

}
