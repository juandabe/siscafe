<?php
App::uses('RemittancesCaffee', 'Model');

/**
 * RemittancesCaffee Test Case
 */
class RemittancesCaffeeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.remittances_caffee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RemittancesCaffee = ClassRegistry::init('RemittancesCaffee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RemittancesCaffee);

		parent::tearDown();
	}

}
