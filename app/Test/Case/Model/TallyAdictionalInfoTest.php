<?php
App::uses('TallyAdictionalInfo', 'Model');

/**
 * TallyAdictionalInfo Test Case
 */
class TallyAdictionalInfoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tally_adictional_info'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TallyAdictionalInfo = ClassRegistry::init('TallyAdictionalInfo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TallyAdictionalInfo);

		parent::tearDown();
	}

}
