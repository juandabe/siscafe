<?php
App::uses('AdictionalElementsHasTallyPackaging', 'Model');

/**
 * AdictionalElementsHasTallyPackaging Test Case
 */
class AdictionalElementsHasTallyPackagingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adictional_elements_has_tally_packaging'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AdictionalElementsHasTallyPackaging = ClassRegistry::init('AdictionalElementsHasTallyPackaging');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdictionalElementsHasTallyPackaging);

		parent::tearDown();
	}

}
