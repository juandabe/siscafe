<?php
App::uses('AdictionalElementsHasProforma', 'Model');

/**
 * AdictionalElementsHasProforma Test Case
 */
class AdictionalElementsHasProformaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adictional_elements_has_proforma'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AdictionalElementsHasProforma = ClassRegistry::init('AdictionalElementsHasProforma');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdictionalElementsHasProforma);

		parent::tearDown();
	}

}
