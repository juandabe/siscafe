<?php
App::uses('PackagingCaffee', 'Model');

/**
 * PackagingCaffee Test Case
 */
class PackagingCaffeeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.packaging_caffee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PackagingCaffee = ClassRegistry::init('PackagingCaffee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PackagingCaffee);

		parent::tearDown();
	}

}
