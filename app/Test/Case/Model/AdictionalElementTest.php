<?php
App::uses('AdictionalElement', 'Model');

/**
 * AdictionalElement Test Case
 */
class AdictionalElementTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adictional_element'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AdictionalElement = ClassRegistry::init('AdictionalElement');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdictionalElement);

		parent::tearDown();
	}

}
