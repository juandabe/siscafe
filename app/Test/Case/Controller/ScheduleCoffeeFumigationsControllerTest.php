<?php
App::uses('ScheduleCoffeeFumigationsController', 'Controller');

/**
 * ScheduleCoffeeFumigationsController Test Case
 */
class ScheduleCoffeeFumigationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.schedule_coffee_fumigation',
		'app.fumigation_service',
		'app.hookup_status',
		'app.departament',
		'app.info_navy',
		'app.navy_agent',
		'app.shipping_line',
		'app.custom',
		'app.packaging_caffee',
		'app.ready_container',
		'app.checking_ctn',
		'app.user',
		'app.profiles',
		'app.bascule',
		'app.users',
		'app.remittances_caffee',
		'app.slot_store',
		'app.stores_caffee',
		'app.material_coffee',
		'app.bill',
		'app.state_operation',
		'app.units_caffee',
		'app.type_unit',
		'app.novelty_in_caffee',
		'app.packing_caffee',
		'app.shipper',
		'app.traceability_download',
		'app.client',
		'app.operation_tracking',
		'app.mark_caffee',
		'app.detail_packaging_caffee',
		'app.remittances_caffee_has_fumigation_service',
		'app.remittances_caffee_has_noveltys_caffee',
		'app.noveltys_coffee',
		'app.returns_caffee',
		'app.remittances_caffee_returns_caffee',
		'app.supplies_operation',
		'app.supply',
		'app.charge_massive_sample_coffee',
		'app.state_packaging',
		'app.services_order',
		'app.details_services_to_caffee',
		'app.items_service',
		'app.service_package_has_items_service',
		'app.service_package',
		'app.traceability_packaging',
		'app.detail_packakging_crossdocking'
	);

}
