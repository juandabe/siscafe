<?php
App::uses('TareRemesasController', 'Controller');

/**
 * TareRemesasController Test Case
 */
class TareRemesasControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tare_remesa'
	);

}
