<?php
App::uses('AppController', 'Controller');
/**
 * PackingCaffees Controller
 *
 * @property PackingCaffee $PackingCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 */
class PackingCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash');

}
