<?php

App::uses('AppController', 'Controller');

/**
 * NavyAgent Controller
 */
class NavyAgentsController extends AppController {

    public function findById($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $navyAgent = $this->NavyAgent->find('first', array('conditions' => array('NavyAgent.id' => $id)));
        return json_encode($navyAgent);
    }

}
