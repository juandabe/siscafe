<?php
App::uses('AppController', 'Controller');
/**
 * CitySources Controller
 *
 * @property CitySource $CitySource
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 */
class CitySourcesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash');

}
