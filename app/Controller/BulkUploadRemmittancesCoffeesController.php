<?php
App::uses('AppController', 'Controller');
require_once("../Vendor/aws/aws-autoloader.php");
App::import('Vendor', 'PHPExcel/IOFactory');
 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/**
 * BulkUploadRemmittancesCoffees Controller
 *
 * @property BulkUploadRemmittancesCoffee $BulkUploadRemmittancesCoffee
 * @property PaginatorComponent $Paginator
 */
class BulkUploadRemmittancesCoffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                if($this->Session->read('User.profiles_id')==1){
                    $this->layout = 'colaborador';
                }
		$this->BulkUploadRemmittancesCoffee->recursive = 0;
		$this->set('bulkUploadRemmittancesCoffees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('RemittancesCaffee');
            $this->loadModel('User');
            $this->loadModel('TypeSampleCoffe');
            if (!$this->BulkUploadRemmittancesCoffee->exists($id)) {
                    throw new NotFoundException(__('Invalid charge massive sample coffee'));
            }
            $options = array('conditions' => array('BulkUploadRemmittancesCoffee.' . $this->BulkUploadRemmittancesCoffee->primaryKey => $id));
            $this->set('bulkUploadRemmittancesCoffee', $this->BulkUploadRemmittancesCoffee->find('first', $options));
            $optionRemmittancesCoffee = array(
                        'limit' => 100,
                        'conditions' => array('RemittancesCaffee.ref_bulk_upload'=>$id),
                        'order' => array('RemittancesCaffee.created_date' => 'DESC')
                    );
            $this->set('remmittancesCoffees', $this->RemittancesCaffee->find('all', $optionRemmittancesCoffee));
	}
        
        
         /**
        * getTrackingRequest method
        *
        * @return void
        */
       public function getTrackingRequest($idRequest = null){
           if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;

           $bucket = 'remittancescoffeecvs';
           $keyname = $idRequest;

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-east-1',
               'credentials' => [
               'key'    => "AKIAJZHFB4GIIF3FDWMA",
               'secret' => "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",
           ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__('No se encontro el archivo!'));
           }

       }

/**
 * add method
 *
 * @return void
 */
	public function add() {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                if($this->Session->read('User.profiles_id')==1){
                    $this->layout = 'colaborador';
                }
                $this->loadModel('Client');
                $this->loadModel('RemittancesCaffee');
                $fileUploaded = null;
		if ($this->request->is('post')) {
			$this->BulkUploadRemmittancesCoffee->create();
                        $fileUploaded = $this->request->data['BulkUploadRemmittancesCoffee']['name_file'];
                        $this->request->data['BulkUploadRemmittancesCoffee']['name_file'] = $this->request->data['BulkUploadRemmittancesCoffee']['name_file']['name'];
                        $this->request->data['BulkUploadRemmittancesCoffee']['date_reg'] = date('Y-m-d H:i:s');
                        $this->request->data['BulkUploadRemmittancesCoffee']['user_reg'] = $this->Session->read('User.id');
			if ($this->BulkUploadRemmittancesCoffee->save($this->request->data)) {
                                $contadorImpo=0;
                                $objPHPExcel = PHPExcel_IOFactory::load($fileUploaded['tmp_name']);
				
                                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                                    $highestRow = $worksheet->getHighestRow();
                                    $highestColumn = $worksheet->getHighestColumn();
                                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                                    if($worksheet->getTitle() == 'Hoja1') {
                                        for ($row = 3; $row <= $highestRow; ++ $row) {
                                            $cellExpo = $worksheet->getCellByColumnAndRow(0, $row);
                                            $codExpoChar = $cellExpo->getValue();
                                            $dataClient = $this->Client->find('first',array('conditions' => array('Client.exporter_code like'=>"%".$codExpoChar."%")));
                                            $cellLotRef = $worksheet->getCellByColumnAndRow(1, $row);
                                            $cellQtaCoffee = $worksheet->getCellByColumnAndRow(2, $row);
                                            $cellQtaPallet = $worksheet->getCellByColumnAndRow(3, $row);
                                            $cellOPesoNominal = $worksheet->getCellByColumnAndRow(4, $row);
                                            $cellPlaca = $worksheet->getCellByColumnAndRow(5, $row);
                                            $cellFechaReg = $worksheet->getCellByColumnAndRow(6, $row);
                                            $cellUnidCafe = $worksheet->getCellByColumnAndRow(7, $row);
                                            $cellEmpCafe = $worksheet->getCellByColumnAndRow(8, $row);
                                            $cellTipoCafe = $worksheet->getCellByColumnAndRow(9, $row);
                                            $cellUser = $worksheet->getCellByColumnAndRow(10, $row);
                                            $cellTerminal = $worksheet->getCellByColumnAndRow(11, $row);
                                            $cellPosicion = $worksheet->getCellByColumnAndRow(12, $row);
                                            $arrayRemmittancesCoffee = array(
                                                'client_id'=>$dataClient['Client']['id'],
                                                'lot_caffee'=>$cellLotRef->getValue(),
                                                'quantity_bag_in_store'=>$cellQtaCoffee->getValue(),
                                                'quantity_in_pallet_caffee'=>$cellQtaPallet->getValue(),
                                                'quantity_radicated_bag_in'=>$cellQtaCoffee->getValue(),
                                                'total_weight_net_nominal'=>$cellOPesoNominal->getValue(),
                                                'vehicle_plate'=>$cellPlaca->getValue(),
                                                'created_date'=>$cellFechaReg->getValue(),
                                                'download_caffee_date'=>$cellFechaReg->getValue(),
                                                'is_active'=>1,
                                                'units_cafee_id'=>$cellUnidCafe->getValue(),
                                                'packing_cafee_id'=>$cellEmpCafe->getValue(),
                                                'type_units_id'=>$cellTipoCafe->getValue(),
                                                'user_register'=>$cellUser->getValue(),
                                                'jetty'=>$cellTerminal->getValue(),
                                                'state_operation_id'=>2,
                                                'cargolot_id'=>0,
                                                'observation'=>'REGISTRO POR CARGUE MASIVO',
                                                'slot_store_id'=>$cellPosicion->getValue(),
                                                'ref_bulk_upload'=>$this->BulkUploadRemmittancesCoffee->id
                                            );
                                            $this->RemittancesCaffee->create();
                                            if ($this->RemittancesCaffee->save($arrayRemmittancesCoffee)) {
                                                $contadorImpo++;
                                            }
                                        }
                                    }
                                }
                                
                                $bucket = 'remittancescoffeecvs';
                                $keyname = $fileUploaded['name'];
                                $temp_file_location = $fileUploaded['tmp_name'];
                                $s3 = new S3Client([
                                    'version' => 'latest',
                                    'region'  => 'us-east-1',
                                    'credentials' => [
                                    'key'    => "AKIAJZHFB4GIIF3FDWMA",
                                    'secret' => "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",]]);

                                $result = $s3->putObject([
                                        'Bucket' => $bucket,
                                        'Key'    => $keyname,
                                        'SourceFile' => $temp_file_location
                                ]);
                                
                                $this->BulkUploadRemmittancesCoffee->updateAll(
                                    array('count_insert'=>$contadorImpo),
                                    array('BulkUploadRemmittancesCoffee.id'=>$this->BulkUploadRemmittancesCoffee->id));
                                
                                $this->Flash->success(__('Cargue exitoso. Por favor validar'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Se genero un error en el proceso. Por favor valide'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->BulkUploadRemmittancesCoffee->exists($id)) {
			throw new NotFoundException(__('Invalid bulk upload remmittances coffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BulkUploadRemmittancesCoffee->save($this->request->data)) {
				$this->Flash->success(__('The bulk upload remmittances coffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The bulk upload remmittances coffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BulkUploadRemmittancesCoffee.' . $this->BulkUploadRemmittancesCoffee->primaryKey => $id));
			$this->request->data = $this->BulkUploadRemmittancesCoffee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->BulkUploadRemmittancesCoffee->id = $id;
		if (!$this->BulkUploadRemmittancesCoffee->exists()) {
			throw new NotFoundException(__('Invalid bulk upload remmittances coffee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->BulkUploadRemmittancesCoffee->delete()) {
			$this->Flash->success(__('The bulk upload remmittances coffee has been deleted.'));
		} else {
			$this->Flash->error(__('The bulk upload remmittances coffee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
