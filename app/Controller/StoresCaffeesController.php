<?php
App::uses('AppController', 'Controller');
/**
 * StoresCaffees Controller
 *
 * @property StoresCaffee $StoresCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class StoresCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->StoresCaffee->recursive = 0;
		$this->set('storesCaffees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->StoresCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid stores caffee'));
		}
		$options = array('conditions' => array('StoresCaffee.' . $this->StoresCaffee->primaryKey => $id));
		$this->set('storesCaffee', $this->StoresCaffee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->StoresCaffee->create();
			if ($this->StoresCaffee->save($this->request->data)) {
				$this->Flash->success(__('The stores caffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The stores caffee could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->StoresCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid stores caffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->StoresCaffee->save($this->request->data)) {
				$this->Flash->success(__('The stores caffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The stores caffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('StoresCaffee.' . $this->StoresCaffee->primaryKey => $id));
			$this->request->data = $this->StoresCaffee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->StoresCaffee->id = $id;
		if (!$this->StoresCaffee->exists()) {
			throw new NotFoundException(__('Invalid stores caffee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->StoresCaffee->delete()) {
			$this->Flash->success(__('The stores caffee has been deleted.'));
		} else {
			$this->Flash->error(__('The stores caffee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
