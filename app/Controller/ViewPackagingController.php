<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 *ViewPackaging Controller
 * * @property \App\Model\Table\ViewPackagingTable $ViewPackaging *
 * @method \App\Model\Entity\ViewPackaging[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = []) */
class ViewPackagingController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		$idModule = "2";
		$dataStaff;
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				if($dataStaff['type_staff_id'] == 6){
					return $this->redirect(['controller' => 'Staffs', 'action' => 'terminal']);
				}
				else{
					return $this->redirect(['controller' => 'Staffs', 'action' => 'home']);
				}
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}
		$columns = [
				[
					'title' => __('ID'),
					'field' => 'ViewPackaging.oie',
					'data' => 'oie'
				],
				[
					'title' => __('FECHA'),
					'field' => 'ViewPackaging.packaging_date',
					'data' => 'packaging_date'
				],
				[
					'title' => __('TERMINAL'),
					'field' => 'ViewPackaging.jetty',
					'data' => 'jetty'
				],
				[
					'title' => __('CTN'),
					'field' => 'ViewPackaging.bic_ctn',
					'data' => 'bic_ctn'
				],
				[
					'title' => __('LOTE'),
					'field' => 'ViewPackaging.lotes1',
					'data' => 'lotes1'
				],
				[
					'title' => __('LINEA'),
					'field' => 'ViewPackaging.business_name',
					'data' => 'business_name'
				],
				[
					'title' => __('BOOKING'),
					'field' => 'ViewPackaging.booking',
					'data' => 'booking'
				],
				[
					'title' => __('CTA_CAFE'),
					'field' => 'ViewPackaging.total_sacos',
					'data' => 'total_sacos'
				],
				[
					'title' => __('MOTONAVE'),
					'field' => 'ViewPackaging.motorship_name',
					'data' => 'motorship_name'
				],
				[
				  'searchable'=> false,
				  'title'=> "OPCIONES",
				  'orderable'=> false,
				  'defaultContent'=>"<button id=\"view1\" class=\"btn btn-primary fa fa-eye\"></button>
						<button id=\"view2\" class=\"btn btn-info fa fa-arrow-circle-o-down\"></button>",
				]
 			];
			
		$dateFrom = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " - 365 day"));
		if($dataStaff['type_staff']['id'] == 7){
			$data = $this->DataTables->find('ViewPackaging', 'all', 
			[
			'order' => ['ViewPackaging.packaging_date' => 'desc'],
			'conditions' => ['and'=> [['ViewPackaging.packaging_date >' => $dateFrom],
				['or'=>[['ViewPackaging.jetty'=>'BUN'],['ViewPackaging.jetty'=>'STM']]],
				['or'=>['ViewPackaging.expo_id1' => $dataStaff['cod_siscafe'],['ViewPackaging.expo_id2' => $dataStaff['cod_siscafe']]]]]]		],
			$columns);


			$this->set('columns', $columns);
			$this->set('data', $data);
			$this->set('_serialize', array_merge($this->viewVars['_serialize'], ['data']));
		}
		else if($dataStaff['type_staff']['id'] == 1){
			$data = $this->DataTables->find('ViewPackaging', 'all', 
			[
			'order' => ['ViewPackaging.packaging_date' => 'desc'],
			'conditions' => ['and'=> [['ViewPackaging.packaging_date >' => $dateFrom],
				['or'=>[['ViewPackaging.jetty'=>'BUN'],['ViewPackaging.jetty'=>'STM']]],
				['or'=>['ViewPackaging.expo_id1' => $dataStaff['ref2_user_id_siscafe'],['ViewPackaging.expo_id2' => $dataStaff['ref2_user_id_siscafe']]]]]]		],
			$columns);


			$this->set('columns', $columns);
			$this->set('data', $data);
			$this->set('_serialize', array_merge($this->viewVars['_serialize'], ['data']));
		}
    }
	
	/**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function indexTerminal()
    {
		
		$idModule = "42";
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}
		$columns = [
				[
					'title' => __('ID'),
					'field' => 'ViewPackaging.oie',
					'data' => 'oie'
				],
				[
					'title' => __('FECHA'),
					'field' => 'ViewPackaging.packaging_date',
					'data' => 'packaging_date'
				],
				[
					'title' => __('TERMINAL'),
					'field' => 'ViewPackaging.jetty',
					'data' => 'jetty'
				],
				[
					'title' => __('CTN'),
					'field' => 'ViewPackaging.bic_ctn',
					'data' => 'bic_ctn'
				],
				[
					'title' => __('LOTE'),
					'field' => 'ViewPackaging.lotes',
					'data' => 'lotes'
				],
				[
					'title' => __('LINEA'),
					'field' => 'ViewPackaging.business_name',
					'data' => 'business_name'
				],
				[
					'title' => __('CTA_CAFE'),
					'field' => 'ViewPackaging.total_sacos',
					'data' => 'total_sacos'
				],
				[
					'title' => __('MODALIDAD'),
					'field' => 'ViewPackaging.packaging_mode',
					'data' => 'packaging_mode'
				],
				[
					'title' => __('MOTONAVE'),
					'field' => 'ViewPackaging.motorship_name',
					'data' => 'motorship_name'
				],
				[
				  'searchable'=> false,
				  'title'=> "OPCIONES",
				  'orderable'=> false,
				  'defaultContent'=>"<button id=\"view1\" class=\"btn btn-primary fa fa-eye\"></button>",
				]
 			];
		
		$dateFrom = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " - 365 day"));
		
		$data = $this->DataTables->find('ViewPackaging', 'all', 
		[
			'order' => ['ViewPackaging.packaging_date' => 'desc'],
			'conditions' => ['AND'=>[['ViewPackaging.jetty like' => '%'.$this->getRequest()->getSession()->read('Staff.terminal').'%'],['ViewPackaging.packaging_date >' => $dateFrom]]]
		],
		$columns);
		$this->set('columns', $columns);
		$this->set('data', $data);
		$this->set('_serialize', array_merge($this->viewVars['_serialize'], ['data']));
    }


/**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function indexMuromar()
    {
		$idModule = "55";
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}
		$columns = [
				[
					'title' => __('ID'),
					'field' => 'ViewPackaging.oie',
					'data' => 'oie'
				],
				[
					'title' => __('FECHA'),
					'field' => 'ViewPackaging.packaging_date',
					'data' => 'packaging_date'
				],
				[
					'title' => __('TERMINAL'),
					'field' => 'ViewPackaging.jetty',
					'data' => 'jetty'
				],
				[
					'title' => __('CTN'),
					'field' => 'ViewPackaging.bic_ctn',
					'data' => 'bic_ctn'
				],
				[
					'title' => __('LOTE'),
					'field' => 'ViewPackaging.lotes',
					'data' => 'lotes'
				],
				[
					'title' => __('LINEA'),
					'field' => 'ViewPackaging.business_name',
					'data' => 'business_name'
				],
				[
					'title' => __('CTA_CAFE'),
					'field' => 'ViewPackaging.total_sacos',
					'data' => 'total_sacos'
				],
				[
					'title' => __('MODALIDAD'),
					'field' => 'ViewPackaging.packaging_mode',
					'data' => 'packaging_mode'
				],
				[
					'title' => __('MOTONAVE'),
					'field' => 'ViewPackaging.motorship_name',
					'data' => 'motorship_name'
				],
				[
				  'searchable'=> false,
				  'title'=> "OPCIONES",
				  'orderable'=> false,
				  'defaultContent'=>'',
				]
 			];
		
		$dateFrom = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " - 365 day"));
		
		$data = $this->DataTables->find('ViewPackaging', 'all', 
		[
			'order' => ['ViewPackaging.packaging_date' => 'desc'],
			'conditions' => ['AND'=>[['ViewPackaging.jetty like' => '%BUN%'],['ViewPackaging.packaging_date >' => $dateFrom]]]
		],
		$columns);
		$this->set('columns', $columns);
		$this->set('data', $data);
		$this->set('_serialize', array_merge($this->viewVars['_serialize'], ['data']));
	}
	
	/**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function indexMantas()
    {
        $dataStaff;
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']]);
        }
        $this->paginate = [
            'contain' => [ ]
        ];
        //$viewPackaging = $this->paginate($this->ViewPackaging);

        //$this->set(compact('viewPackaging'));
	}

	
	/**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function indexNavy()
    {
		$idModule = "49";
		$dataStaff;
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}

		$this->paginate = [
            'contain' => [ ]
        ];
		
    }


	
	/**
     * indexJsonNavy method
     *
     * @param string|null $idView Packaging id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	public function indexJsonNavy(){
		
		$idModule = "49";
		$dataStaff;
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}

        $this->autoRender = false;

        $requestData = $this->request->getParam('?');

        $options = [
            'contain' => [],
		];
		
		if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'booking like "%' . $search . '%"' .
				' OR packaging_date like "%' . $search . '%"' .
				' OR motorship_name like "%' . $search . '%"' .
				' OR lotes like "%' . $search . '%"' .
				' OR bic_ctn like "%' . $search . '%"'];
		}
		
		$orderList = [
            'oie' => 'ViewPackaging.oie',
            'packaging_date' => 'ViewPackaging.packaging_date',
            'iso_ctn' => 'ViewPackaging.iso_ctn',
            'bic_ctn' => 'ViewPackaging.bic_ctn',
            'lotes' => 'ViewPackaging.lotes',
            'business_name' => 'ViewPackaging.business_name',
			'booking' => 'ViewPackaging.booking',
            'tipo_papel' => 'ViewPackaging.tipo_papel',
            'seal_1' => 'ViewPackaging.seal_1',
			'total_sacos' => 'ViewPackaging.seal_1',
            'packaging_mode' => 'ViewPackaging.total_cafe_empaque',
            'motorship_name' => 'ViewPackaging.tare_estibas_descargue'
		];
		
		$orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
		];
		//filtro de contenedores con manta terminaca a partir de la solicitud 2020-10-01
		$dateFrom = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " - 365 day"));

		$whereOps = ['and'=>['navy_agent_id'=>$dataStaff['ref2_user_id_siscafe'],'jetty'=>'BUN']];

		$players = $this->ViewPackaging->find('all', $options)
		->order([$orderBy['column'] => $orderBy['dir']])
		->where($whereOps);

		//$total = $players->count();

		if ($requestData['length'] != -1) {
			$players = $players->limit(intval($requestData['length']));
		}
		else {
		 $players = $players->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
		->limit(90000)
		->order([$orderBy['column'] => $orderBy['dir']])->where($whereOps);
		}

        echo json_encode([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($players->count()),
            "recordsFiltered" => intval($players->count()),
            "data"            => $players->toList()
        ]);
	}

	

	/**
     * indexJsonMantas method
     *
     * @param string|null $idView Packaging id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	public function indexJsonMantas(){
		$dataStaff;
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']]);
        }
        $this->autoRender = false;

        $requestData = $this->request->getParam('?');

        $options = [
            'contain' => [],
		];
		
		if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'oie like "%' . $search . '%"' .
                ' OR packaging_date like "%' . $search . '%"' .
                ' OR iso_ctn like "%' . $search . '%"' .
                ' OR bic_ctn like "%' . $search . '%"' .
                ' OR jetty like "%' . $search . '%"'];
		}
		
		$orderList = [
            'oie' => 'ViewPackaging.oie',
            'packaging_date' => 'ViewPackaging.packaging_date',
            'iso_ctn' => 'ViewPackaging.iso_ctn',
            'bic_ctn' => 'ViewPackaging.bic_ctn',
            'lotes' => 'ViewPackaging.lotes',
            'business_name' => 'ViewPackaging.business_name',
		'booking' => 'ViewPackaging.booking',
            'tipo_papel' => 'ViewPackaging.tipo_papel',
            'seal_1' => 'ViewPackaging.seal_1',
	'total_sacos' => 'ViewPackaging.seal_1',
            'packaging_mode' => 'ViewPackaging.total_cafe_empaque',
            'motorship_name' => 'ViewPackaging.tare_estibas_descargue',
		'jetty' => 'ViewPackaging.jetty',
	'manta_termica' => 'ViewPackaging.manta_termica',
	'mantas_vdry_40' => 'ViewPackaging.mantas_vdry_40'
		];
		
		$orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
		];
		//filtro de contenedores con manta terminaca a partir de la solicitud 2020-10-01
        //$whereOps = ['and'=>['packaging_date >=' => '2020-10-02','jetty !='=>'STA','manta_termica !='=>0]];
		$whereOps = ['and'=>['packaging_date >=' => '2020-10-02','jetty !='=>'STA','or'=>['mantas_vdry_40 !='=>0,'manta_termica !='=>0]]];

		
		$players = $this->ViewPackaging->find('all', $options)
		->order([$orderBy['column'] => $orderBy['dir']])
		->where($whereOps);

		//$total = $players->count();

		if ($requestData['length'] != -1) {
			$players = $players->limit(intval($requestData['length']));
		}
		else {
		 $players = $players->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
		->limit(10000)
		->order([$orderBy['column'] => $orderBy['dir']])->where($whereOps);
		}

			
        echo json_encode([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($players->count()),
            "recordsFiltered" => intval($players->count()),
            "data"            => $players->toList()
        ]);
	}



    /**
     * View method
     *
     * @param string|null $idView Packaging id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
		$idModule = "2";
		$dataStaff;
		$this->Staffs = TableRegistry::get('Staffs');
		$this->OperationTracking = TableRegistry::get('OperationTracking');
		$this->TrackingHasPhotos = TableRegistry::get('TrackingHasPhotos');
		$this->InvoiceNote = TableRegistry::get('InvoiceNote');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}
		
		$dataOperationTracking = $this->OperationTracking->find('all')->where(['OperationTracking.ref_text' => $id])->toArray();
		
		$dataTrackingHasPhotos  = $this->TrackingHasPhotos->find('all')->order(['TrackingHasPhotos.created_date' => 'DESC'])
                        ->where(['TrackingHasPhotos.tracking_id' => $dataOperationTracking[0]['id']]);
		
        $viewPackaging = $this->ViewPackaging->get($id, [
            'contain' => []
		]);

	if($dataStaff['with_agreement'] == 2){//Si el cliente tiene convenio 1, sino, 2 marca para facturar la trazabilidad.
		
		$dataInvoiceNote = $this->InvoiceNote->find()
		->where(['id_tracking_siscafe'=>$id])->first();

		if(empty($dataInvoiceNote)){
			return $this->redirect(['controller' => 'view-packaging', 'action' => 'index']);
		}
		/* $dataInvoiceNote = $this->InvoiceNote->find()->where(['InvoiceNote.id_tracking_siscafe'=>$id])->toArray();
			
		if(empty($dataInvoiceNote)){
		  $invoiceNote = $this->InvoiceNote->newEntity();
		  $invoiceNote->billed = true;
		  $invoiceNote->id_expo_pilcaf = $idStaffs;
		  $invoiceNote->ip_conection = $_SERVER['REMOTE_ADDR'];
		  $invoiceNote->id_tracking_siscafe = $id;
		  $invoiceNote->email = null;
		  $invoiceNote->observation = null;
		  $invoiceNote->type_invoice = 1;//reporte trazabilidad embalaje
				
		  $this->InvoiceNote->save($invoiceNote);
	        } */
	 }

        $this->set('viewPackaging', $viewPackaging);
		
		$this->set('dataTrackingHasPhotos', $dataTrackingHasPhotos->toArray());
		$this->set('dataOperationTracking', $dataOperationTracking);
	}
	

	/**
     * updateFullNameInvoice method
     *
     * @param string|null $fullname String fullname.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	public function updateFullNameInvoice($data = null){
		$this->autoRender = false;
		$this->InvoiceNote = TableRegistry::get('InvoiceNote');
		$this->Staffs = TableRegistry::get('Staffs');
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
		$dataStaff;
		if ($idStaffs == null) {
            	  return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
       		}
		else{
		  $dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
		]);
		  }
		
		$parameters = explode("-",$data);

		$viewPackaging = $this->ViewPackaging->get($parameters[1], [
            'contain' => []
		]);

		$isSameExport = ($dataStaff['cod_siscafe'] == $viewPackaging['expo_id2']) ? true:false;


		if(!$isSameExport){
			return $this->redirect(['controller' => 'ViewPackaging', 'action' => 'index']);
		}

	    $invoiceNote = $this->InvoiceNote->newEntity();
		$invoiceNote->billed = true;
		$invoiceNote->id_expo_pilcaf = $idStaffs;
		$invoiceNote->ip_conection = $_SERVER['REMOTE_ADDR'];
		$invoiceNote->id_tracking_siscafe = $parameters[1];
		$invoiceNote->email = null;
		$invoiceNote->observation = $parameters[0];
		$invoiceNote->type_invoice = $parameters[2];//reporte trazabilidad embalaje

		if($this->InvoiceNote->save($invoiceNote)){
			echo 1;
		}
		else{
			echo 0;
		}
	}
	
	/**
     * viewTerminal method
     *
     * @param string|null $idView Packaging id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewTerminal($id = null)
    {
		$idModule = "42";
		$this->Staffs = TableRegistry::get('Staffs');
		$this->OperationTracking = TableRegistry::get('OperationTracking');
		$this->TrackingHasPhotos = TableRegistry::get('TrackingHasPhotos');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}
		
		$dataOperationTracking = $this->OperationTracking->find('all')->where(['OperationTracking.ref_text' => $id])->toArray();
		
		$dataTrackingHasPhotos  = $this->TrackingHasPhotos->find('all')->order(['TrackingHasPhotos.created_date' => 'DESC'])
                        ->where(['TrackingHasPhotos.tracking_id' => $dataOperationTracking[0]['id']]);
		
        $viewPackaging = $this->ViewPackaging->get($id, [
            'contain' => []
        ]);

        $this->set('viewPackaging', $viewPackaging);
		
		$this->set('dataTrackingHasPhotos', $dataTrackingHasPhotos->toArray());
		$this->set('dataOperationTracking', $dataOperationTracking);
    }

	

	/**
     * findInvoiceNoteTracking method
     *
     * @return \Cake\Http\Response|void
     */
	 public function findInvoiceNoteTracking($id_tracking = null){
		$this->autoRender = false;
		$this->InvoiceNote = TableRegistry::get('InvoiceNote');
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
		$dataStaff;
		if ($idStaffs == null) {
            	  return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
       		}
		else{
		  $dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
		}
		$dataInvoiceNote = $this->InvoiceNote->find()->where(['InvoiceNote.id_tracking_siscafe'=>$id_tracking])->toArray();
				
		if(empty($dataInvoiceNote) && $dataStaff['with_agreement'] == 2){
			echo 0;
		}
		else{
			echo 1;
		}
	}

	/**
     * findInvoiceNoteTracking method
     *
     * @return \Cake\Http\Response|void
     */
	public function findInvoiceNoteTracking2($id_tracking = null){
		$this->autoRender = false;
		$this->InvoiceNote = TableRegistry::get('InvoiceNote');
		$this->Staffs = TableRegistry::get('Staffs');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
		$dataStaff;
		if ($idStaffs == null) {
            	  return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
       		}
		else{
		  $dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
		}
		$dataInvoiceNote = $this->InvoiceNote->find()->where(['InvoiceNote.id_tracking_siscafe'=>$id_tracking,'InvoiceNote.type_invoice'=>2])->first();

		if(empty($dataInvoiceNote)){
			
		/* if(empty($dataInvoiceNote)){

			$invoiceNote = $this->InvoiceNote->newEntity();
			$invoiceNote->billed = true;
			$invoiceNote->id_expo_pilcaf = $idStaffs;
			$invoiceNote->ip_conection = $_SERVER['REMOTE_ADDR'];
			$invoiceNote->id_tracking_siscafe = $id_tracking;
			$invoiceNote->email = null;
			$invoiceNote->observation = null;
			$invoiceNote->type_invoice = 2;//reporte EIR spb
				  
			$this->InvoiceNote->save($invoiceNote); */

			echo 0;
		}
		else{
			echo 1;
		}
	}
		
	
	/**
     * viewNavy method
     *
     * @param string|null $idView Packaging id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewNavy($id = null)
    {
		$idModule = "49";
		$this->Staffs = TableRegistry::get('Staffs');
		$this->OperationTracking = TableRegistry::get('OperationTracking');
		$this->TrackingHasPhotos = TableRegistry::get('TrackingHasPhotos');
		$idStaffs = $this->getRequest()->getSession()->read('Staff.id');
        if ($idStaffs == null) {
            return $this->redirect(['controller' => 'staffs', 'action' => 'login']);
        }
		else{
			$dataStaff = $this->Staffs->get($this->getRequest()->getSession()->read('Staff.id'),[
            'contain' => ['TypeStaffs']
        ]);
			$arrayAuth = explode(',',$dataStaff['type_staff']['key_array']);
			if (!in_array($idModule,$arrayAuth)) {
				return $this->redirect(['controller' => 'staffs', 'action' => 'home']);
			}
		}
		
		$dataOperationTracking = $this->OperationTracking->find('all')->where(['OperationTracking.ref_text' => $id])->toArray();
		
		$dataTrackingHasPhotos  = $this->TrackingHasPhotos->find('all')->order(['TrackingHasPhotos.created_date' => 'DESC'])
                        ->where(['TrackingHasPhotos.tracking_id' => $dataOperationTracking[0]['id']]);
		
        $viewPackaging = $this->ViewPackaging->get($id, [
            'contain' => []
        ]);

        $this->set('viewPackaging', $viewPackaging);
		
		$this->set('dataTrackingHasPhotos', $dataTrackingHasPhotos->toArray());
		$this->set('dataOperationTracking', $dataOperationTracking);
    }

	/**
     * findCoffeeLot method
     *
     * @return \Cake\Http\Response|void
     */
	public function findCoffeeLot($lotCoffee = null){
		$this->autoRender = false;
		$yearCurrent = "2020";
		if($lotCoffee){
			$parameters = explode("-",$lotCoffee);
			//print_r($parameters);exit;
			if($parameters[0] != "" && $parameters[1] !="" && strlen($parameters[1]) ==5 &&$parameters[0] !=""){
				if($parameters[2] == "undefined"){$parameters[2]="";}
				$dataViewPackaging = $this->ViewPackaging->find('all',
					[
					'fields' =>['ViewPackaging.lotes','ViewPackaging.packaging_date','ViewPackaging.jetty'],
					'conditions' => ['and'=>[
							['year(ViewPackaging.packaging_date)'=>$yearCurrent],
							['ViewPackaging.jetty LIKE'=>'%'.$parameters[2].'%'],
							['ViewPackaging.lotes LIKE'=>'%'.$parameters[1].'%'],
							['or'=>[['ViewPackaging.expo_id2'=>$parameters[0]],['ViewPackaging.expo_id1'=>$parameters[0]]]]
						]
					]]
				);
				//print_r($dataViewPackaging);exit;
				if($dataViewPackaging->toArray()){
					echo json_encode($dataViewPackaging->toArray());
				}
				else{
					return null;
				}
			}
		}
		else{
			return null;
		}
	}
}
