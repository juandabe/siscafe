<?php
App::uses('AppController', 'Controller');
App::uses('EmbalajesController', 'Controller');
App::uses('ExportersController', 'Controller');
App::uses('TareRemesasController', 'Controller');
App::uses('AdictionalElementsController', 'Controller');
App::uses('TypeCtnsController', 'Controller');
App::uses('TallyAdictionalInfosController', 'Controller');
App::uses('TallyExposController', 'Controller');


/**
 * TallyPackagings Controller
 *
 * @property TallyPackaging $TallyPackaging
 * @property PaginatorComponent $Paginator
 */
class TallyPackagingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Flash', 'Session','RequestHandler');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
		$this->layout = 'colaborador';
		$this->TallyPackaging->recursive = 0;
		$this->set('tallyPackagings', $this->Paginator->paginate());
	}
	

	
/**
 * indexbybicctn method
 *
 * @return void
 */
	public function indexbybicctn($bicContainer) {
		$this->layout = 'shippingline';
		$this->Paginator->settings = array(
		  'conditions' => 
		    array('TallyPackaging.bic_container' => $bicContainer),
			     'limit' => 10);
		$data = $this->Paginator->paginate('TallyPackaging');
		$this->set('bicContainer', $bicContainer);
		$this->set('tallyPackagings', $data);
	}
	
/**
 * indexbyshippingline method
 *
 * @return void
 */
	public function indexbyshippingline($shippingline) {
		$this->layout = 'shippingline';
		
		$this->set('bicContainer', $bicContainer);
		$this->set('tallyPackagings', $data);
	}
	
/**
 * indexbybicctnadictionalelement method
 *
 * @return void
 */
	public function indexbybicctnadictionalelement($bicContainer) {
		$this->layout = 'colaborador';
		$this->Paginator->settings = array(
		  'conditions' => 
		    array('TallyPackaging.bic_container' => $bicContainer),
			     'limit' => 10);
		$data = $this->Paginator->paginate('TallyPackaging');
		$this->set('bicContainer', $bicContainer);
		$this->set('tallyPackagings', $data);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = 'colaborador';
		
		if (!$this->TallyPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
		$tallyPackaging = $this->TallyPackaging->find('first', $options);
		$EmbalajesController = new EmbalajesController;
		$ExportersController = new ExportersController;
		$TareRemesasController = new TareRemesasController;
		$bicContainer = $tallyPackaging["TallyPackaging"]["bic_container"];
		$dateTally = substr($tallyPackaging["TallyPackaging"]["created_date"], 0, 10);
		$Embalajes = $EmbalajesController->findWeightByContainer($bicContainer, $dateTally);
		$Exporter=$ExportersController->exporterBycode($Embalajes);
		$TaraEstiba = $TareRemesasController->tarePallet($Embalajes);
		$totalWeight = $EmbalajesController->totalWeightContainer($Embalajes);
		$dateExportation=$EmbalajesController->findDateByContainer($bicContainer);
		$AdictionalElementsHasTallyPackaging = $tallyPackaging["AdictionalElementsHasTallyPackaging"];
		$AdictionalElementsController = new AdictionalElementsController;
		$AdictionalElements = $AdictionalElementsController->findAll();
		$this->pdfConfig = array(
			'download' => true,
			'filename' => 'TallyPackaging_' . $id .'.pdf'
		);
		$this->set('totalWeight', $totalWeight);
		$this->set('dateexportation', $dateExportation);
		$this->set('taraestiba', $TaraEstiba);
		$this->set('exporter', $Exporter);
		$this->set('embalajes', $Embalajes);
		$this->set('tallyPackaging', $tallyPackaging);
		$this->set('adictionalElementsHasTallyPackagings',$AdictionalElementsHasTallyPackaging);
		$this->set('adictionalElements',$AdictionalElements);
	}

/**
 * viewbybicctn method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function viewbybicctn($id = null) {
		$this->layout = 'shippingline';
		if (!$this->TallyPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
		$tallyPackaging = $this->TallyPackaging->find('first', $options);
		$EmbalajesController = new EmbalajesController;
		$ExportersController = new ExportersController;
		$TareRemesasController = new TareRemesasController;
		$bicContainer = $tallyPackaging["TallyPackaging"]["bic_container"];
		$dateTally = substr($tallyPackaging["TallyPackaging"]["created_date"], 0, 10);
		$Embalajes = $EmbalajesController->findWeightByContainer($bicContainer, $dateTally);
		$Exporter=$ExportersController->exporterBycode($Embalajes);
		$TaraEstiba = $TareRemesasController->tarePallet($Embalajes);
		$totalWeight = $EmbalajesController->totalWeightContainer($Embalajes);
		$dateExportation=$EmbalajesController->findDateByContainer($bicContainer);
		$AdictionalElementsHasTallyPackaging = $tallyPackaging["AdictionalElementsHasTallyPackaging"];
		$AdictionalElementsController = new AdictionalElementsController;
		$AdictionalElements = $AdictionalElementsController->findAll();
		$this->pdfConfig = array(
			'download' => true,
			'filename' => 'TallyPackaging_' . $id .'.pdf'
		);
		$this->set('totalWeight', $totalWeight);
		$this->set('dateexportation', $dateExportation);
		$this->set('taraestiba', $TaraEstiba);
		$this->set('exporter', $Exporter);
		$this->set('embalajes', $Embalajes);
		$this->set('tallyPackaging', $tallyPackaging);
		$this->set('adictionalElementsHasTallyPackagings',$AdictionalElementsHasTallyPackaging);
		$this->set('adictionalElements',$AdictionalElements);
	}
	
	/**
 * viewbybicctnadictionalelement method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function viewbybicctnadictionalelement($id = null) {
		$this->layout = 'shippingline';
		
		if (!$this->TallyPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
		$tallyPackaging = $this->TallyPackaging->find('first', $options);
		$EmbalajesController = new EmbalajesController;
		$ExportersController = new ExportersController;
		$TareRemesasController = new TareRemesasController;
		$TallyAdictionalInfosController = new TallyAdictionalInfosController;
		$TypeCtnsController = new TypeCtnsController;
		$bicContainer = $tallyPackaging["TallyPackaging"]["bic_container"];
		$dateTally = substr($tallyPackaging["TallyPackaging"]["created_date"], 0, 10);
		$Embalajes = $EmbalajesController->findWeightByContainer($bicContainer, $dateTally);//Evita repetir contenedores, por el distintivo de la fecha del tally
		$Exporter=$ExportersController->exporterBycode($Embalajes);
		$TaraEstiba = $TareRemesasController->tarePallet($Embalajes);
		$totalWeight = $EmbalajesController->totalWeightContainer($Embalajes);
		$weightNet = $totalWeight-($TaraEstiba["tareEstiba"]);
		$dateExportation=$EmbalajesController->findDateByContainer($bicContainer);
		$AdictionalElementsHasTallyPackaging = $tallyPackaging["AdictionalElementsHasTallyPackaging"];
		$AdictionalElementsController = new AdictionalElementsController;
		$AdictionalElements = $AdictionalElementsController->findAll();
		$proforma = $Embalajes[0]['Embalaje']['proforma'];
		$TallyAdictionalInfos = $TallyAdictionalInfosController->findByProforma($proforma);
		$booking = $TallyAdictionalInfos['TallyAdictionalInfo']['booking'];
		$destino = $TallyAdictionalInfos['TallyAdictionalInfo']['destino'];
		$viaje = $TallyAdictionalInfos['TallyAdictionalInfo']['viaje'];
		$idIsoCtn = $tallyPackaging['TallyPackaging']['type_ctn_id'];
		$TypeCtn = $TypeCtnsController->findById($idIsoCtn);
		$totalCtnLoad = $weightNet+$TypeCtn['TypeCtn']['weight_ctn'];
		$cod_exportador = $Embalajes[0]['Embalaje']['id_exportador'];
		$this->pdfConfig = array(
			'download' => true,
			'filename' => $bicContainer . '.pdf'
		);
		$this->set('totalWeight', $totalWeight);
		$this->set('dateexportation', $dateExportation);
		$this->set('taraestiba', $TaraEstiba);
		$this->set('exporter', $Exporter);
		$this->set('Booking', $booking);
		$this->set('Viaje', $viaje);
		$this->set('TareCtn', $TypeCtn['TypeCtn']['weight_ctn']);
		$this->set('ISOCtn', $TypeCtn['TypeCtn']['iso_ctn']);
		$this->set('CtnNet', $weightNet);
		$this->set('CtnAndLoad', $totalCtnLoad);
		$this->set('Destino', $destino);
		$this->set('cod_exporter', $cod_exportador);
		$this->set('embalajes', $Embalajes);
		$this->set('tallyPackaging', $tallyPackaging);
		$this->set('adictionalElementsHasTallyPackagings',$AdictionalElementsHasTallyPackaging);
		$this->set('adictionalElements',$AdictionalElements);
	}
	
	
/* viewbybicctnadictionalelement method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function viewtally($id = null) {
		$this->layout = 'shippingline';
		
		if (!$this->TallyPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
		$tallyPackaging = $this->TallyPackaging->find('first', $options);
		$EmbalajesController = new EmbalajesController;
		$ExportersController = new ExportersController;
		$TareRemesasController = new TareRemesasController;
		$TallyAdictionalInfosController = new TallyAdictionalInfosController;
		$TypeCtnsController = new TypeCtnsController;
		$bicContainer = $tallyPackaging["TallyPackaging"]["bic_container"];
		$dateTally = substr($tallyPackaging["TallyPackaging"]["created_date"], 0, 10);
		$Embalajes = $EmbalajesController->findWeightByContainer($bicContainer, $dateTally);//Evita repetir contenedores, por el distintivo de la fecha del tally
		$Exporter=$ExportersController->exporterBycode($Embalajes);
		$TaraEstiba = $TareRemesasController->tarePallet($Embalajes);
		$totalWeight = $EmbalajesController->totalWeightContainer($Embalajes);
		$weightNet = $totalWeight-($TaraEstiba["tareEstiba"]);
		$dateExportation=$EmbalajesController->findDateByContainer($bicContainer);
		$AdictionalElementsHasTallyPackaging = $tallyPackaging["AdictionalElementsHasTallyPackaging"];
		$AdictionalElementsController = new AdictionalElementsController;
		$AdictionalElements = $AdictionalElementsController->findAll();
		$proforma = $Embalajes[0]['Embalaje']['proforma'];
		$TallyAdictionalInfos = $TallyAdictionalInfosController->findByProforma($proforma);
		$booking = $TallyAdictionalInfos['TallyAdictionalInfo']['booking'];
		$destino = $TallyAdictionalInfos['TallyAdictionalInfo']['destino'];
		$viaje = $TallyAdictionalInfos['TallyAdictionalInfo']['viaje'];
		$idIsoCtn = $tallyPackaging['TallyPackaging']['type_ctn_id'];
		$TypeCtn = $TypeCtnsController->findById($idIsoCtn);
		$totalCtnLoad = $weightNet+$TypeCtn['TypeCtn']['weight_ctn'];
		$cod_exportador = $Embalajes[0]['Embalaje']['id_exportador'];
		$this->pdfConfig = array(
			'download' => true,
			'filename' => $bicContainer . '.pdf'
		);
		$this->set('totalWeight', $totalWeight);
		$this->set('dateexportation', $dateExportation);
		$this->set('taraestiba', $TaraEstiba);
		$this->set('exporter', $Exporter);
		$this->set('Booking', $booking);
		$this->set('Viaje', $viaje);
		$this->set('TareCtn', $TypeCtn['TypeCtn']['weight_ctn']);
		$this->set('ISOCtn', $TypeCtn['TypeCtn']['iso_ctn']);
		$this->set('CtnNet', $weightNet);
		$this->set('CtnAndLoad', $totalCtnLoad);
		$this->set('Destino', $destino);
		$this->set('cod_exporter', $cod_exportador);
		$this->set('embalajes', $Embalajes);
		$this->set('tallyPackaging', $tallyPackaging);
		$this->set('adictionalElementsHasTallyPackagings',$AdictionalElementsHasTallyPackaging);
		$this->set('adictionalElements',$AdictionalElements);
	}
	
	/**
 * viewbybicctnadictionalelement method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function viewbylotcaffeetnadictionalelement($id = null) {
		$this->layout = 'shippingline';
		
		if (!$this->TallyPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
		$tallyPackaging = $this->TallyPackaging->find('first', $options);
		$EmbalajesController = new EmbalajesController;
		$ExportersController = new ExportersController;
		$TareRemesasController = new TareRemesasController;
		$bicContainer = $tallyPackaging["TallyPackaging"]["bic_container"];
		$dateTally = substr($tallyPackaging["TallyPackaging"]["created_date"], 0, 10);
		$Embalajes = $EmbalajesController->findWeightByContainer($bicContainer, $dateTally);//Evita repetir contenedores, por el distintivo de la fecha del tally
		$Exporter=$ExportersController->exporterBycode($Embalajes);
		$TaraEstiba = $TareRemesasController->tarePallet($Embalajes);
		$totalWeight = $EmbalajesController->totalWeightContainer($Embalajes);
		$dateExportation=$EmbalajesController->findDateByContainer($bicContainer);
		$AdictionalElementsHasTallyPackaging = $tallyPackaging["AdictionalElementsHasTallyPackaging"];
		$AdictionalElementsController = new AdictionalElementsController;
		$AdictionalElements = $AdictionalElementsController->findAll();
		$this->pdfConfig = array(
			'download' => true,
			'filename' => $bicContainer . '.pdf'
		);
		$this->set('totalWeight', $totalWeight);
		$this->set('dateexportation', $dateExportation);
		$this->set('taraestiba', $TaraEstiba);
		$this->set('exporter', $Exporter);
		$this->set('embalajes', $Embalajes);
		$this->set('tallyPackaging', $tallyPackaging);
		$this->set('adictionalElementsHasTallyPackagings',$AdictionalElementsHasTallyPackaging);
		$this->set('adictionalElements',$AdictionalElements);
	}
	
/**
 * findbybicctn method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function findbybicctn() {
		$this->layout = 'shippingline';
		if ($this->request->is('post')) {
		  $bicContainer = $this->request['data']['TallyPackaging']['bic_container'];
		  $options = array('conditions' => array('TallyPackaging.bic_container' => $bicContainer));
		  $allTallys = $this->TallyPackaging->find('all',$options);
		  if($allTallys == null) {
		    $this->Flash->error(__('Contenedor no encontrado!'));
		  }
		  else{
		    return $this->redirect(array('action' => 'indexbybicctn',$bicContainer));
		  }
		}
		
	}
	
/**
 * findshippingline method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function findTallyByBICAndDate() {
		$this->layout = 'shippingline';
		$rpscontenedor         = isset($this->request->query['rpscontenedor'])         ? $this->request->query['rpscontenedor']         : null;
		$date      = isset($this->request->query['date'])       ? $this->request->query['date']       : null;
		$date = substr($date, 0, 10);
		$tallyPackaging = $this->TallyPackaging->find('first', array(
		'conditions' => array(
		  'AND' => array(
		    'TallyPackaging.bic_container = ' => $rpscontenedor,
		    'TallyPackaging.created_date LIKE ' => $date.'%')
		    )
		));
		return $this->redirect(array('action' => 'viewbylotcaffeetnadictionalelement',$tallyPackaging['TallyPackaging']['id']));
	}
	
/**
 * findshippingline method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function changemotoship() {
	    $this->layout = 'colaborador';
	    if($this->request->is('post')) {
			$contadorImpo=0;
			$file = $this->request->data['CaffeDownloadCtg']['file'];
			$pathFile = WWW_ROOT.'data'.DS.$file['name'];
			move_uploaded_file($file['tmp_name'], $pathFile);
		        $objPHPExcel = PHPExcel_IOFactory::load($pathFile);
		        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		          $highestRow = $worksheet->getHighestRow(); // e.g. 10
			  $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			  $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
			    for ($row = 2; $row <= $highestRow; ++ $row) {
			      $this->CaffeDownloadCtg->create();
			      $cell = $worksheet->getCellByColumnAndRow(0, $row);
			      $timestamp = PHPExcel_Shared_Date::ExcelToPHP($cell->getValue());
			      $date_packaging = gmdate("Y-m-d\TH:i:s\Z", $timestamp);
			      
			      
			    }
			    $this->Flash->success(__('Se importaron exitosamente '.$contadorImpo.' registros!'));
			    return $this->redirect(array('action' => 'index'));
			}
		}

		$rpscontenedor         = isset($this->request->query['rpscontenedor'])         ? $this->request->query['rpscontenedor']         : null;
		$date      = isset($this->request->query['date'])       ? $this->request->query['date']       : null;
		$date = substr($date, 0, 10);
		$tallyPackaging = $this->TallyPackaging->find('first', array(
		'conditions' => array(
		  'AND' => array(
		    'TallyPackaging.bic_container = ' => $rpscontenedor,
		    'TallyPackaging.created_date LIKE ' => $date.'%')
		    )
		));
		return $this->redirect(array('action' => 'viewbybicctnadictionalelement',$tallyPackaging['TallyPackaging']['id']));
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function findbybicctnadictionalelement() {
		$this->layout = 'colaborador';
		if ($this->request->is('post')) {
		  $bicContainer = $this->request['data']['TallyPackaging']['bic_container'];
		  $options = array('conditions' => array('TallyPackaging.bic_container' => $bicContainer));
		  $allTallys = $this->TallyPackaging->find('all',$options);
		  if($allTallys == null) {
		    $this->Flash->error(__('Contenedor no encontrado!'));
		  }
		  else{
		    return $this->redirect(array('action' => 'indexbybicctnadictionalelement',$bicContainer));
		  }
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->TallyPackaging->create();
			$file_line_path = $this->request->data['TallyPackaging']['seals_line_img'];
			$file_police_path = $this->request->data['TallyPackaging']['seals_policy_img'];
			$code_seal_line = $this->request->data['TallyPackaging']['seals_line_code'];
			$idIsoCtn = $this->request->data['TallyPackaging']['type_ctn_id'];
			$this->request->data['TallyPackaging']['seals_line_img']="/huu/";
			$this->request->data['TallyPackaging']['seals_policy_img']="/hhu/";
			$this->request->data['TallyPackaging']['created_date']= date("Y/m/d h:i:sa");
			$this->request->data['TallyPackaging']['users_id'] = $this->Session->read('User.id');
			$this->saveDataPackaging($code_seal_line,$idIsoCtn);//información de excel
			if ($this->TallyPackaging->save($this->request->data)) {
			  $tallyContainer = $this->TallyPackaging->find('first', array('conditions' => array('TallyPackaging.seals_line_code' => $code_seal_line)));
			  $pathDirectory='data/tarja'.DS.$tallyContainer['TallyPackaging']['id'].DS.$tallyContainer['TallyPackaging']['bic_container'];
			  
			  if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
			      mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
			  }
			  $pathFullLine=$pathDirectory.DS.$file_line_path['name'];
			  $resultLine = move_uploaded_file($file_line_path['tmp_name'], WWW_ROOT.'img/'.$pathFullLine);
			  $tallyContainer['TallyPackaging']['seals_line_img']=$pathFullLine;
			  $pathFullPolicy=$pathDirectory.DS.$file_police_path['name'];
			  $tallyContainer['TallyPackaging']['seals_policy_img']=$pathFullPolicy;
			  $resultPolicy = move_uploaded_file($file_police_path['tmp_name'], WWW_ROOT.'img/'.$pathFullPolicy);
			  $this->request->data['TallyPackaging']['seals_line_img']=$pathFullLine;
			  $this->request->data['TallyPackaging']['seals_policy_img']=$pathFullPolicy;
			  $this->TallyPackaging->save($this->request->data);
			  $this->Flash->success(__('Tarja cargada correctamente.'));
			  return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tally packaging could not be saved. Please, try again.'));
			}
		}
		$typeCtnsController = new TypeCtnsController;
		$typeCtns = $typeCtnsController->findAll();
		$this->set('typectn', $typeCtns);
	}

	
	function saveDataPackaging($code_seal_line,$idIsoCtn){
		$TallyAdictionalInfosController = new TallyAdictionalInfosController;
		$EmbalajesController = new EmbalajesController;
		$ExportersController = new ExportersController;
		$TareRemesasController = new TareRemesasController;
		$TypeCtnsController = new TypeCtnsController;
		$TallyExposController = new TallyExposController;
		$TallyExposController->TallyExpo->create();
		$tallyPackaging = $this->TallyPackaging->find('first', array('conditions' => array('TallyPackaging.seals_line_code' => $code_seal_line)));
		$bicContainer = $tallyPackaging["TallyPackaging"]["bic_container"];
		$dateTally = substr($tallyPackaging["TallyPackaging"]["created_date"], 0, 10);
		$Embalajes = $EmbalajesController->findWeightByContainer($bicContainer, $dateTally);
		$totalWeight = $EmbalajesController->totalWeightContainer($Embalajes);
		$TaraEstiba = $TareRemesasController->tarePallet($Embalajes);
		$weightNet = $totalWeight-($TaraEstiba["tareEstiba"]);//Grano café + paquete
		$proforma = $Embalajes[0]['Embalaje']['proforma'];
		$Exporter=$ExportersController->exporterBycode($Embalajes);
		$TallyAdictionalInfos = $TallyAdictionalInfosController->findByProforma($proforma);
		$TypeCtn = $TypeCtnsController->findById($idIsoCtn);
		$totalSacos=0;
		foreach ($Embalajes as $embalaje){
		    $totalSacos+=$embalaje["Embalaje"]["rpssacos"];
		}
		$loteCafe=0;
		$numLotes="";
		foreach ($Embalajes as $embalaje){
		    $loteCafetmp=$embalaje["Embalaje"]["lote"];
		    if($loteCafe != $loteCafetmp){
		      $loteCafe=$loteCafetmp;
		      $numLotes .= $loteCafe . " | ";
		    }
		}
		$data = array(
		    'id' => null, 
		    'lotes' => $numLotes, 
		    'booking' => $TallyAdictionalInfos['TallyAdictionalInfo']['booking'],
		    'destino' => $TallyAdictionalInfos['TallyAdictionalInfo']['destino'],
		    'viaje' => $TallyAdictionalInfos['TallyAdictionalInfo']['viaje'],
		    'linea' => $Embalajes[0]['Embalaje']['linea'],
		    'agt_maritimo' => $Embalajes[0]['Embalaje']['agente_maritimo'],
		    'exportador' => $Exporter['Exporter']['name'],
		    'cod_exportador' => $Embalajes[0]['Embalaje']['id_exportador'],
		    'total_sacos' => $totalSacos, 
		    'modalidad_embalaje' => $Embalajes[0]['Embalaje']['modalidad_embalaje'],
		    'tipo_embalaje' => $Embalajes[0]['Embalaje']['tipo_embalaje'],
		    'bic_ctn' => strtoupper($tallyPackaging["TallyPackaging"]["bic_container"]), 
		    'iso_ctn' => $TypeCtn['TypeCtn']['iso_ctn'],
		    'seal1' => $tallyPackaging["TallyPackaging"]["seals_line_code"],
		    'seal2' => $tallyPackaging["TallyPackaging"]["seals_policy_code"], 
		    'weight_ctn' => $TypeCtn['TypeCtn']['weight_ctn'],
		    'weight_load_total' => ($weightNet+$TypeCtn['TypeCtn']['weight_ctn']),
		    'weight_load_net' => $weightNet,
		    'packaging_date' => $tallyPackaging["TallyPackaging"]["created_date"],
		    'adictional_elements' => 'Elementos adicionales',
		    'seq_tally' => $tallyPackaging["TallyPackaging"]["id"]);
		$TallyExposController->TallyExpo->save($data);
		exit;
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = 'colaborador';
		if (!$this->TallyPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TallyPackaging->save($this->request->data)) {
				$this->Flash->success(__('The tally packaging has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tally packaging could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
			$this->request->data = $this->TallyPackaging->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TallyPackaging->id = $id;
		if (!$this->TallyPackaging->exists()) {
			throw new NotFoundException(__('Invalid tally packaging'));
		}
		$options = array('conditions' => array('TallyPackaging.' . $this->TallyPackaging->primaryKey => $id));
		$tallyPackaging = $this->request->data = $this->TallyPackaging->find('first', $options);
		$pathDirectory=WWW_ROOT.'img/data/tarja'.DS.$tallyPackaging['TallyPackaging']['id'].DS.$tallyPackaging['TallyPackaging']['bic_container'];
		if (!is_dir($pathDirectory)) {
		  rmdir($pathDirectory);
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TallyPackaging->delete()) {
			$this->Flash->success(__('Tarja borrada correctamente.'));
		} else {
			$this->Flash->error(__('The tally packaging could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
