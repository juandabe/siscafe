<?php
App::uses('AppController', 'Controller');
/**
 * CheckingCtns Controller
 *
 * @property CheckingCtn $CheckingCtn
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class CheckingCtnsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CheckingCtn->recursive = 0;
		$this->set('checkingCtns', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
                $oie = isset($this->request->query['oie']) ? $this->request->query['oie']: null;
                $checking_ctn_id = isset($this->request->query['checking']) ? $this->request->query['checking']: null;
                $this->loadModel('PackagingCaffee');
                $findPackagingCaffee = $this->PackagingCaffee->find('first',array('conditions' => array('PackagingCaffee.id'=>$oie)));
		if (!$this->CheckingCtn->exists($checking_ctn_id)) {
			throw new NotFoundException(__('Invalid checking ctn'));
		}
		$options = array('conditions' => array('CheckingCtn.' . $this->CheckingCtn->primaryKey => $checking_ctn_id));
		$this->set('checkingCtn', $this->CheckingCtn->find('first', $options));
                $this->set('findPackagingCaffee',$findPackagingCaffee);
	}

/**
 * add method
 *
 * @return void
 */
	public function check_ctn($idOIE=null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->loadModel('ServicesOrder');
            $this->loadModel('PackagingCaffee');
            $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->CheckingCtn->create();
                        $findPackagingCaffee = $this->PackagingCaffee->find('first',array('conditions' => array('PackagingCaffee.id'=>$idOIE)));
                        $this->request->data['CheckingCtn']['date_check']=date("Y-m-d H:i:s");
                        $this->request->data['CheckingCtn']['users_id']=$this->Session->read('User.id');
                        $this->Flash->success(__('Registro chequeo del ctn existoso!'));
                        //ebug($this->request->data);exit;
                        if($this->CheckingCtn->save($this->request->data)){
                            $findPackagingCaffee['PackagingCaffee']['checking_ctn_id']=$this->CheckingCtn->id;
                            $this->PackagingCaffee->save($findPackagingCaffee);
                            $stateContainer=$this->request->data['CheckingCtn']['status'];
                            if($stateContainer === "RECHAZADO"){
                                $dataDetailPackagingCaffees = $this->DetailPackagingCaffee->find('all',array('conditions'=>array('DetailPackagingCaffee.packaging_caffee_id'=>$idOIE)));
                                $this->PackagingCaffee->updateAll(
                                    array(
                                        'PackagingCaffee.state_packaging_id'=>8,
                                        'PackagingCaffee.cause_emptied_ctn_id'=>$this->request->data['PackagingCaffee']['cause_emptied_ctn_id']),
                                    array('PackagingCaffee.id'=>$idOIE));
                                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state'=>8),array('DetailPackagingCaffee.packaging_caffee_id'=>$idOIE));
                                foreach ($dataDetailPackagingCaffees as $detailPackagingCaffee){
                                    $remesa = $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'];
                                    $weighingPackagingCaffees = $this->WeighingPackagingCaffee->find('all',array('conditions'=>array('WeighingPackagingCaffee.id_oie'=>$idOIE)));
                                    $palletDevueltos = 0;
                                    $sacosDevueltos = 0;
                                    foreach($weighingPackagingCaffees as $weighingPackagingCaffee){
                                        $palletDevueltos += $weighingPackagingCaffee['WeighingPackagingCaffee']['seq_weight_pallet'];
                                        $sacosDevueltos += $weighingPackagingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet '];
                                    }
                                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first',array('conditions'=>array('RemittancesCaffee.id'=>$remesa)));
                                    $this->RemittancesCaffee->updateAll(
                                            array('RemittancesCaffee.quantity_bag_in_store'=>$dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']+$sacosDevueltos,
                                            'RemittancesCaffee.quantity_bag_out_store'=>$dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store']-$sacosDevueltos,
                                            'RemittancesCaffee.quantity_in_pallet_caffee'=>$dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee']+$palletDevueltos,
                                            'RemittancesCaffee.quantity_out_pallet_caffee'=>$dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee']-$palletDevueltos),
                                            array('RemittancesCaffee.id'=>$remesa));
                                }
                                $this->Flash->success(__('Se registro una cancelación del contenedor '.$findPackagingCaffee['PackagingCaffee']['id']));
                                return $this->redirect(array('controller' => 'PackagingCaffees', 'action' => 'process_oie'));
                            }
                            $this->Flash->success(__('Se registro el chequeo del ctrn exitosamente.'));
                            return $this->redirect(array('controller'=>'PackagingCaffees', 'action' => 'index'));
                        }
			else {
				$this->Flash->error(__('No se pudo registrar el chequeo del CTNR. Intentelo nuevamente'));
			}
		}
            $this->set('findPackagingCaffee',$this->PackagingCaffee->find('first',array('conditions' => array('PackagingCaffee.id'=>$idOIE))));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CheckingCtn->exists($id)) {
			throw new NotFoundException(__('Invalid checking ctn'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CheckingCtn->save($this->request->data)) {
				$this->Flash->success(__('The checking ctn has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The checking ctn could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CheckingCtn.' . $this->CheckingCtn->primaryKey => $id));
			$this->request->data = $this->CheckingCtn->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CheckingCtn->id = $id;
		if (!$this->CheckingCtn->exists()) {
			throw new NotFoundException(__('Invalid checking ctn'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CheckingCtn->delete()) {
			$this->Flash->success(__('The checking ctn has been deleted.'));
		} else {
			$this->Flash->error(__('The checking ctn could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
