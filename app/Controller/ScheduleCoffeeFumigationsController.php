<?php
App::uses('AppController', 'Controller');
/**
 * ScheduleCoffeeFumigations Controller
 *
 * @property ScheduleCoffeeFumigation $ScheduleCoffeeFumigation
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ScheduleCoffeeFumigationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');


	public function index(){
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $this->loadModel('ScheduleCoffeeFumigation'); 


        $this->Paginator->settings = [
            'fields'=> [
                'FumigationService.id',
                'HookupStatus.status_name', 
                'ScheduleCoffeeFumigation.lot_coffee',
                'ScheduleCoffeeFumigation.qta_coffee',
                'ScheduleCoffeeFumigation.id',
                'Client.business_name',
                'Departament.name',
                'ScheduleCoffeeFumigation.reg_date',],
            'limit'=>20,
			'conditions' => array(
				'FumigationService.hookup_status_id' => [2,3],
                'FumigationService.modality' => 'PUERTO',
                'Departament.name' => 'SPB',
				'ScheduleCoffeeFumigation.status' => 1,
            ),
            'joins' => array(
                array(
                    'table' => 'hookup_status',
                    'alias' => 'HookupStatus',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'HookupStatus.id = FumigationService.hookup_status_id'
                    )),
                array(
                        'table' => 'clients',
                        'alias' => 'Client',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Client.id = FumigationService.client_id'
                        )),
                array(
                        'table' => 'departaments',
                        'alias' => 'Departament',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Departament.id = FumigationService.departament_id'
                        )
                ))
        ];

        //debug($this->Paginator->paginate());exit;
        $this->set('scheduleCoffeeFumigations', $this->Paginator->paginate());
    }

}
