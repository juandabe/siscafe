<?php
App::uses('AppController', 'Controller');
/**
 * TraceabilityPackagings Controller
 *
 * @property TraceabilityPackaging $TraceabilityPackaging
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TraceabilityPackagingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TraceabilityPackaging->recursive = 0;
		$this->set('traceabilityPackagings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TraceabilityPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid traceability packaging'));
		}
		$options = array('conditions' => array('TraceabilityPackaging.' . $this->TraceabilityPackaging->primaryKey => $id));
		$this->set('traceabilityPackaging', $this->TraceabilityPackaging->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add1($idOIE = null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->loadModel('PackagingCaffee');
            $this->layout = 'colaborador';
            if ($this->request->is('post')) {
                $db = $this->TraceabilityPackaging->getDataSource();
                $this->TraceabilityPackaging->create();
                $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE)));
                $traceablityPackaging = $this->TraceabilityPackaging->find('first',array('conditions'=>array('TraceabilityPackaging.id'=>$packagingCaffee['PackagingCaffee']['traceability_packaging_id'])));
                if(empty($traceablityPackaging)){
                    $this->request->data['TraceabilityPackaging']['date_created']=date("Y-m-d H:i:s");
                    $file_img1_path = $this->request->data['TraceabilityPackaging']['img1'];
                    $this->request->data['TraceabilityPackaging']['img1'] = null;
                    $file_img2_path = $this->request->data['TraceabilityPackaging']['img2'];
                    $this->request->data['TraceabilityPackaging']['img2'] = null;
                    $file_img3_path = $this->request->data['TraceabilityPackaging']['img3'];
                    $this->request->data['TraceabilityPackaging']['img3'] = null;
                    if ($this->TraceabilityPackaging->save($this->request->data)) {
                            $traceablityPackagingId = $this->TraceabilityPackaging->id;
                            $this->PackagingCaffee->updateAll(
                                    array('PackagingCaffee.traceability_packaging_id'=>$traceablityPackagingId),
                                    array('PackagingCaffee.id'=>$idOIE));
                    } else {
                            $this->Flash->error(__('Trazabilidad no se registro correctamente. Por favor intento nuevamente.'));
                            return $this->redirect(array('controller'=>'PackagingCaffees','action' => 'index'));
                    }
                    $pathDirectory='data/TraceabilityPackaging'."/".$packagingCaffee['PackagingCaffee']['id']."/".$packagingCaffee['ReadyContainer']['bic_container'];
                    if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
                        mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
                    }
                    $pathImg1=$pathDirectory."/".$file_img1_path['name'];
                    $resultImg1 = move_uploaded_file($file_img1_path['tmp_name'], WWW_ROOT.'img/'.$pathImg1);
                    $pathImg2=$pathDirectory."/".$file_img2_path['name'];
                    $resultImg2 = move_uploaded_file($file_img2_path['tmp_name'], WWW_ROOT.'img/'.$pathImg2);
                    $pathImg3=$pathDirectory."/".$file_img3_path['name'];
                    $resultImg3 = move_uploaded_file($file_img3_path['tmp_name'], WWW_ROOT.'img/'.$pathImg3);
                    if($this->TraceabilityPackaging->updateAll(
                            array('TraceabilityPackaging.img1'=>$db->value($pathImg1,'string'),
                            'TraceabilityPackaging.img2'=>$db->value($pathImg2,'string'),
                            'TraceabilityPackaging.img3'=>$db->value($pathImg3,'string')),
                            array('TraceabilityPackaging.id'=>$traceablityPackagingId)
                            )){
                        $this->Flash->success(__('Trazabilidad embalaje actualizada exitosamente.'));
                        return $this->redirect(array('controller'=>'TraceabilityPackagings','action' => 'add2',$idOIE));
                    }
                }
                else{
                    if($traceablityPackaging['$traceablityPackaging']['img4'] === null){
                        return $this->redirect(array('controller'=>'TraceabilityPackagings','action' => 'add2',$idOIE));
                    }
                    else if($traceablityPackaging['$traceablityPackaging']['img7'] === null){
                        return $this->redirect(array('controller'=>'TraceabilityPackagings','action' => 'add3',$idOIE));
                    }
                    else if($traceablityPackaging['$traceablityPackaging']['img10'] === null){
                        return $this->redirect(array('controller'=>'TraceabilityPackagings','action' => 'add4',$idOIE));
                    }
                    }
            }
            $this->set('packagingCoffee',$this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE))));
	}
        
        
        /**
 * add method
 *
 * @return void
 */
	public function add2($idOIE = null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->loadModel('PackagingCaffee');
            $this->layout = 'colaborador';
		if ($this->request->is('post')) {
                        $db = $this->TraceabilityPackaging->getDataSource();
			$this->TraceabilityPackaging->create();
                        $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE)));
                        $traceablityPackaging = $this->TraceabilityPackaging->find('first',array('conditions'=>array('TraceabilityPackaging.id'=>$packagingCaffee['PackagingCaffee']['traceability_packaging_id'])));
                        if(!empty($traceablityPackaging)){
                            $file_img4_path = $this->request->data['TraceabilityPackaging']['img4'];
                            $file_img5_path = $this->request->data['TraceabilityPackaging']['img5'];
                            $file_img6_path = $this->request->data['TraceabilityPackaging']['img6'];
                            $pathDirectory='data/TraceabilityPackaging'."/".$packagingCaffee['PackagingCaffee']['id']."/".$packagingCaffee['ReadyContainer']['bic_container'];
                            if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
                                mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
                            }
                            $pathImg4=$pathDirectory."/".$file_img4_path['name'];
                            $resultImg4 = move_uploaded_file($file_img4_path['tmp_name'], WWW_ROOT.'img/'.$pathImg4);
                            $pathImg5=$pathDirectory."/".$file_img5_path['name'];
                            $resultImg5 = move_uploaded_file($file_img5_path['tmp_name'], WWW_ROOT.'img/'.$pathImg5);
                            $pathImg6=$pathDirectory."/".$file_img6_path['name'];
                            $resultImg6 = move_uploaded_file($file_img6_path['tmp_name'], WWW_ROOT.'img/'.$pathImg6);
                            if($this->TraceabilityPackaging->updateAll(
                                    array('TraceabilityPackaging.img4'=>$db->value($pathImg4,'string'),
                                    'TraceabilityPackaging.img5'=>$db->value($pathImg5,'string'),
                                    'TraceabilityPackaging.img6'=>$db->value($pathImg6,'string')),
                                    array('TraceabilityPackaging.id'=>$traceablityPackaging['TraceabilityPackaging']['id']))){
                                $this->Flash->success(__('Trazabilidad embalaje actualizada correctamente.'));
                                return $this->redirect(array('controller'=>'TraceabilityPackagings','action' => 'add3',$idOIE));
                            }
                        }
		}
                $this->set('packagingCoffee',$this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE))));
	}
        
                /**
 * add method
 *
 * @return void
 */
	public function add3($idOIE = null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->loadModel('PackagingCaffee');
            $this->layout = 'colaborador';
		if ($this->request->is('post')) {
                        $db = $this->TraceabilityPackaging->getDataSource();
			$this->TraceabilityPackaging->create();
                        $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE)));
                        $traceablityPackaging = $this->TraceabilityPackaging->find('first',array('conditions'=>array('TraceabilityPackaging.id'=>$packagingCaffee['PackagingCaffee']['traceability_packaging_id'])));
                        if(!empty($traceablityPackaging)){
                            $file_img7_path = $this->request->data['TraceabilityPackaging']['img7'];
                            $file_img8_path = $this->request->data['TraceabilityPackaging']['img8'];
                            $file_img9_path = $this->request->data['TraceabilityPackaging']['img9'];
                            $pathDirectory='data/TraceabilityPackaging'."/".$packagingCaffee['PackagingCaffee']['id']."/".$packagingCaffee['ReadyContainer']['bic_container'];
                            if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
                                mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
                            }
                            $pathImg7=$pathDirectory."/".$file_img7_path['name'];
                            $resultImg7 = move_uploaded_file($file_img7_path['tmp_name'], WWW_ROOT.'img/'.$pathImg7);
                            $pathImg8=$pathDirectory."/".$file_img7_path['name'];
                            $resultImg8 = move_uploaded_file($file_img8_path['tmp_name'], WWW_ROOT.'img/'.$pathImg8);
                            $pathImg9=$pathDirectory."/".$file_img9_path['name'];
                            $resultImg9 = move_uploaded_file($file_img9_path['tmp_name'], WWW_ROOT.'img/'.$pathImg9);
                            if($this->TraceabilityPackaging->updateAll(
                                    array('TraceabilityPackaging.img7'=>$db->value($pathImg7,'string'),
                                    'TraceabilityPackaging.img8'=>$db->value($pathImg8,'string'),
                                    'TraceabilityPackaging.img9'=>$db->value($pathImg9,'string')),
                                    array('TraceabilityPackaging.id'=>$traceablityPackaging['TraceabilityPackaging']['id'])
                                    )){
                                $this->Flash->success(__('Trazabilidad embalaje actualizada correctamente.'));
                                return $this->redirect(array('controller'=>'TraceabilityPackagings','action' => 'add4',$idOIE));
                            }
                        }
		}
                $this->set('packagingCoffee',$this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE))));
	}
        
                        /**
 * add method
 *
 * @return void
 */
	public function add4($idOIE = null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->loadModel('PackagingCaffee');
            $this->layout = 'colaborador';
		if ($this->request->is('post')) {
                        $db = $this->TraceabilityPackaging->getDataSource();
			$this->TraceabilityPackaging->create();
                        $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE)));
                        $traceablityPackaging = $this->TraceabilityPackaging->find('first',array('conditions'=>array('TraceabilityPackaging.id'=>$packagingCaffee['PackagingCaffee']['traceability_packaging_id'])));
                        if(!empty($traceablityPackaging)){
                            $array_time_ini = $this->request->data['TraceabilityPackaging']['time_start'];
                            $array_time_end = $this->request->data['TraceabilityPackaging']['time_end'];
                            $file_img10_path = $this->request->data['TraceabilityPackaging']['img10'];
                            $file_img11_path = $this->request->data['TraceabilityPackaging']['img11'];
                            $file_img12_path = $this->request->data['TraceabilityPackaging']['img12'];
                            $seal1 = $this->request->data['TraceabilityPackaging']['seal1'];
                            $seal2 = $this->request->data['TraceabilityPackaging']['seal2'];
                            $seal3 = $this->request->data['TraceabilityPackaging']['seal3'];
                            $seal4 = $this->request->data['TraceabilityPackaging']['seal4'];
                            $pathDirectory='data/TraceabilityPackaging'."/".$packagingCaffee['PackagingCaffee']['id']."/".$packagingCaffee['ReadyContainer']['bic_container'];
                            if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
                                mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
                            }
                            $pathImg10=$pathDirectory."/".$file_img10_path['name'];
                            $resultImg10 = move_uploaded_file($file_img10_path['tmp_name'], WWW_ROOT.'img/'.$pathImg10);
                            $pathImg11=$pathDirectory."/".$file_img11_path['name'];
                            $resultImg11 = move_uploaded_file($file_img11_path['tmp_name'], WWW_ROOT.'img/'.$pathImg11);
                            $pathImg12=$pathDirectory."/".$file_img12_path['name'];
                            $resultImg12 = move_uploaded_file($file_img12_path['tmp_name'], WWW_ROOT.'img/'.$pathImg12);
                            if($this->TraceabilityPackaging->updateAll(
                                    array(
                                        'TraceabilityPackaging.completed'=>1,
                                        'TraceabilityPackaging.img10'=>$db->value($pathImg10,'string'),
                                        'TraceabilityPackaging.img11'=>$db->value($pathImg11,'string'),
                                        'TraceabilityPackaging.img12'=>$db->value($pathImg12,'string')),
                                    array('TraceabilityPackaging.id'=>$traceablityPackaging['TraceabilityPackaging']['id'])
                                    )){
                                        $this->PackagingCaffee->updateAll(
                                                array(
                                                    'PackagingCaffee.time_start' => $db->value($array_time_ini['year'].'-'.$array_time_ini['month'].'-'.$array_time_ini['day'].' '.$array_time_ini['hour'].':'.$array_time_ini['min'],'string'),
                                                    'PackagingCaffee.time_end' => $db->value($array_time_end['year'].'-'.$array_time_end['month'].'-'.$array_time_end['day'].' '.$array_time_end['hour'].':'.$array_time_end['min'],'string'),
                                                    'PackagingCaffee.seal_1' => $db->value($seal1,'string'),
                                                    'PackagingCaffee.seal_2' => $db->value($seal2,'string'),
                                                    'PackagingCaffee.seal_3' => $db->value($seal3,'string'),
                                                    'PackagingCaffee.seal_4' => $db->value($seal4,'string')),
                                                array('PackagingCaffee.id' => $idOIE));
                                $this->Flash->success(__('Trazabilidad embalaje actualizada correctamente.'));
                                return $this->redirect(array('controller'=>'PackagingCaffees','action' => 'index'));
                            }
                        }
		}
                $this->set('packagingCoffee',$this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE))));
	}
        
        /**
 * zip method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function zip($oie = null) {
		$this->autoRender = false;
                $this->loadModel('PackagingCaffee');
                $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$oie)));
                $targzname = "fotos-trazabilidad-embalaje-".$packagingCaffee['ReadyContainer']['bic_container'].".tar.gz";
                shell_exec("cd ".WWW_ROOT.DS.'img'.DS.'data/TraceabilityPackaging/'.";tar -cvf ./targz/".$targzname." ".$oie);
                $this->response->file(
                    WWW_ROOT.DS.'img'.DS.'data/TraceabilityPackaging/targz/'.$targzname,
                    array('download' => true, 'name' => $targzname)
                );       
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TraceabilityPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid traceability packaging'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TraceabilityPackaging->save($this->request->data)) {
				$this->Flash->success(__('The traceability packaging has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The traceability packaging could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TraceabilityPackaging.' . $this->TraceabilityPackaging->primaryKey => $id));
			$this->request->data = $this->TraceabilityPackaging->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TraceabilityPackaging->id = $id;
		if (!$this->TraceabilityPackaging->exists()) {
			throw new NotFoundException(__('Invalid traceability packaging'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TraceabilityPackaging->delete()) {
			$this->Flash->success(__('The traceability packaging has been deleted.'));
		} else {
			$this->Flash->error(__('The traceability packaging could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
