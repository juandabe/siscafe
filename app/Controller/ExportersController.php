<?php
App::uses('AppController', 'Controller');
/**
 * Exporters Controller
 *
 * @property Exporter $Exporter
 * @property PaginatorComponent $Paginator
 */
class ExportersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function exporterBycode($Embalajes) {
	  $codeExporter = $Embalajes[0]["Embalaje"]["id_exportador"];
	  return $this->Exporter->find('first', array('conditions' => array('Exporter.id_exportador = ' => $codeExporter)));
	}

}
