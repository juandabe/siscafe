<?php
App::uses('AppController', 'Controller');
/**
 * AdictionalElementsHasPackagingCaffees Controller
 *
 * @property AdictionalElementsHasPackagingCaffee $AdictionalElementsHasPackagingCaffee
 * @property PaginatorComponent $Paginator
 */
class AdictionalElementsHasPackagingCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AdictionalElementsHasPackagingCaffee->recursive = 0;
		$this->set('adictionalElementsHasPackagingCaffees', $this->Paginator->paginate());
	}

/**
 * add method
 *
 * @return void
 */
	public function addElementAdicional() {
          if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
          }
	  $this->layout = 'colaborador';
	  $info_navy_id = isset($this->request->query['info_navy_id']) ? $this->request->query['info_navy_id']: null;
	  $this->loadModel('InfoNavy');
          $option=array('conditions' => array('InfoNavy.id' => $info_navy_id));
          $infoNavy = $this->InfoNavy->find('first',$option);
	  $proforma = $infoNavy['InfoNavy']['proforma'];
          if ($this->request->is('post')) {
            $this->AdictionalElementsHasPackagingCaffee->create();
            $this->request->data['AdictionalElementsHasPackagingCaffee']['info_navy_id']=$info_navy_id;
            if ($this->AdictionalElementsHasPackagingCaffee->save($this->request->data)) {
                    $this->Flash->success(__('Se agregó el elemento adicional con exitó a la proforma!'));
                    return $this->redirect(array('controller'=> 'AdictionalElementsHasPackagingCaffees','action' => 'addelementadicional','?' => ['info_navy_id' => $info_navy_id,'proforma'=> $proforma]));
            } else {
                    $this->Flash->error(__('Elemento adicional no se pudo agregar.Intelo nuevamente'));
            }
          }
	  $options=array('conditions' => array('AdictionalElementsHasPackagingCaffee.info_navy_id' => $info_navy_id));
	  $allElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->find('all',$options);
	  $this->loadModel('AdictionalElement');
	  $allAdictionalElements = $this->AdictionalElement->find('list');
	  $this->set(compact('allAdictionalElements'));
	  $this->set('allElementsHasPackagingCaffee',$allElementsHasPackagingCaffee);
	  $this->set('proforma',$proforma);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AdictionalElementsHasPackagingCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid adictional elements has packaging caffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AdictionalElementsHasPackagingCaffee->save($this->request->data)) {
				$this->Flash->success(__('The adictional elements has packaging caffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The adictional elements has packaging caffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AdictionalElementsHasPackagingCaffee.' . $this->AdictionalElementsHasPackagingCaffee->primaryKey => $id));
			$this->request->data = $this->AdictionalElementsHasPackagingCaffee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
		$adictional_elements_id = isset($this->request->query['adictional_elements_id']) ? $this->request->query['adictional_elements_id']: null;
		$info_navy_id = isset($this->request->query['info_navy_id']) ? $this->request->query['info_navy_id']: null;
                $this->loadModel('InfoNavy');
                $option=array('conditions' => array('InfoNavy.id' => $info_navy_id));
                $infoNavy = $this->InfoNavy->find('first',$option);
                $proforma = $infoNavy['InfoNavy']['proforma'];
		if ($this->AdictionalElementsHasPackagingCaffee->deleteAll(['AdictionalElementsHasPackagingCaffee.adictional_elements_id' => $adictional_elements_id,
		'AdictionalElementsHasPackagingCaffee.info_navy_id' => $info_navy_id],false)) {
			$this->Flash->success(__('Elemento adicional eliminado!'));
			return $this->redirect(array('controller'=> 'AdictionalElementsHasPackagingCaffees','action' => 'addelementadicional','?' => ['info_navy_id' => $info_navy_id,'proforma' => $proforma]));
		} else {
			$this->Flash->error(__('No se pudo eliminar el elemento adicional!'));
			return $this->redirect(array('controller'=> 'AdictionalElementsHasPackagingCaffees','action' => 'addelementadicional','?' => ['info_navy_id' => $info_navy_id,'proforma' => $proforma]));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
