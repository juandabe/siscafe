<?php
App::uses('AppController', 'Controller');
/**
 * UnitsCaffees Controller
 *
 * @property UnitsCaffee $UnitsCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 */
class UnitsCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash');

}
