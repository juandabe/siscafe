<?php

App::uses('AppController', 'Controller');
App::uses('UtilHelper', 'View/Helper');

require_once("../Vendor/aws/aws-autoloader.php");
 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/**
 * ServicesOrders Controller
 *
 * @property ServicesOrder $ServicesOrder
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ServicesOrdersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('User');
        $this->loadModel('Departament');
        $this->loadModel('ServicesOrdersBilling');
        $currentUser = $this->User->find('first',array('conditions' => array('and'=>array('User.id' => $this->Session->read('User.id')))));
        $this->layout = 'colaborador';
        $this->Paginator->settings = [
            'limit' => 30,
            'fields'=>['Client.business_name,HookupStatus.status_name,HookupStatus.id,ServicesOrder.created_date,
            ServicesOrder.closed_date,Departament.name'],
            'conditions' => ['Departament.cod_ce_unoee' => $currentUser['Departament']['cod_ce_unoee'], 'ServicesOrder.hookup_status_id IN'=>['1','2','3','5	']],
            'order' => ['ServicesOrder.id' => 'DESC']
        ];
        $this->set('servicesOrders', $this->Paginator->paginate('ServicesOrder'));
    }


    public function completed() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('User');
        $this->loadModel('Departament');
        $this->loadModel('ServicesOrdersBilling');
        $currentUser = $this->User->find('first',array('conditions' => array('and'=>array('User.id' => $this->Session->read('User.id')))));
        $this->layout = 'colaborador';
        $this->Paginator->settings = [
            'limit' => 30,
            'fields'=>['Client.business_name,HookupStatus.status_name,HookupStatus.id,ServicesOrder.created_date,
            ServicesOrder.closed_date,Departament.name'],
            'conditions' => ['Departament.cod_ce_unoee' => $currentUser['Departament']['cod_ce_unoee'], 'ServicesOrder.hookup_status_id'=>4 ],
            'order' => ['ServicesOrder.id' => 'DESC']
        ];
        $this->set('servicesOrders', $this->Paginator->paginate('ServicesOrder'));
    }
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->ServicesOrder->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->layout = null ;
        $this->ServicesOrder->recursive = 1;
        $options = array(
            'conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id)
        );
        $servicesOrder = $this->ServicesOrder->find('first', $options);
        //debug($servicesOrder);exit;
        $this->set('servicesOrder', $servicesOrder);
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('DetailServicesOrderCrossdocking');
        $this->loadModel('ServicesOrdersSupply');
        $this->loadModel('ServicesOrdersBilling');

        $this->Paginator->settings = 
        [
            'fields'=>['DetailsServicesToCaffee.id','DetailsServicesToCaffee.remittances_caffee_id','Client.exporter_code', 
            'RemittancesCaffee.lot_caffee','DetailsServicesToCaffee.qta_bag_to_work','DetailsServicesToCaffee.observation',
            'DetailsServicesToCaffee.completed','DetailsServicesToCaffee.services_orders_id',
            'ItemsUnoeeConexo.ITEMS_UNOEE','ItemsUnoeeConexo.TARIFA','DetailsServicesToCaffee.value'],
            'joins'=>[
                        [
                        'table' => 'items_unoee_conexos',
                            'alias' => 'ItemsUnoeeConexo',
                            'type' => 'LEFT',
                            'conditions' => [
                                'and'=>[
                                    'DetailsServicesToCaffee.items_unoee = ItemsUnoeeConexo.ITEMS_UNOEE',
                                    'DetailsServicesToCaffee.lista_precio = ItemsUnoeeConexo.LISTA_PRECIO'
                                ]
                            ]
                        ],
                        [
                            'table' => 'clients',
                                'alias' => 'Client',
                                'type' => 'LEFT',
                                'conditions' => [
                                    ['Client.id = ServicesOrder.exporter_code']
                                ]
                        ]
            ],
            'conditions'=> ['DetailsServicesToCaffee.services_orders_id'=>$id]
        ];
        $dataDetailsServicesToCaffee= $this->Paginator->paginate('DetailsServicesToCaffee');
        $this->set('detailsServicesToCaffees', $dataDetailsServicesToCaffee);
        $this->Paginator->settings = 
        [
            'fields'=>['ItemsUnoeeConexo.ITEMS_UNOEE','ItemsUnoeeConexo.TARIFA','ItemsUnoeeConexo.DESCRIPCION_ITEM','ServicesOrdersSupply.cantidad', 'ServicesOrdersSupply.is_move',
            'ServicesOrdersSupply.observation','Departament.name','ServicesOrdersSupply.date_reg','ServicesOrdersSupply.id'],
            'joins'=>[
                        
                        [
                            'table' => 'items_unoee_conexos',
                                'alias' => 'ItemsUnoeeConexo',
                                'type' => 'LEFT',
                                'conditions' => [
                                    'and'=>[
                                        'ServicesOrdersSupply.items_unoee = ItemsUnoeeConexo.ITEMS_UNOEE',
                                        'ServicesOrdersSupply.lista_precio = ItemsUnoeeConexo.LISTA_PRECIO'
                                    ]
                                ]
                        ],
                        [
                            'table' => 'departaments',
                                'alias' => 'Departament',
                                'type' => 'LEFT',
                                'conditions' => [
                                    ['Departament.id = ServicesOrdersSupply.terminal_arrive']
                                ]
                        ]
            ],
            'conditions'=> ['ServicesOrdersSupply.id_services_order'=>$id]
        ];

        $dataServicesOrdersBillings = $this->ServicesOrdersBilling->find('all',['conditions'=> ['ServicesOrdersBilling.id_services_order'=>$id]]);
        $dataServicesOrdersSupply = $this->Paginator->paginate('ServicesOrdersSupply');
        $this->set('dataServicesOrdersSupply', $dataServicesOrdersSupply);
        $this->set('dataServicesOrdersBillings', $dataServicesOrdersBillings);
    }
    
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function search($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';  
    }


    function ajaxSearch($textSearch = null){
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;

        $this->loadModel('User');
        $currentUser = $this->User->find('first',array('conditions' => array('and'=>array('User.id' => $this->Session->read('User.id')))));

        if($currentUser['User']['departaments_id'] == 1){
            $dataServicesOrders = $this->ServicesOrder->find('all',
            [
                'conditions'=>[ ['or'=>
                [
                    'ServicesOrdersBilling.observation LIKE'=>'%'.$textSearch.'%',
                    'ServicesOrdersBilling.observation2 LIKE'=>'%'.$textSearch.'%'
                ]], ['and'=> ['ServicesOrder.departaments_id IN'=>[1,7,8] ]] ],
                'joins'=>[
                    [
                    'table' => 'services_orders_billing',
                        'alias' => 'ServicesOrdersBilling',
                        'type' => 'LEFT',
                        'conditions' => [
                            'ServicesOrdersBilling.id_services_order = ServicesOrder.id',
                        ]
                    ]
                ]
            ]);
        }else{
            $dataServicesOrders = $this->ServicesOrder->find('all',
            [
                'conditions'=>[ ['or'=>
                [
                    'ServicesOrdersBilling.observation LIKE'=>'%'.$textSearch.'%',
                    'ServicesOrdersBilling.observation2 LIKE'=>'%'.$textSearch.'%'
                ]], ['and'=> ['ServicesOrder.departaments_id'=>$currentUser['User']['departaments_id'] ]] ],
                'joins'=>[
                    [
                    'table' => 'services_orders_billing',
                        'alias' => 'ServicesOrdersBilling',
                        'type' => 'LEFT',
                        'conditions' => [
                            'ServicesOrdersBilling.id_services_order = ServicesOrder.id',
                        ]
                    ]
                ]
            ]);
        }
        
        if($dataServicesOrders){
            return json_encode($dataServicesOrders);
        }
        else{
            return "";
        }
    }

	/**
     * add method
     *
     * @return void
     */
    public function complete_all($id = null){
        $idTransaction = 5;
        $this->loadModel('User');
        $this->autoRender = false;
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        else{
            $hasPermit = $this->User->Permit->find('first',['conditions'=>['and'=>['Permit.transaction_id'=> $idTransaction,'Permit.user_id'=>$this->Session->read('User.id')]]]);
            if(!$hasPermit){
                return json_encode(('No se ha podido completar la orden. No tiene permisos asociados.'));
            }
        }
        $valorRetorno;
        date_default_timezone_set('America/Bogota');
        $this->loadModel('DetailsServicesToCaffee');
        if($this->DetailsServicesToCaffee->updateAll(
            ['DetailsServicesToCaffee.completed'=>1],
            ['DetailsServicesToCaffee.services_orders_id'=>$id]
        )){
            if($this->ServicesOrder->updateAll(
                ['ServicesOrder.closed'=>true,
                'ServicesOrder.hookup_status_id'=>4,
                'ServicesOrder.closed_date'=>"'".date('Y-m-d h:i:s')."'"],
                ['ServicesOrder.id'=>$id]
            )){
                $valorRetorno = true;
            }
        }
        else{
            $valorRetorno = false;
        }
        return json_encode(['result'=>$valorRetorno]);
    }

    /**
     * completeByExpiredDate method
     *
     * @return void
     */
    public function completeByExpiredDate(){

        $this->autoRender = false;
        date_default_timezone_set('America/Bogota');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
        
        $listServicesOrder = $this->ServicesOrder->find('all',
        [
            'contains'=>['ServicesOrder'],
            'conditions' => ['and'=>[
                                'ServicesOrder.created_date <=' => date('Y-m-d', strtotime('-30 day', strtotime(date('Y-m-d H:i:s')))),
                                'ServicesOrder.hookup_status_id IN' => [1,2,3,6]
                                ]
                            ]
        ]);


        foreach($listServicesOrder as $servicesOrder){

            $this->ServicesOrder->updateAll(
                ['ServicesOrder.closed'=>true,
                'ServicesOrder.hookup_status_id'=>5,
                'ServicesOrder.closed_date'=>"'".date('Y-m-d h:i:s')."'"],
                ['ServicesOrder.id'=>$servicesOrder['ServicesOrder']["id"]]);

            $listDetailsServicesToCaffee = $this->DetailsServicesToCaffee->find('all',
            [
                'conditions' => [
                                    'DetailsServicesToCaffee.services_orders_id'=>$servicesOrder['ServicesOrder']["id"]
                ]
            ]);

            foreach($listDetailsServicesToCaffee as $rowDetailServices){

                $this->RemittancesCaffeeHasNoveltysCaffee->updateAll(
                    ['RemittancesCaffeeHasNoveltysCaffee.active'=>1],
                    ['RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id'=>$rowDetailServices['DetailsServicesToCaffee']['remittances_caffee_id']]);

            }
            
            $this->DetailsServicesToCaffee->updateAll(
                ['DetailsServicesToCaffee.completed'=>1],
                ['DetailsServicesToCaffee.services_orders_id'=>$servicesOrder['ServicesOrder']["id"]]);

        }
    }



    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('Custom');
        $this->loadModel('Client');
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->ServicesOrder->create();
            date_default_timezone_set('America/Bogota');
            $client = $this->Client->find('first',['conditions'=>['Client.exporter_code'=>$this->request->data['ServicesOrder']['exporter_code']]]);
            $this->request->data['ServicesOrder']['created_date'] = date('Y-m-d h:i:s');
            $this->request->data['ServicesOrder']['total_valor'] = 0;
            $this->request->data['ServicesOrder']['exporter_code'] = $client['Client']['id'];
            $this->request->data['ServicesOrder']['hookup_status_id'] = 1;
            $this->request->data['ServicesOrder']['departaments_id'] = $this->Session->read('User.centerId');
            $this->request->data['ServicesOrder']['custom_id'] = 1;
            $this->request->data['ServicesOrder']['create_user'] = $this->Session->read('User.id');
            //debug($this->request->data);exit;
            if ($this->ServicesOrder->save($this->request->data)) {
                $this->Flash->success(__('La nueva orden de servicio fue registrada existosamente!.'));
                return $this->redirect(array('controller'=>'ServicesOrders','action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        }
        $customns = $this->Custom->find('list', array('fields' => array('Custom.id', 'Custom.cia_name')));
        $this->set(compact('customns'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->ServicesOrder->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is(array('post', 'put'))) {
            if ($this->ServicesOrder->save($this->request->data)) {
                $this->Flash->success(__('The services order has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id));
            $this->request->data = $this->ServicesOrder->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->ServicesOrder->id = $id;
        if (!$this->ServicesOrder->exists()) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->layout = 'colaborador';
        $servicesOrder = $this->ServicesOrder->find('first',array('conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id)));
        $servicesOrder['ServicesOrder']['active']=false;
        $servicesOrder['ServicesOrder']['approve_user'] = $this->Session->read('User.id');
        if ($this->ServicesOrder->save($servicesOrder)) {
                $this->Flash->success(__('Desactiva la OS # '.$id));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        return $this->redirect(array('action' => 'index'));
    }



 /**
        * getTrackingRequest method
        *
        * @return void
        */
       public function getTrackingRequest($idRequest = null){
           if (!$this->Session->read('User.id')) {
               //return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;
           $this->layout = 'colaborador';
           $servicesOrders = $this->ServicesOrder->find('first',['conditions'=>['ServicesOrder.id'=>$idRequest ]]);
            //print_r($servicesOrders);exit();

           $bucket = 'services-orders';
           $keyname = $servicesOrders['ServicesOrder']['pdfica'];
           $folder = $servicesOrders['ServicesOrder']['id'];

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-west-1',
               'credentials' => [
                'key'    => UtilHelper::getAwsAccess()['aws_key'],
                'secret' => UtilHelper::getAwsAccess()['aws_secret']
            ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $folder."/".$keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__($e));
           }

       }




}
