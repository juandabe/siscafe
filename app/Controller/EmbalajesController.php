<?php
App::uses('AppController', 'Controller');
App::uses('PermissionsController', 'Controller');
/**
 * Embalajes Controller
 *
 * @property Embalaje $Embalaje
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class EmbalajesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	   public function index() {
            $this->layout = 'cliente';
            $this->set('embalajes',$this->Embalaje->find('all',
                        array(
                            'conditions'=> array(
                                'expotador_code' => $this->Session->read('User.id_exportador')),
                                'group' => 'bic_ctn',
                                'order' => array('packaging_date DESC' )),
                        $this->Paginator->paginate()));
	    }

        public function allembalajes() {
            $this->layout = 'administrador';
            $this->set('embalajes',$this->Embalaje->find('all'),$this->Paginator->paginate());
        }
        
        public function viewperembalaje($lotes) {
    		$options = array(
                        'conditions' => array('lotes' => $lotes,'expotador_code' => $this->Session->read('User.id_exportador')),
                        'group' => array('lotes'));
    		$this->set('embalajes', $this->Embalaje->find('all', $options));
	}
        
        public function view($id = null) {
		if (!$this->Embalaje->exists($id)) {
			throw new NotFoundException(__('Invalid pqr'));
		}
		$options = array('conditions' => array('Embalaje.' . $this->Embalaje->primaryKey => $id));
		$this->set('embalaje', $this->Embalaje->find('first', $options));
	}
        
        public function searchByLote($lotes=null)
        {
            $this->layout = 'cliente';
            if ($this->request->is('post')) {
                $lotes=$this->request->data['Embalaje']['lote'];
             return   $this->redirect(array('controller' => 'embalajes', 'action' => 'indexsearch',$lotes));
            }
        }
        
        public function indexsearch($lotes){
            $this->layout = 'cliente';
            $conditions = array('conditions' => array(
                'Embalaje.lotes' => $lotes,
                    'expotador_code' => $this->Session->read('User.id_exportador')),
                            'group' => 'bic_ctn',
                            'order' => array('packaging_date DESC')
                    );
            $embalajes = $this->Embalaje->find('all',$conditions);
            $this->set('embalajes', $embalajes);
        }
        
        
}
