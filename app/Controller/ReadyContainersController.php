<?php
App::uses('AppController', 'Controller');
/**
 * ReadyContainers Controller
 *
 * @property ReadyContainer $ReadyContainer
 * @property PaginatorComponent $Paginator
 */
class ReadyContainersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ReadyContainer->recursive = 0;
		$this->set('readyContainers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ReadyContainer->exists($id)) {
			throw new NotFoundException(__('Invalid ready container'));
		}
		$options = array('conditions' => array('ReadyContainer.' . $this->ReadyContainer->primaryKey => $id));
		$this->set('readyContainer', $this->ReadyContainer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($idOIE=null) {
            if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
		if ($this->request->is('post')) {
			$this->ReadyContainer->create();
                        $this->loadModel('PackagingCaffee');
                        $this->request->data['PackagingCaffee']['date_registre'] = date('Y-m-s h:i:s');
                        $options = array('conditions' => array('PackagingCaffee.id=' .$idOIE));
                        $findPackagingCaffee = $this->PackagingCaffee->find('first',$options);
                        debug($findPackagingCaffee);exit;
			if ($this->ReadyContainer->save($this->request->data)) {
				$this->Flash->success(__('Se actualizo el ctnr exitosamente!'));
				return $this->redirect(array('controller' => 'PackagingCaffees','action' => 'index'));
			} else {
				$this->Flash->error(__('Se genero un error registrar el ctrn. Por favor intentolo nuevamente'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ReadyContainer->exists($id)) {
			throw new NotFoundException(__('Invalid ready container'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ReadyContainer->save($this->request->data)) {
				$this->Flash->success(__('The ready container has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ready container could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ReadyContainer.' . $this->ReadyContainer->primaryKey => $id));
			$this->request->data = $this->ReadyContainer->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ReadyContainer->id = $id;
		if (!$this->ReadyContainer->exists()) {
			throw new NotFoundException(__('Invalid ready container'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ReadyContainer->delete()) {
			$this->Flash->success(__('The ready container has been deleted.'));
		} else {
			$this->Flash->error(__('The ready container could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
