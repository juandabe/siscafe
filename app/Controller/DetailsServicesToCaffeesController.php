<?php
App::uses('AppController', 'Controller');

/**
 * DetailsServicesToCaffees Controller
 *
 * @property DetailsServicesToCaffee $DetailsServicesToCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DetailsServicesToCaffeesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->DetailsServicesToCaffee->recursive = 0;
        $this->set('detailsServicesToCaffee', $this->Paginator->paginate());
    }


    /**
     * add method
     *
     * @return void
     */
    public function add($serviceOrderid = null) {
        $idTransaction = 2;
        $this->loadModel('User');
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        else{
            $hasPermit = $this->User->Permit->find('first',['conditions'=>['and'=>['Permit.transaction_id'=> $idTransaction,'Permit.user_id'=>$this->Session->read('User.id')]]]);
            if(!$hasPermit){
                return json_encode(['error'=>true,'msj'=>'El detalle de servicio NO se ha guardado. No hay permisos asociados.']);
            }
        }
        $this->loadModel('ItemsUnoeeConexo');
        $this->loadModel('ServicesOrder');
        $this->loadModel('RemittancesCaffee');
        $this->autoRender = false;
	    $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $serviceOrderid)));
        if( $this->request->is('ajax') ) {
            $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $serviceOrderid)));
            if($this->request->data['DetailsServicesToCaffee']['lock_remesa']){
                if($this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'] == null){
                    return json_encode(['error'=>true,'msj'=>'Esta tratando de bloquear una fracción no seleccionada. Por favor asocie una remesa']);
                }
                $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
                $noveltys = $this->RemittancesCaffeeHasNoveltysCaffee->find('first', ['conditions' => ['remittances_caffee_id' => $this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'],
                'noveltys_caffee_id' => 4]]);
                if (empty($noveltys)) {
                    $noveltys = $this->RemittancesCaffeeHasNoveltysCaffee->create([
                        'remittances_caffee_id' => $this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'],
                        'noveltys_caffee_id' => 4,
                        'created_date' => date('Y-m-d h:i:s'),
                        'active' => 1]);
                    $this->RemittancesCaffeeHasNoveltysCaffee->save($noveltys);
                }
            }

            $dataItemsUnoeeConexo = $this->ItemsUnoeeConexo->find('first',
            [
                'conditions'=>[
                    'and'=>[
                        'ItemsUnoeeConexo.ITEMS_UNOEE'=>explode("-",$this->request->data['DetailsServicesToCaffee']['service_package'])[0],
                        'ItemsUnoeeConexo.LISTA_PRECIO'=>explode("-",$this->request->data['DetailsServicesToCaffee']['service_package'])[1]
                    ]
                ]
            ]
            );

            $dataDetail = $this->DetailsServicesToCaffee->create([
                'remittances_caffee_id' => $this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'],
                'items_unoee' =>explode("-",$this->request->data['DetailsServicesToCaffee']['service_package'])[0],
                'lista_precio'=>explode("-",$this->request->data['DetailsServicesToCaffee']['service_package'])[1],
                'services_orders_id'=>$this->request->data['DetailsServicesToCaffee']['services_orders_id'],
                'qta_bag_to_work'=>$this->request->data['DetailsServicesToCaffee']['qta_bag_to_work'],
                'completed'=>0,
                'observation'=> $dataItemsUnoeeConexo['ItemsUnoeeConexo']['DESCRIPCION_ITEM'].' x '.$this->request->data['DetailsServicesToCaffee']['qta_bag_to_work'],
                'value'=>($dataItemsUnoeeConexo['ItemsUnoeeConexo']['UNIDAD_FACTURACION'] == 1) ? $dataItemsUnoeeConexo['ItemsUnoeeConexo']['TARIFA']*$this->request->data['DetailsServicesToCaffee']['qta_bag_to_work']:$dataItemsUnoeeConexo['ItemsUnoeeConexo']['TARIFA'],
                'created_date' => date('Y-m-d h:i:s')]);

	    
            $this->DetailsServicesToCaffee->save($dataDetail);

            return json_encode(['error'=>false,'msj'=>'El detalle de servicio se ha guardado #'.$this->DetailsServicesToCaffee->id]);
        }
    }


    /**
     * add method
     *
     * @return void
     */
    public function view($serviceOrderid = null) {
        $this->layout = null ;
        
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('ItemsUnoeeConexo');
        $this->loadModel('ServicesOrder');
        $this->loadModel('RemittancesCaffee');
        
        $dataServicesOrder = $this->ServicesOrder->find('first',array('conditions' =>array('ServicesOrder.id'=>$serviceOrderid)));
        $dataGetItemConexos = $this->ItemsUnoeeConexo->find('all',
        [
            'conditions' =>
            ['and'=>
                [
                    //'ItemsUnoeeConexo.USE_PILCAF'=>0,
                    'ItemsUnoeeConexo.IS_FNC'=>($dataServicesOrder['Client']['id'] == 1) ? true:false,
                    'ItemsUnoeeConexo.CENTRO_OPERATION'=>$dataServicesOrder['Departament']['cod_ce_unoee']
                ]
            ]
        ]);
        $dataItemsUnoeeConexo;
        foreach ($dataGetItemConexos as $key => $value) {
            $dataItemsUnoeeConexo[$value['ItemsUnoeeConexo']['ITEMS_UNOEE'].'-'.$value['ItemsUnoeeConexo']['LISTA_PRECIO'].'-'.$value['ItemsUnoeeConexo']['DESCRIPCION_ITEM']] = $value['ItemsUnoeeConexo']['DESCRIPCION_ITEM'] . " ".$value['ItemsUnoeeConexo']['DESCRIPCION_LISTA_PRECIO'] . " ";
        }
        //debug($dataItemsUnoeeConexo);exit;
        $this->set('servicePackages', $dataItemsUnoeeConexo);
        $this->set('serviceOrder', $dataServicesOrder);
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null, $exportCode = null) {
        if (!$this->DetailsServicesToCaffee->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->loadModel('ServicePackage');
        if ($this->request->is(array('post', 'put'))) {

            if ($this->DetailsServicesToCaffee->save($this->request->data)) {
                $this->Flash->success(__('El detalle de servicio se ha guardado'));
                return $this->redirect(array('controller' => 'ServicesOrders', 'action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('DetailsServicesToCaffee.' . $this->DetailsServicesToCaffee->primaryKey => $id));
            $this->request->data = $this->DetailsServicesToCaffee->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($idDetaill = null) {
        $idTransaction = 1;
        $this->loadModel('User');
        $this->autoRender = false;
        $detailsServicesToCaffee;
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        else{
            $detailsServicesToCaffee = $this->DetailsServicesToCaffee->find('first',array('conditions' => ['DetailsServicesToCaffee.id' => $idDetaill]));
	    
            $hasPermit = $this->User->Permit->find('first',['conditions'=>['and'=>['Permit.transaction_id'=> $idTransaction,'Permit.user_id'=>$this->Session->read('User.id')]]]);
            if(!$hasPermit){
                return json_encode(['service_order'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['services_orders_id'],'id'=>$idDetaill,'msj'=>'El detalle de servicio No se podido borrar. No tiene permisos asociados.']);
            }
        }
        
        if( $this->request->is('ajax') ) {
            if ($this->DetailsServicesToCaffee->deleteAll([
                'DetailsServicesToCaffee.id' => $idDetaill])) {
            return json_encode(['service_order'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['services_orders_id'],'id'=>$idDetaill,'msj'=>'El detalle de servicio se ha borrado']);
            } else {
                return json_encode(['service_order'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['services_orders_id'],'id'=>$idDetaill,'msj'=>'El detalle de servicio No se podido borrar. Intentelo nuevamente.']);
            }
        }
    }

    
    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete_supply($idDetaill = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $this->loadModel('ServicesOrdersSupply');
        $dataServicesOrdersSupply = $this->ServicesOrdersSupply->find('first',['conditions'=>['ServicesOrdersSupply.id'=>$idDetaill]]);
        if ($this->ServicesOrdersSupply->deleteAll([
            'ServicesOrdersSupply.id' => $idDetaill])) {
          return json_encode(['result'=>true,'services_order'=>$dataServicesOrdersSupply['ServicesOrdersSupply']['id_services_order']]);
        } else {
            return json_encode(['result'=>false,'services_order'=>$dataServicesOrdersSupply['ServicesOrdersSupply']['id_services_order']]);
        }
        
    }

    /**
     * delete_crossdocking method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete_crossdocking($idDetail = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $this->loadModel('DetailServiceOrderCrossdocking');
        $dataDetailServiceOrderCrossdocking = $this->DetailServiceOrderCrossdocking->find('first',['conditions'=>['DetailServiceOrderCrossdocking.id'=>$idDetail]]);
        if ($this->DetailServiceOrderCrossdocking->deleteAll([
            'DetailServiceOrderCrossdocking.id' => $idDetail])) {
          return json_encode(['result'=>true,'services_order'=>$dataDetailServiceOrderCrossdocking['DetailServiceOrderCrossdocking']['services_orders_id']]);
        } else {
            return json_encode(['result'=>false,'services_order'=>$dataDetailServiceOrderCrossdocking['DetailServiceOrderCrossdocking']['services_orders_id']]);
        }
        
    }

    public function completed($idDetaill = null) {
        $idTransaction = 5;
        $this->loadModel('User');
        $this->autoRender = false;
	var_dump($this->Session->read('User.id'));
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        else{
            var_dump($this->Session->read('User.id'));
            $hasPermit = $this->User->Permit->find('first',['conditions'=>['and'=>['Permit.transaction_id'=> $idTransaction,'Permit.user_id'=>$this->Session->read('User.id')]]]);
            if(!$hasPermit){
                return json_encode(('No se ha podido completar el servicio. No tiene permisos asociados.'));
            }
        }
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('ServicesOrder');
        $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
        date_default_timezone_set('America/Bogota');
        $detailsServicesToCaffee = $this->DetailsServicesToCaffee->find('first',array('conditions' => ['DetailsServicesToCaffee.id' => $idDetaill]));
        if ($this->DetailsServicesToCaffee->updateAll(['completed' => 'true', 'updated_date' => "'" . date('Y-m-d h:i:s') . "'"], ['DetailsServicesToCaffee.id' => $idDetaill])) {
            $detailsCompleted = $this->DetailsServicesToCaffee->find('all', ['conditions' => ['DetailsServicesToCaffee.services_orders_id' => $detailsServicesToCaffee['DetailsServicesToCaffee']['services_orders_id'], 'DetailsServicesToCaffee.completed' => 0]]);
            if (empty($detailsCompleted)) {
                $this->ServicesOrder->updateAll(
                        ['closed' => 1, 'hookup_status_id' => 4,'closed_date' => "'" . date('Y-m-d h:i:s') . "'", 'approve_user' => $this->Session->read('User.id')], ['ServicesOrder.id' => $detailsServicesToCaffee['DetailsServicesToCaffee']['services_orders_id']]);
            }
            $detailsServicesToCaffee = $this->DetailsServicesToCaffee->find('first',array('conditions' => ['DetailsServicesToCaffee.id' => $idDetaill]));
            if($detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id'] != null){
                $this->RemittancesCaffeeHasNoveltysCaffee->updateAll(array('RemittancesCaffeeHasNoveltysCaffee.active'=>0),array('RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id']));
            }
            return json_encode(['Se ha completado el trabajo #'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['id'],'para la orden de servicio #'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['services_orders_id']]);
        } else {
            return json_encode(('No se ha podido completar el servicio. Por favor intentelo nuevamente.'));
        }
    }

}
