<?php

App::uses('AppController', 'Controller');

/**
 * Customs Controller
 *
 * @property Custom $Custom
 * @property PaginatorComponent $Paginator
 */
class CustomsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    //public $components = array('Paginator');
    public $components = array('Paginator', 'Flash', 'Session');

    public function findById($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $custom = $this->Custom->find('first', array('conditions' => array('Custom.id' => $id)));
        return json_encode($custom);
    }
}
