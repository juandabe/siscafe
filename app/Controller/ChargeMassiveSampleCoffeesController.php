<?php
App::uses('AppController', 'Controller');
require_once("../Vendor/aws/aws-autoloader.php");
App::import('Vendor', 'PHPExcel/IOFactory');
 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/**
 * ChargeMassiveSampleCoffees Controller
 *
 * @property ChargeMassiveSampleCoffee $ChargeMassiveSampleCoffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ChargeMassiveSampleCoffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            if($this->Session->read('User.profiles_id')==1){
                $this->layout = 'colaborador';
            }
            else if($this->Session->read('User.profiles_id')==9){
                $this->layout = 'invitado';
            }
	    
            $this->ChargeMassiveSampleCoffee->recursive = 0;

	    $userMitsui = array(164,168,169,170,1);

	    if($this->Session->read('User.id') == 164 || $this->Session->read('User.id') == 168 || $this->Session->read('User.id') == 169 || $this->Session->read('User.id') == 170){            
	    $this->Paginator->settings = array(
                    'limit' => 30,
                    'conditions' => array('and'=>
                        array('ChargeMassiveSampleCoffee.user_reg'=>$userMitsui)),
                    'order' => array('ChargeMassiveSampleCoffee.created_date' => 'ASC'));
	    }

            $this->set('chargeMassiveSampleCoffees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'invitado';
            $this->loadModel('CoffeeSample');
            $this->loadModel('User');
            $this->loadModel('TypeSampleCoffe');
            //debug($this->Session->read('User.profiles_id'));exit;
            if($this->Session->read('User.profiles_id')==9){
                $this->layout = 'invitado';
            }
            else{
                $this->layout = 'colaborador';
            }
            if (!$this->ChargeMassiveSampleCoffee->exists($id)) {
                    throw new NotFoundException(__('Invalid charge massive sample coffee'));
            }
            $options = array('conditions' => array('ChargeMassiveSampleCoffee.' . $this->ChargeMassiveSampleCoffee->primaryKey => $id));
            $chargeMassiveSampleCoffee = $this->ChargeMassiveSampleCoffee->find('first', $options);
            $this->set('chargeMassiveSampleCoffee', $this->ChargeMassiveSampleCoffee->find('first', $options));
            $optionCoffeeSample = array(
                        'limit' => 100,
                        'conditions' => array('CoffeeSample.id_charger'=>$id),
                        'order' => array('CoffeeSample.created_date' => 'DESC')
                    );
            $this->set('coffeeSamples', $this->CoffeeSample->find('all', $optionCoffeeSample));
            $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
            $dataUser = $this->User->find('first',array('conditions'=>array('User.id'=>$chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['user_reg'])));
            $this->set('typeSampleCoffes',$typeSampleCoffes);
            $this->set('dataUser',$dataUser);
	}
        
        
        /**
        * getTrackingRequest method
        *
        * @return void
        */
       public function getTrackingRequest($idRequest = null){
           if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;

           $bucket = 'samplecoffeecvs';
           $keyname = $idRequest;

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-east-2',
               'credentials' => [
               'key'    => "AKIAJZHFB4GIIF3FDWMA",
               'secret' => "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",
           ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__('No se encontro el archivo!'));
           }

       }

/**
 * add method
 *
 * @return void
 */
	public function add() {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            if($this->Session->read('User.profiles_id')==1){
                $this->layout = 'colaborador';
            }
            else if($this->Session->read('User.profiles_id')==9){
                $this->layout = 'invitado';
            }
            $this->loadModel('Client');
            $this->loadModel('CoffeeSample');
            $this->loadModel('SampleCoffeeHasTypeSample');
            $fileUploaded = null;
            if ($this->request->is('post')) {
                    $this->ChargeMassiveSampleCoffee->create();
                    $fileUploaded = $this->request->data['ChargeMassiveSampleCoffee']['name_file'];
                    $this->request->data['ChargeMassiveSampleCoffee']['name_file'] = $this->request->data['ChargeMassiveSampleCoffee']['name_file']['name'];
                    $this->request->data['ChargeMassiveSampleCoffee']['date_reg'] = date('Y-m-d H:i:s');
                    $this->request->data['ChargeMassiveSampleCoffee']['user_reg'] = $this->Session->read('User.id');
                    if ($this->ChargeMassiveSampleCoffee->save($this->request->data)) {
                        $contadorImpo=0;
		        $objPHPExcel = PHPExcel_IOFactory::load($fileUploaded['tmp_name']);
		        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		          $highestRow = $worksheet->getHighestRow(); // e.g. 10
			  $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			  $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
			  if($worksheet->getTitle() == 'Hoja1') {
			    for ($row = 3; $row <= $highestRow; ++ $row) {
			      $cellExpo = $worksheet->getCellByColumnAndRow(0, $row);
			      $codExpoChar = $cellExpo->getValue();
                              $dataClient = $this->Client->find('first',array('conditions' => array('Client.exporter_code like'=>"%".$codExpoChar."%")));
			      $cellLotRef = $worksheet->getCellByColumnAndRow(1, $row);
			      $cellQtaCoffee = $worksheet->getCellByColumnAndRow(2, $row);
			      $cellOperationCenter = $worksheet->getCellByColumnAndRow(3, $row);
			      $cellTerminal = $worksheet->getCellByColumnAndRow(4, $row);
			      $cellTypeSample = $worksheet->getCellByColumnAndRow(5, $row);
			      $cellObservation = $worksheet->getCellByColumnAndRow(6, $row);
			      $cellGetSample = $worksheet->getCellByColumnAndRow(7, $row);
                              $dataCoffeeSample = array(
                                  'qta_bag'=>$cellQtaCoffee->getValue(),
                                  'status'=>"REGISTRADA",
                                  'created_date'=>date('Y-m-d H:i:s'),
                                  'observation'=>$cellObservation->getValue(),
                                  'register_users_id'=>$this->Session->read('User.id'),
                                  'client_id'=>$dataClient['Client']['id'],
                                  'operation_center'=>$cellOperationCenter->getValue(),
                                  'terminal'=>$cellTerminal->getValue(),
                                  'lot_coffee_ref'=>preg_replace('/\s/', '', $dataClient['Client']['exporter_code'])."-".preg_replace('/\s/', '', $cellLotRef->getValue()),
                                  'id_charger'=>$this->ChargeMassiveSampleCoffee->id,
				  'get_sample'=>$cellGetSample->getValue()
                              );
                              $this->CoffeeSample->create();
			      if ($this->CoffeeSample->save($dataCoffeeSample)) {
                                  $arrayDataSampleCoffeeHasTypeSample = array(
                                                    'sample_coffee_id'=>$this->CoffeeSample->id,
                                                    'type_sample_coffee_id'=>$cellTypeSample->getValue());
                              $this->SampleCoffeeHasTypeSample->save($arrayDataSampleCoffeeHasTypeSample);
                              $contadorImpo++;
			      }
			    }
                           }
                        }
                        $bucket = 'samplecoffeecvs';
                        $keyname = $fileUploaded['name'];
                        $temp_file_location = $fileUploaded['tmp_name'];
                        $s3 = new S3Client([
                            'version' => 'latest',
                            'region'  => 'us-east-2',
                            'credentials' => [
                            'key'    => "AKIAJZHFB4GIIF3FDWMA",
                            'secret' => "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",]]);

                        $result = $s3->putObject([
                                'Bucket' => $bucket,
                                'Key'    => $keyname,
                                'SourceFile' => $temp_file_location
                        ]);
                        
                        $this->ChargeMassiveSampleCoffee->updateAll(
                                    array('count_insert'=>$contadorImpo),
                                    array('ChargeMassiveSampleCoffee.id'=>$this->ChargeMassiveSampleCoffee->id));
                        
                        $this->Flash->success(__('Cargue masivo realizado correctamente!.'));
                        return $this->redirect(array('action' => 'index',$this->Session->read('User.id'))); 
                    }
            }
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ChargeMassiveSampleCoffee->exists($id)) {
			throw new NotFoundException(__('Invalid charge massive sample coffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ChargeMassiveSampleCoffee->save($this->request->data)) {
				$this->Flash->success(__('The charge massive sample coffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The charge massive sample coffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ChargeMassiveSampleCoffee.' . $this->ChargeMassiveSampleCoffee->primaryKey => $id));
			$this->request->data = $this->ChargeMassiveSampleCoffee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ChargeMassiveSampleCoffee->id = $id;
		if (!$this->ChargeMassiveSampleCoffee->exists()) {
			throw new NotFoundException(__('Invalid charge massive sample coffee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ChargeMassiveSampleCoffee->delete()) {
			$this->Flash->success(__('The charge massive sample coffee has been deleted.'));
		} else {
			$this->Flash->error(__('The charge massive sample coffee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
