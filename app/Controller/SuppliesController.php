<?php
App::uses('AppController', 'Controller');
/**
 * Supplies Controller
 *
 * @property Supply $Supply
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SuppliesController extends AppController {


/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		$this->Supply->recursive = 0;
		$this->Paginator->settings = array(
			'limit' => 30,
			'conditions' => array('Supply.jetty' => $this->Session->read('User.jetty'))
		);
		$this->set('supplies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		if (!$this->Supply->exists($id)) {
			throw new NotFoundException(__('Invalid supply'));
		}
		$options = array('conditions' => array('Supply.' . $this->Supply->primaryKey => $id));
		$this->set('supply', $this->Supply->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
		$this->loadModel('Departaments');
        $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->Supply->create();
                        $this->request->data['Supply']['created_date']=date('Y-m-d H:i:s');
			if ($this->Supply->save($this->request->data)) {
				$this->Flash->success(__('Se registro correctamente el suministro'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The supply could not be saved. Please, try again.'));
			}
		}
		$dataDeparament = $this->Departaments->find('all',array('conditions'=>array('Departaments.cod_city'=>$this->Session->read('User.jetty'))));
		$listDeparaments;
		foreach ($dataDeparament as $key => $value) {
			$listDeparaments[$value['Departaments']['name']] = $value['Departaments']['name'] . " " ;
		}

        $this->set('portList',$listDeparaments);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		if (!$this->Supply->exists($id)) {
			throw new NotFoundException(__('Invalid supply'));
		}
		if ($this->request->is(array('post', 'put'))) {
                    $this->request->data['Supply']['updated_date']=date('Y-m-d H:i:s');
			if ($this->Supply->save($this->request->data)) {
				$this->Flash->success(__('Se registro correctamente el suministro.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The supply could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Supply.' . $this->Supply->primaryKey => $id));
			$this->request->data = $this->Supply->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		$this->Supply->id = $id;
		if (!$this->Supply->exists()) {
			throw new NotFoundException(__('Invalid supply'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Supply->delete()) {
			$this->Flash->success(__('The supply has been deleted.'));
		} else {
			$this->Flash->error(__('The supply could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
