<?php
App::import('Vendor','nusoap',array('file'=>'nusoap'.DS.'lib'.DS.'nusoap.php'));
App::uses('HttpSocket', 'Network/Http');

/**
 * Description of ShippingUtil
 *
 * @author asistentetic
 */
class ShippingUtil {
    //put your code here
    
    public function sd_SampleCoffee($arrayDataSend)
    {
        if($arrayDataSend != null){
            $shipping = $arrayDataSend['Shipping'];
            $operation = $arrayDataSend['Operation'];
            switch($shipping){
                case "SERVIENTREGA":
                    $options = array(
                    'uri'=>'http://schemas.xmlsoap.org/soap/envelope/',
                    'style'=>SOAP_RPC,
                    'use'=>SOAP_ENCODED,
                    'soap_version'=>SOAP_1_1,
                    'cache_wsdl'=>WSDL_CACHE_NONE,
                    'connection_timeout'=>15,
                    'trace'=>true,
                    'encoding'=>'UTF-8',
                    'exceptions'=>true,
                    );
                    switch ($operation){
                        case "SEND":
                            $url_WS = 'http://web.servientrega.com:8081/GeneracionGuias.asmx?WSDL';
                            $auth = array(
                                'login'=>'luis1937',
                                'pwd'=>'MZR0zNqnI/KplFlYXiFk7m8/G/Iqxb3O',
                                'Id_CodFacturacion'=>'SER408',
                                'Nombre_Cargue'=>'COPCSA'
                            );
                            $header = new SoapHeader('http://tempuri.org/','AuthHeader',$auth);
                            $client = new SoapClient($url_WS,$options);
                            $client->__setSoapHeaders($header);
                            $param3= "<ns1:CargueMasivoExterno>
                            <ns1:envios>
                               <ns1:CargueMasivoExternoDTO>
                                  <ns1:objEnvios>             
                                        <ns1:EnviosExterno>
                                        <ns1:Num_Guia>0</ns1:Num_Guia>
                                        <ns1:Num_Sobreporte>0</ns1:Num_Sobreporte>
                                        <ns1:Doc_Relacionado>?</ns1:Doc_Relacionado>
                                        <ns1:Num_Piezas>1</ns1:Num_Piezas>
                                        <ns1:Des_TipoTrayecto>1</ns1:Des_TipoTrayecto>
                                        <ns1:Ide_Producto>2</ns1:Ide_Producto><!--ENVÍO CON PRODCUTO INDUSTRIAL-->
                                        <ns1:Des_FormaPago>2</ns1:Des_FormaPago>
                                        <ns1:Des_MedioTransporte>1</ns1:Des_MedioTransporte>
                                        <ns1:Num_PesoTotal>3</ns1:Num_PesoTotal>
                                        <ns1:Num_ValorDeclaradoTotal>".$arrayDataSend['Num_ValorDeclaradoTotal']."</ns1:Num_ValorDeclaradoTotal>
                                        <ns1:Num_VolumenTotal>0</ns1:Num_VolumenTotal>
                                        <ns1:Num_BolsaSeguridad>0</ns1:Num_BolsaSeguridad>
                                        <ns1:Num_Precinto>0</ns1:Num_Precinto>
                                        <ns1:Des_TipoDuracionTrayecto>1</ns1:Des_TipoDuracionTrayecto>
                                        <ns1:Des_Telefono>".$arrayDataSend['Des_Telefono']."</ns1:Des_Telefono>
                                        <ns1:Des_Ciudad>".$arrayDataSend['Des_Ciudad']."</ns1:Des_Ciudad><!--o codigo dane para ciudad destino-->
                                        <ns1:Des_Direccion>".$arrayDataSend['Des_Direccion']."</ns1:Des_Direccion>
                                        <ns1:Nom_Contacto>".$arrayDataSend['Nom_Contacto']."</ns1:Nom_Contacto>
                                        <ns1:Des_VlrCampoPersonalizado1 />
                                        <ns1:Num_ValorLiquidado>0</ns1:Num_ValorLiquidado>
                                        <ns1:Des_DiceContener>TOMA DE MUESTRA DE CAFÉ</ns1:Des_DiceContener>
                                        <ns1:Des_TipoGuia>1</ns1:Des_TipoGuia>
                                        <ns1:Num_VlrSobreflete>0</ns1:Num_VlrSobreflete>
                                        <ns1:Num_VlrFlete>0</ns1:Num_VlrFlete>
                                        <ns1:Num_Descuento>0</ns1:Num_Descuento>
                                        <ns1:idePaisOrigen>1</ns1:idePaisOrigen>
                                        <ns1:idePaisDestino>1</ns1:idePaisDestino>
                                        <ns1:Des_IdArchivoOrigen>1</ns1:Des_IdArchivoOrigen>
                                        <ns1:Des_DireccionRemitente>".$arrayDataSend['Des_DireccionRemitente']."</ns1:Des_DireccionRemitente><!--Opcional-->
                                        <ns1:Num_PesoFacturado>0</ns1:Num_PesoFacturado>
                                        <ns1:Est_CanalMayorista>false</ns1:Est_CanalMayorista>
                                        <ns1:Num_IdentiRemitente />
                                        <ns1:Num_TelefonoRemitente />
                                        <ns1:Num_Alto>1</ns1:Num_Alto>
                                        <ns1:Num_Ancho>1</ns1:Num_Ancho>
                                        <ns1:Num_Largo>1</ns1:Num_Largo>
                                        <ns1:Des_DepartamentoDestino>".$arrayDataSend['Des_DepartamentoDestino']."</ns1:Des_DepartamentoDestino>
                                        <ns1:Des_DepartamentoOrigen>".$arrayDataSend['Des_DepartamentoOrigen']."</ns1:Des_DepartamentoOrigen>
                                        <ns1:Gen_Cajaporte>false</ns1:Gen_Cajaporte>
                                        <ns1:Gen_Sobreporte>false</ns1:Gen_Sobreporte>
                                        <ns1:Nom_UnidadEmpaque>GENERICA</ns1:Nom_UnidadEmpaque>
                                        <ns1:Nom_RemitenteCanal />
                                        <ns1:Des_UnidadLongitud>cm</ns1:Des_UnidadLongitud>
                                        <ns1:Des_UnidadPeso>kg</ns1:Des_UnidadPeso>
                                        <ns1:Num_ValorDeclaradoSobreTotal></ns1:Num_ValorDeclaradoSobreTotal>
                                        <ns1:Num_Factura>FACT-001</ns1:Num_Factura>
                                        <ns1:Des_CorreoElectronico>".$arrayDataSend['Des_CorreoElectronico']."</ns1:Des_CorreoElectronico>
                                        <ns1:Num_Recaudo>0</ns1:Num_Recaudo>
                                        <ns1:objEnviosUnidadEmpaqueCargue>
                                           <ns1:EnviosUnidadEmpaqueCargue>
                                              <ns1:Num_Alto>3</ns1:Num_Alto>
                                              <ns1:Num_Ancho>3</ns1:Num_Ancho>
                                              <ns1:Num_Cantidad>1</ns1:Num_Cantidad>
                                              <ns1:Des_DiceContener>TOMA DE MUESTRA DE CAFÉ</ns1:Des_DiceContener>
                                              <ns1:Des_IdArchivoOrigen>1</ns1:Des_IdArchivoOrigen>
                                              <ns1:Num_Largo>3</ns1:Num_Largo>
                                              <ns1:Nom_UnidadEmpaque>GENERICA</ns1:Nom_UnidadEmpaque>
                                              <ns1:Num_Peso>3</ns1:Num_Peso>
                                              <ns1:Des_UnidadLongitud>cm</ns1:Des_UnidadLongitud>
                                             <ns1:Des_UnidadPeso>kg</ns1:Des_UnidadPeso>
                                              <ns1:Num_Volumen>3</ns1:Num_Volumen>
                                              <ns1:Num_ValorDeclarado>".$arrayDataSend['Num_ValorDeclaradoTotal']."</ns1:Num_ValorDeclarado>
                                           </ns1:EnviosUnidadEmpaqueCargue>
                                        </ns1:objEnviosUnidadEmpaqueCargue>
                                     </ns1:EnviosExterno>
                                  </ns1:objEnvios>
                               </ns1:CargueMasivoExternoDTO>
                            </ns1:envios>
                            <ns1:arrayGuias />
                         </ns1:CargueMasivoExterno>";
                        $result = $client->CargueMasivoExterno(new SoapVar($param3, XSD_ANYXML));
//                        debug($param3);exit;
//                        debug($client->__getLastRequest());
//                        debug($client->__getLastResponse());
                        if($result){
                            return $result->arrayGuias->string;
                        }
                        break;
                        case "TICKET_PRINT":
                            $url_WS = 'http://web.servientrega.com:8081/GeneracionGuias.asmx?WSDL';
                            $auth = array(
                                'login'=>'luis1937',
                                'pwd'=>'MZR0zNqnI/KplFlYXiFk7m8/G/Iqxb3O',
                                'Id_CodFacturacion'=>'SER408',
                                'Nombre_Cargue'=>'COPCSA'
                            );
                            $header = new SoapHeader('http://tempuri.org/','AuthHeader',$auth);
                            $client = new SoapClient($url_WS,$options);
                            $client->__setSoapHeaders($header);
                            $param3="<ns1:GenerarGuiaSticker>
                                        <ns1:num_Guia>".$arrayDataSend['Tracking']."</ns1:num_Guia>
                                        <ns1:num_GuiaFinal>".$arrayDataSend['Tracking']."</ns1:num_GuiaFinal>
                                        <!--Optional:-->
                                        <ns1:ide_CodFacturacion>SER408</ns1:ide_CodFacturacion>
                                        <ns1:sFormatoImpresionGuia>2</ns1:sFormatoImpresionGuia>
                                        <!--Optional:-->
                                        <ns1:Id_ArchivoCargar>0</ns1:Id_ArchivoCargar>
                                        <ns1:interno>0</ns1:interno>
                                        <!--Optional:-->
                                        <ns1:bytesReport/>
                                    </ns1:GenerarGuiaSticker>";
                            
                            $result = $client->GenerarGuiaSticker(new SoapVar($param3, XSD_ANYXML));
                            $pdfCode = $result->bytesReport;
                            header('Content-Description: File Transfer');
                            header('Content-Type: application/octet-stream');
                            header('Content-Disposition: attachment; filename='.$arrayDataSend['Tracking'].".pdf");
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Length: '. strlen($pdfCode));
                            ob_clean();
                            flush();
                            echo $pdfCode;
                        break;
                    }
                    break;
                    case "DEPRISA":
                    switch ($operation){
                        case "SEND":
                                $param = "<ADMISIONES>
                                            <ADMISION>
                                                <GRABAR_ENVIO>S</GRABAR_ENVIO>
                                                <CODIGO_ADMISION>123</CODIGO_ADMISION>
                                                <NUMERO_ENVIO/>
                                                <NUMERO_BULTOS>1</NUMERO_BULTOS>
                                                <FECHA_HORA_ADMISION>".date("m/d/y H:i")."</FECHA_HORA_ADMISION>
                                                <CLIENTE_REMITENTE>00000037</CLIENTE_REMITENTE>
                                                <CENTRO_REMITENTE>47</CENTRO_REMITENTE>
                                                <NOMBRE_REMITENTE>COMPAÑIA OPERADORA PORTUARIA CAFETERA S.A</NOMBRE_REMITENTE>
                                                <DIRECCION_REMITENTE>".$arrayDataSend['Des_DireccionRemitente']."</DIRECCION_REMITENTE >
                                                <PAIS_REMITENTE>057</PAIS_REMITENTE>
                                                <CODIGO_POSTAL_REMITENTE></CODIGO_POSTAL_REMITENTE>
                                                <POBLACION_REMITENTE>".$arrayDataSend['Ori_Ciudad']."</POBLACION_REMITENTE>
                                                <TIPO_DOC_REMITENTE>NIT</TIPO_DOC_REMITENTE>
                                                <DOCUMENTO_IDENTIDAD_REMITENTE>800247852-1</DOCUMENTO_IDENTIDAD_REMITENTE>
                                                <PERSONA_CONTACTO_REMITENTE>Servicio al cliente - COPCSA</PERSONA_CONTACTO_REMITENTE>
                                                <TELEFONO_CONTACTO_REMITENTE>0922417428</TELEFONO_CONTACTO_REMITENTE>
                                                <DEPARTAMENTO_REMITENTE>".$arrayDataSend['Des_DepartamentoOrigen']."</DEPARTAMENTO_REMITENTE>
                                                <EMAIL_REMITENTE>".$arrayDataSend['Des_CorreoElectronico']."</EMAIL_REMITENTE>
                                                <CLIENTE_DESTINATARIO>99999999</CLIENTE_DESTINATARIO>
                                                <CENTRO_DESTINATARIO>99</CENTRO_DESTINATARIO>
                                                <NOMBRE_DESTINATARIO>".$arrayDataSend['Nom_Contacto']."</NOMBRE_DESTINATARIO>
                                                <DIRECCION_DESTINATARIO>".$arrayDataSend['Des_Direccion']."</DIRECCION_DESTINATARIO>
                                                <PAIS_DESTINATARIO>057</PAIS_DESTINATARIO>
                                                <CODIGO_POSTAL_DESTINATARIO>".$arrayDataSend['Des_codigo_postal']."</CODIGO_POSTAL_DESTINATARIO>
                                                <POBLACION_DESTINATARIO>".$arrayDataSend['Des_Ciudad']."</POBLACION_DESTINATARIO>
                                                <TIPO_DOC_DESTINATARIO>CC</TIPO_DOC_DESTINATARIO>
                                                <DOCUMENTO_IDENTIDAD_DESTINATARIO>73082468Z</DOCUMENTO_IDENTIDAD_DESTINATARIO>
                                                <PERSONA_CONTACTO_DESTINATARIO>".$arrayDataSend['Nom_Contacto']."</PERSONA_CONTACTO_DESTINATARIO >
                                                <TELEFONO_CONTACTO_DESTINATARIO>".$arrayDataSend['Des_Telefono']."</TELEFONO_CONTACTO_DESTINATARIO>
                                                <DEPARTAMENTO_DESTINATARIO>".$arrayDataSend['Des_DepartamentoDestino']."</DEPARTAMENTO_DESTINATARIO>
                                                <EMAIL_DESTINATARIO>".$arrayDataSend['Des_CorreoElectronico']."</EMAIL_DESTINATARIO>
                                                <INCOTERM>FCA</INCOTERM>
                                                <RAZON_EXPORTAR>01</RAZON_EXPORTAR>
                                                <EMBALAJE>EV</EMBALAJE>
                                                <CODIGO_SERVICIO>1000</CODIGO_SERVICIO>
                                                <KILOS>1</KILOS>
                                                <VOLUMEN>1</VOLUMEN>
                                                <LARGO>1</LARGO>
                                                <ANCHO>1</ANCHO>
                                                <ALTO>1</ALTO>
                                                <NUMERO_REFERENCIA>1234</NUMERO_REFERENCIA>
                                                <IMPORTE_REEMBOLSO>".$arrayDataSend['Num_ValorDeclaradoTotal']."</IMPORTE_REEMBOLSO>
                                                <IMPORTE_VALOR_DECLARADO>".$arrayDataSend['Num_ValorDeclaradoTotal']."</IMPORTE_VALOR_DECLARADO>
                                                <TIPO_PORTES>P</TIPO_PORTES>
                                                <OBSERVACIONES1>TOMA DE MUESTRA DE CAFÉ</OBSERVACIONES1>
                                                <OBSERVACIONES2>OBS2</OBSERVACIONES2>
                                                <TIPO_MERCANCIA>P</TIPO_MERCANCIA>
                                                <!--CRR_DESTINO>BOGTCG</CRR_DESTINO-->
                                                <ASEGURAR_ENVIO>S</ASEGURAR_ENVIO   >
                                                <TIPO_MONEDA>COP</TIPO_MONEDA>
                                                <BULTOS_ADMISION>
                                                  <BULTO>
                                                    <REFERENCIA_BULTO_CLIENTE>111111</REFERENCIA_BULTO_CLIENTE>
                                                    <TIPO_BULTO>1425</TIPO_BULTO>
                                                    <LARGO>19</LARGO>
                                                    <ANCHO>39</ANCHO>
                                                    <ALTO>29</ALTO>
                                                    <VOLUMEN>9</VOLUMEN>
                                                    <KILOS>1</KILOS>
                                                    <OBSERVACIONES>TOMA DE MUESTRA DE CAFÉ</OBSERVACIONES>
                                                    <CODIGO_BARRAS_CLIENTE>4534534534534</CODIGO_BARRAS_CLIENTE>
                                                  </BULTO>
                                                </BULTOS_ADMISION>
                                            </ADMISION>
                                        </ADMISIONES>";
                            $url = 'http://190.86.194.73:38080/conecta2/seam/resource/restv1/admision_envios';
                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/xml'
                            ));
                            curl_setopt($ch, CURLOPT_POST,1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS,$param); 
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            if( ! $result = curl_exec($ch)) 
                            { 
                                trigger_error(curl_error($ch)); 
                                echo "error!";
                                exit;
                            }
                            return ($result);
                        break;
                        case "TICKET_PRINT":
                            $param = "<ETIQUETAS>
                                    <ETIQUETA> <!-- Devuelve láser-->
                                        <NUMERO_ENVIO>".$arrayDataSend['Tracking']."</NUMERO_ENVIO>
                                        <TIPO_IMPRESORA>L</TIPO_IMPRESORA>
                                    </ETIQUETA>
                              </ETIQUETAS>
                              ";
                            $url = 'http://190.86.194.73:38080/conecta2/seam/resource/restv1/admision_envios/etiquetas';
                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/xml'
                            ));
                            curl_setopt($ch, CURLOPT_POST,1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS,$param); 
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            if( ! $result = curl_exec($ch)) 
                            { 
                                trigger_error(curl_error($ch)); 
                                echo "error!";
                                exit;
                            }
                            $resultxml = simplexml_load_string($result);
                            $admisionXml = $resultxml->RESPUESTA_ETIQUETAS;
                            $arrayEnvio = json_decode( json_encode($admisionXml) , 1);
                            $pdfBytes = $arrayEnvio['ETIQUETA'];
                            debug($pdfBytes);exit;
                            header('Content-Description: File Transfer');
                            header('Content-Type: application/pdf');
                            header('Content-Disposition: attachment; filename='.$arrayDataSend['Tracking'].".pdf");
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Pragma: public');
                            header('Content-Length: '. strlen($pdfBytes));
                            echo $pdfBytes;
                        break;
                }
                break;    
            }
        }
        else{
            return false;
        }
    }
}
