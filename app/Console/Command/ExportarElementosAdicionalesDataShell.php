
<?php

    App::uses('EmbalajesController', 'Controller');
    App::uses('EmbalajesController', 'Controller');
    App::uses('ExportersController', 'Controller');
    App::uses('TareRemesasController', 'Controller');
    App::uses('AdictionalElementsController', 'Controller');
    App::uses('TypeCtnsController', 'Controller');
    App::uses('TallyAdictionalInfosController', 'Controller');
    App::uses('TallyExposController', 'Controller');
    App::uses('TallyPackagingsController', 'Controller');
    App::uses('AdictionalElementsHasTallyPackagingsController', 'Controller');
    
    
class ExportarElementosAdicionalesDataShell extends AppShell {

      function exportDataPackaging(){
	      $connection_expocafe = "192.168.35.213";
	      $username = "sop_user";
	      $password = "123";
	      $dbname = "schema_siscafe";
	      $date_var = date("Y-m-d", strtotime('-1 day'));
	      //$date_var = date("2017-01-26");
	      echo $date_var."\n";
	      $conn_expocafe = new mysqli($connection_expocafe, $username, $password, $dbname);
	      $query1 = "SELECT tp.id FROM tally_packaging tp WHERE
			 tp.id IN (select te.seq_tally FROM tally_expo te WHERE te.packaging_date like '%".$date_var."%')";
			  if ($result1 = $conn_expocafe->query($query1)) {
			      if($result1->num_rows == 0) {
				      return;
			      }
			      while($obj1 = $result1->fetch_object()){
				      $idTally=$obj1->id;
				      echo $idTally."\n";
				      $this->updateElementosAdicionales($idTally);
			      }
			  }
	  }
	  
	  function updateElementosAdicionales($idTallyPackaging = null){
	    $AdictionalElementsHasTallyPackagingsController = new AdictionalElementsHasTallyPackagingsController;
	    $TallyExposController = new TallyExposController;
	    $allAdictionalElements = $AdictionalElementsHasTallyPackagingsController->findById($idTallyPackaging);
	    $TallyExpo = $TallyExposController->findByTally($idTallyPackaging);
	    $elementos="";
	    if($allAdictionalElements == null){
		$elementos = "Elementos adicionales";
	    }
	    else{
		
		  foreach($allAdictionalElements as $adictionalElement){
		    $elementos .= $adictionalElement['AdictionalElement']['name'];
		    $elementos .= " - CTA: ".$adictionalElement['AdictionalElementsHasTallyPackaging']['qty'];
		    if($adictionalElement['AdictionalElementsHasTallyPackaging']['unit'] == 0)
		      $elementos .= " - CAPA"." | ";
		    else if($adictionalElement['AdictionalElementsHasTallyPackaging']['unit'] == 1)
		    $elementos .= " - UNIDAD"." | ";
		    else if($adictionalElement['AdictionalElementsHasTallyPackaging']['unit'] == 2)
		      $elementos .= " - N/A"." | ";
		    }
	    }
	    $TallyExpo['TallyExpo']['adictional_elements'] = $elementos;
	    debug($TallyExpo);
	    $TallyExposController->TallyExpo->save($TallyExpo);
	}
	
	public function main() {
	    $this->exportDataPackaging();
	}

}
