-- MySQL dump 10.19  Distrib 10.3.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: 
-- ------------------------------------------------------
-- Server version	10.3.34-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `advannet`
--

USE `advannet`;

--
-- Dumping routines for database 'advannet'

--
-- Current Database: `siscafe_pro`
--

USE `siscafe_pro`;

--
-- Dumping routines for database 'siscafe_pro'
--
/*!50003 DROP PROCEDURE IF EXISTS `COFCO_SUPPLIES` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `COFCO_SUPPLIES`(IN date_start date , IN date_end date)
BEGIN

SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    ifn.tsl AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00104" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900411999" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "SUMINISTRO DE BOLSA JUMBO LINER COFCO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*ifn.hc_qta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000392'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000392','001') AND expo_id1=79
WHERE   vp.PUERTO = 'BUN' AND ifn.tsl!=0 AND vp.STATUS_ID IN (3,4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Descargue_Cafe_Nacional` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `Descargue_Cafe_Nacional`(IN date_start date , IN date_end date)
BEGIN
	SET @FECHA_INF= date_start;
	SET @FECHA_SUP= date_end;





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL SPB TIPO SACOS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000110','007')
where ss.ref_departament=1 and rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.client_id =1 AND rc.bill_id!=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (CEIL(rm.quantity_radicated_bag_in*0.32)) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME FNC COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(CEIL(rm.quantity_radicated_bag_in*0.32)) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','007')
where rm.jetty=1 and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL SPB TIPO CAJAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000285','007')
where ss.ref_departament=1 and rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.client_id =1 AND rc.bill_id=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SPB TIPO CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000316'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000316','007')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SPB TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','007')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SPB TIPO FIQUE MENOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000219','007')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 
























INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE MICROLOTES PARTICULARES EN TERMINAL SPB MAYOR IGUAL A 5 LOTES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join clients cts on cts.id=rc.client_id
inner join units_caffee uc on rc.units_cafee_id=uc.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000114','001')
where rc.microlot=1 and ss.ref_departament=1 and rc.client_id !=1 
     AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MENOR A 100 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000270','001')
where ss.ref_departament=1 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB DE 101 A 250 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000287','001')
where ss.ref_departament=1 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000222','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 30 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000223','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 35 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000224','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000111','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB SACOS CACAO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000049','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000184','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4  
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO MINI BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000347'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000347','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=19
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAJAS DE 1 A 10 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000032','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAJAS DE 11 A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000203','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) between 11 and 100      
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAJAS MAYOR A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000215','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAFE VERDE ESTIBAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000295",'001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=18 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO CAFE MAYOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000277','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 


INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO CAFE MENOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000001','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 


INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 


INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO FIQUE MENOR A 5000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000204','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO FIQUE MAYOR A 5001 Y MENOR 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000010','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE BUENAVENTURA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," BUENAVENTURA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','001')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 and tu.id!=9 
        AND (ss.ref_departament=1 OR ss.ref_departament=7) 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000213','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 OR tsc.id=7)
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000212','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')   
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000084','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','001')
WHERE  ivn.type_invoice= 1 AND pc.jetty='BUN' AND (cts.id!=6 and cts.id!=1 ) AND  
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','007')
WHERE  ivn.type_invoice= 1 AND pc.jetty='BUN' AND cts.id=1 AND  
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    (select cls.nit from clients cls where cls.id=ivn.id_expo_pilcaf) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD EIR (INSPECTION AT THE TIME OF RECEIPT) PARA EL TERMINAL DE SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000268'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000268','001')
WHERE  ivn.type_invoice= 2 AND pc.jetty='BUN' AND (cts.id!=1 AND cts.id!=6) AND
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR CARAVELA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000214','001')
WHERE rc.jetty=1 AND rc.client_id =29 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR CARCAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000086','001')
WHERE rc.jetty=1 AND rc.client_id =7 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    sum(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR INCONEXUS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000149','001')
WHERE rc.jetty=1 AND rc.client_id =61 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA BOLSAS HERMETICAS/CAJAS 101 EN ADELANTE PARTICULARES COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo="0000009"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000009",'001')
where rm.jetty=1 and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in > 100
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÈ EN BOLSAS HERMETICAS/CAJAS 31 A 100 PARTICULARES COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo="0000360"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000360",'001')
where rm.jetty=1 and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in between 31 and 100
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÈ EN BOLSAS HERMETICAS/CAJAS 1 A 30 PARTICULARES COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo="0000359"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000359",'001')
where rm.jetty=1 and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in < 31
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 
































INSERT INTO data_billing_full (
    SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL Cartagena TIPO SACOS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000110','012')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) and rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) and rc.client_id =1 AND rc.bill_id!=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (CEIL(rm.quantity_radicated_bag_in*0.32)) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME FNC COMPLETADA EN PUERTO DE CARTAGENA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(CEIL(rm.quantity_radicated_bag_in*0.32)) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','012')
where (rm.jetty=2 OR rm.jetty=4 OR rm.jetty=5 OR rm.jetty=6 OR rm.jetty=9) and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL CARTAGENA TIPO CAJAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000285','012')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9) and rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9) and rc.client_id =1 AND rc.bill_id=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL CARTAGENA TIPO CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000316'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000316','012')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9)  and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL CARTAGENA TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','012')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9) and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL CARTAGENA TIPO FIQUE MENOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000219','012')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9) and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 



















INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE MICROLOTES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR IGUAL A 5 LOTES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients AS cts on cts.id=rc.client_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join bill AS b_rc on rc.bill_id=b_rc.id
inner join slot_store AS ss on ss.id=rc.slot_store_id
inner join departaments AS dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000114','002')
where  (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
    AND rc.microlot=1 AND rc.client_id !=1 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MENOR A 100 SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients AS cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store AS ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments AS dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000270','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
    AND rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," DE 101 A 250 SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000287','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
    AND rc.client_id != 1 AND rc.microlot=0 AND rc.bill_id=1
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000222','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                AND rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 
                AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 30 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000223','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                    AND rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 
                    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 35 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000224','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                    AND rc.microlot=0 AND rc.client_id !=1 AND rc.bill_id=1 
                    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000111','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                    AND rc.microlot=0 AND rc.client_id !=1 AND rc.bill_id=1 
                    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO SACOS CACAO") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000049','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO BIG BAGS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000184','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO MINI BIG BAGS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000347'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000347','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=19
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS DE 1 A 10 UNIDADES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000032','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=2 AND rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS DE 11 A 100 UNIDADES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000203','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) between 11 and 100
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS MAYOR A 100 UNIDADES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000215','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100    
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in)  AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE VERDE ESTIBAS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000295",'002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=18 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 







INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE MAYOR A 250") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000277','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE MENOR A 250") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000001','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MAYOR A 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000127','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MENOR A 5000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000204','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MAYOR A 5001 Y MENOR 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000010','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000213','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 or tsc.id=7)
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000212','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000084','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA RGC COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo="0000211"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000211",'002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=1 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000054','002')
WHERE  ivn.type_invoice= 1 AND cts.id!=6 
    AND (pc.jetty LIKE 'CTG' OR pc.jetty LIKE 'SPRC' OR pc.jetty LIKE 'CONTECAR' OR pc.jetty LIKE 'BLOC' OR pc.jetty LIKE 'COMPAS' ) 
    AND (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL CARTAGENA DEL EXPORTADOR CARAVELA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000214','002')
WHERE  rc.client_id =29  AND ( rc.slot_store_id=320 OR rc.slot_store_id=322 OR rc.slot_store_id=334 OR rc.slot_store_id=319 ) 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id 
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL CARTAGENA DEL EXPORTADOR CARCAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000086','002')
WHERE rc.client_id=7 AND ( rc.slot_store_id=320 OR rc.slot_store_id=322 OR rc.slot_store_id=334 OR rc.slot_store_id=319 ) 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id 
ORDER BY rc.created_date ASC 
); 



INSERT INTO data_billing_full (
SELECT 
   NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    sum(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL CARTAGENA DEL EXPORTADOR INCONEXUS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000149','002')
WHERE rc.client_id=61 AND ( rc.slot_store_id=320 OR rc.slot_store_id=322 OR rc.slot_store_id=334 OR rc.slot_store_id=319 )
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÉ EN BOLSAS HERMETICAS/CAJAS 101 EN ADELANTE PARTICULARES COMPLETADA EN PUERTO DE CARTAGENA TERMINAL CTG") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000009'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000009','002')
where (rm.jetty=2 OR  rm.jetty=4 OR rm.jetty=5 OR rm.jetty=6 OR rm.jetty=9) and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in > 100
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÈ EN BOLSAS HERMETICAS/CAJAS 31 A 100 PARTICULARES COMPLETADA EN PUERTO DE CARTAGENA TERMINAL CTG") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000360'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000360','002')
where (rm.jetty=2 OR  rm.jetty=4 OR rm.jetty=5 OR rm.jetty=6 OR rm.jetty=9) and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in between 31 and 100
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÈ EN BOLSAS HERMETICAS/CAJAS 1 A 30 PARTICULARES COMPLETADA EN PUERTO DE CARTAGENA TERMINAL CTG") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000359'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000359','002')
where (rm.jetty=2 OR  rm.jetty=4 OR rm.jetty=5 OR rm.jetty=6 OR rm.jetty=9) and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in < 31
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE CARTAGENA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," CARTAGENA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','002')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 and tu.id!=9 
        AND (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=9) 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 




























INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE AGUADULCE (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," AGUADULCE (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','001')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 AND ss.ref_departament=8  and tu.id!=9 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 







































INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL SMITCO TIPO SACOS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000110','010')
where ss.ref_departament=3 and rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.client_id =1 AND rc.bill_id!=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (CEIL(rm.quantity_radicated_bag_in*0.32)) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME FNC COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(CEIL(rm.quantity_radicated_bag_in*0.32)) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','010')
where rm.jetty=3 and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL SMITCO TIPO CAJAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000285','010')
where ss.ref_departament=3 and rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.client_id =1 AND rc.bill_id=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SMITCO TIPO CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000316'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000316','010')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SMITCO TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','010')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SMITCO TIPO FIQUE MENOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000219','010')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 





















INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE MICROLOTES PARTICULARES EN TERMINAL SANTA MARTA MAYOR IGUAL A 5 LOTES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join clients cts on cts.id=rc.client_id
inner join units_caffee uc on rc.units_cafee_id=uc.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000114','003')
where rc.microlot=1 and ss.ref_departament=3 and rc.client_id !=1 
     AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MENOR A 100 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000270','003')
where ss.ref_departament=3 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA DE 101 A 250 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000287','003')
where ss.ref_departament=3 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000222','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 30 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000223','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 35 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000224','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000111','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA SACOS CACAO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000049','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000184','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO MINI BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000347'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000347','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=19
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAJAS DE 1 A 10 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000032','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAJAS DE 11 A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000203','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) between 11 and 100
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAJAS MAYOR A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000215','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100    
ORDER BY cts.exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAFE VERDE ESTIBAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000295",'003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=18
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO CAFE MAYOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000277','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO CAFE MENOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000001','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000127','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO FIQUE MENOR A 5000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000204','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO FIQUE MAYOR A 5001 Y MENOR 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000010','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000213','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 or tsc.id=7)
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000212','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000084','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(vp2.LOTES separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    vp2.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
inner join view_packaging2 AS vp2 on vp2.OIE = ivn.id_tracking_siscafe
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000054','003')
WHERE  ivn.type_invoice= 1 AND cts.id!=6  AND (pc.jetty='stm' OR pc.jetty='sta')
     AND (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,date(ivn.date_reg), pc.jetty
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SANTA MARTA DEL EXPORTADOR CARAVELA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000214','003')
WHERE rc.jetty=3 AND rc.client_id =29 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SANTA MARTA DEL EXPORTADOR CARCAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000086','003')
WHERE rc.jetty=3 AND rc.client_id =7 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    sum(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SANTA MARTA DEL EXPORTADOR INCONEXUS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000149','003')
where rc.jetty=3 AND rc.client_id =61 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC  
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÉ EN BOLSAS HERMETICAS/CAJAS 101 EN ADELANTE PARTICULARES COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL SMITCO") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000009'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000009','003')
where rm.jetty=3 and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in > 100
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÈ EN BOLSAS HERMETICAS/CAJAS 31 A 100 PARTICULARES COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL SMITCO") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000360'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000360','003')
where rm.jetty=3 and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in between 31 and 100
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA DE CAFÈ EN BOLSAS HERMETICAS/CAJAS 1 A 30 PARTICULARES COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL SMITCO") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000359'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000359','003')
where rm.jetty=3 and tu.id=4 AND rm.client_id!=1 AND rm.quantity_radicated_bag_in < 31
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE SANTA MARTA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," SANTA MARTA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','003')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 AND ss.ref_departament=3  and tu.id!=9 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 







	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Facturacion_Embalaje_Sacos_STA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `Facturacion_Embalaje_Sacos_STA`(IN date_start date , IN date_end date)
BEGIN

SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;


    

INSERT INTO data_billing_full (
SELECT 
    NULL AS ID, 
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO SACOS DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000029'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000029','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  and vp.TIPO_EMBALAJE ="FCL"
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO SACOS DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000077'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000077','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 







INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    vp.adictional_layer_kraft_20 AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO KRAFT DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_kraft_20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000026'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000026','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_kraft_20 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    vp.adictional_layer_carton_20 AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CARTON DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_carton_20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000075'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000075','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_carton_20 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    vp.adictional_layer_kraft_40 AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO KRAFT DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_kraft_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000282'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000282','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_kraft_40 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    vp.adictional_layer_carton_40 AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CARTON DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_carton_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000072'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000072','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_carton_40 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("COLOCACION DE BOLSAS TSL ADICIONAL AL CONTENEDOR EN PUERTO SANTA MARTA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000254'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000254','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND vp.tsl != 0  AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("COLOCACION DE BOLSAS TSL CON DESECANTES ADICIONAL AL CONTENEDOR EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000284'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000284','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND vp.tls_desiccant != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.hc_qta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("INSTALACION HC ADICIONAL AL CONTENEDOR EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000271'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000271','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND vp.hc_qta != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("INSTALACION SELLOS CONTENEDOR SACOS EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000019'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000019','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') and vp.TIPO_EMBALAJE ="FCL"
        AND vp.seals != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("VACIADOS DE CONTENEDOR EN PUERTO DE SANTA MARTA TIPO SACOS DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000044'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000044','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY vp.EXPORTADOR ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("VACIADOS DE CONTENEDOR EN PUERTO DE SANTA MARTA TIPO SACOS DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000081'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000081','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY vp.EXPORTADOR ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_dry_bags AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("INSTALACION DRY BAGS SACOS AL CONTENEDOR EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_dry_bags AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000051'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000051','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')
        AND vp.adictional_dry_bags != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CACAO DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000050'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000050','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CACAO DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000074'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000074','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID, 
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CAJAS DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000041'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000041','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  
        AND vp.MODALIDAD LIKE 'CAJA%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CAJAS DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000041'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000041','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'CAJA%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Facturacion_Mensual_MITSUBISHI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `Facturacion_Mensual_MITSUBISHI`(IN date_start date , IN date_end date)
BEGIN

SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;







INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860001543" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA MITSUBISHI COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000351'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000351','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=20)  AND cs.qta_takes_sample=31
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860001543" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA MITSUBISHI COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000351'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000351','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=20) AND cs.qta_takes_sample=31
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860001543" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA MITSUBISHI COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000351'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000351','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=20) AND cs.qta_takes_sample=31
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 










INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860001543" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA MITSUBISHI COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000352'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000352','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=20)  AND cs.qta_takes_sample=100
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860001543" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA MITSUBISHI COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000352'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000352','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=20) AND cs.qta_takes_sample=100
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860001543" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA MITSUBISHI COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000352'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000352','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=20) AND cs.qta_takes_sample=100
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Facturacion_Mensual_MITSUI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `Facturacion_Mensual_MITSUI`(IN date_start date , IN date_end date)
BEGIN

SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;


INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900342832" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA MITSUI COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000210'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000210','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=2 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900342832" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA MITSUI COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000210'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000210','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=2 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900342832" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA MITSUI COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000210'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000210','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=2 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Fumigacion_Por_Lote` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `Fumigacion_Por_Lote`(IN date_start date , IN date_end date)
BEGIN

SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA PARTICULARES HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);















INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726)  AND fs.modality="PUERTO" 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726)   AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726)   AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726)  AND fs.modality="PUERTO" 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA SUCDEN HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA SUCDEN DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id =726)  AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPB MODALIDAD OTRO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000380'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000380','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND (rm.client_id =726) AND fs.modality="OTRO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 0
order by fs.id DESC
);













INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000387'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000387','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id =1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB CON PRESENCIA ICA " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000319'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000319','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FNC FUMIGACION ICA FNC HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FNC FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);

























































INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 









INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA PARTICULARES HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);












INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA SIN PRESENCIA ICA DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 









INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA SUCDEN HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA SUCDEN DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);








INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE SPIA MODALIDAD OTRO " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000380'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000380','001')
WHERE (fs.departament_id=8) AND fs.completed=1  AND (rm.client_id =726) AND fs.modality="OTRO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 0
order by fs.id DESC
); 







 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000387'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000387','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id =1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA CON PRESENCIA ICA " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000319'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000319','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FNC FUMIGACION ICA PARTICULARES HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FNC FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);





INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);






























INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00102")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00102")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);









INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002"AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00202"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00202"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);







INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003"AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA, 
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301"  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);

















INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    (select group_concat(sos.observation separator " | ") from services_orders_supplies sos where sos.id_services_order=so.id) AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00106","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id != 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    (select group_concat(sos.observation separator " | ") from services_orders_supplies sos where sos.id_services_order=so.id) AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00106","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id = 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    (select group_concat(sos.observation separator " | ") from services_orders_supplies sos where sos.id_services_order=so.id) AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00202" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id != 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    (select group_concat(sos.observation separator " | ") from services_orders_supplies sos where sos.id_services_order=so.id) AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00202" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id = 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    (select group_concat(sos.observation separator " | ") from services_orders_supplies sos where sos.id_services_order=so.id) AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id != 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    (select group_concat(sos.observation separator " | ") from services_orders_supplies sos where sos.id_services_order=so.id) AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id = 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);


























INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9)
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id !=1 and rm.client_id !=726)     
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR CTG DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id !=1 and rm.client_id !=726) 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);








INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO" 
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9)
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND (rm.client_id =726) AND fs.modality="PUERTO"   
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 



INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA SUCDEN CTG DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND (rm.client_id =726) AND fs.modality="PUERTO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);







INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA SUCDEN EN TERMINAL DE CTG MODALID OTRO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000380'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000380','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) 
      AND fs.completed=1 AND (rm.client_id =726) AND fs.modality="OTRO"
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 0
order by fs.id DESC
); 







 


INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG SIN PRESENCIA ICA " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000387'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000387','012')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id =1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
order by fs.id DESC
); 




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','012')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG CON PRESENCIA ICA " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000319'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000319','012')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id =1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
order by fs.id DESC
); 






INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','012')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC CTG DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','012')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id =1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);








 


SET SQL_SAFE_UPDATES=0;
UPDATE `siscafe_pro`.`data_billing_full` SET `BODEGA` = '00202' where ITEM_UNOERP IN (000017,0000183,0000273,0000051,0000021,0000069,0000362,0000364,0000361,0000342,0000341) and CO_DOC=002 and BODEGA=00201;
UPDATE `siscafe_pro`.`data_billing_full` SET `BODEGA` = '00104' where ITEM_UNOERP IN (000017,0000183,0000273,0000051,0000021,0000069,0000362,0000364,0000361,0000342,0000341) and CO_DOC=001 and BODEGA=00102;
UPDATE `siscafe_pro`.`data_billing_full` SET `BODEGA` = '00106' where ITEM_UNOERP IN (000017,0000183,0000273,0000051,0000021,0000069,0000362,0000364,0000361,0000342,0000341) and CO_MOV=004 and BODEGA=00105;
SET SQL_SAFE_UPDATES=1;






END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `generateItemsToConexos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `generateItemsToConexos`()
BEGIN





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE, 
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 OR lp.id_lista=010 OR lp.id_lista=012, 1, 0) AS IS_FNC,
    IF(lp.id_lista=003 OR lp.id_lista=010, "003", 
        IF(lp.id_lista=002 OR lp.id_lista=012, "002", "001")
    ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000272
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    20 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000206 AND lp.id_lista!=007
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    21 AS RANGO_CANTIDAD_INICIAL,
    250 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000153 AND lp.id_lista!=007
); 








INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    250 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000278 AND lp.id_lista!=007
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000076 AND lp.id_lista!=007
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    11 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000208 AND lp.id_lista!=007
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    50 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000038 AND lp.id_lista!=007
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    51 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000231 AND lp.id_lista!=007
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000057 AND lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    1 AS IS_FNC,
    "001" AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000057 AND lp.id_lista=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000021 and (lp.id_lista!=007 and lp.id_lista!=012)
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista="007" OR lp.id_lista="012" OR lp.id_lista="010", 1,0) AS IS_FNC,
    if(lp.id_lista="001" OR lp.id_lista="002" OR lp.id_lista="003",lp.id_lista,
        IF(lp.id_lista="007","001",IF(lp.id_lista="010","003",IF(lp.id_lista="012","002",0)))
        ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000069
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    20 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000036
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    21 AS RANGO_CANTIDAD_INICIAL,
    75 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000209
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    76 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000218
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista="007" OR lp.id_lista="012" OR lp.id_lista="010", 1,0) AS IS_FNC,
    if(lp.id_lista="001" OR lp.id_lista="002" OR lp.id_lista="003",lp.id_lista,
        IF(lp.id_lista="007","001",IF(lp.id_lista="010","003",IF(lp.id_lista="012","002","001")))
        ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000039
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista="007" OR lp.id_lista="012" OR lp.id_lista="010", 1,0) AS IS_FNC,
    if(lp.id_lista="001" OR lp.id_lista="002" OR lp.id_lista="003",lp.id_lista,
        IF(lp.id_lista="007","001",IF(lp.id_lista="010","003",IF(lp.id_lista="012","002","001")))
        ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000034
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista="007" OR lp.id_lista="012" OR lp.id_lista="010", 1,0) AS IS_FNC,
    if(lp.id_lista="001" OR lp.id_lista="002" OR lp.id_lista="003",lp.id_lista,
        IF(lp.id_lista="007","001",IF(lp.id_lista="010","003",IF(lp.id_lista="012","002","001")))
        ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000033
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000037 AND lp.id_lista!=007
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    11 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000205 and lp.id_lista!=007
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    101 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000009 and lp.id_lista!=007
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    31 AS RANGO_CANTIDAD_INICIAL,
    100 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000360 and lp.id_lista!=007
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    30 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000359 and lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    20 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000126 AND  lp.id_lista!=007
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    21 AS RANGO_CANTIDAD_INICIAL,
    250 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000207 and (lp.id_lista!=007 and lp.id_lista!=010 and lp.id_lista!=012)
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    250 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000279 and (lp.id_lista!=007 and lp.id_lista!=010 and lp.id_lista!=012)
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    5000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000204 and lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    5001 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000010 and lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    10000 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 OR lp.id_lista=010 OR lp.id_lista=012 ,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",
        IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000127
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 OR lp.id_lista=010 OR lp.id_lista=012 ,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",
        IF(lp.id_lista=010 OR lp.id_lista=009,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000006
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=004,0,lp.id_lista)) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000051
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000032 AND lp.id_lista!=007
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    11 AS RANGO_CANTIDAD_INICIAL,
    100 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    IF(lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000203 AND lp.id_lista!=007
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    101 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=006 OR lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000215 and lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    250 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    lp.id_lista AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000001 AND lp.id_lista!=007
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    251 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000277 AND lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",if(lp.id_lista=004,0,lp.id_lista)) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000183
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",if(lp.id_lista=004,0,lp.id_lista)) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000363
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=006 or lp.id_lista=007,"001",if(lp.id_lista=010,"003",if(lp.id_lista=012,"002",if(lp.id_lista=004,"0",lp.id_lista)))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000019
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=012,"002",lp.id_lista) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000122
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=006 or lp.id_lista=007,"001",if(lp.id_lista=010,"003",if(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000065
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=006 or lp.id_lista=007,"001",if(lp.id_lista=010,"003",if(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000112
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    20 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=006 or lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000280 and (lp.id_lista!=006 and lp.id_lista!=007)
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    21 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    "001" AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000281 and  lp.id_lista=006
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    21 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000281 and  (lp.id_lista!=006 and lp.id_lista!=007)
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000146
); 










INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    0 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000145
); 








INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    1 AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000320
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    1 AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000220
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000035
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000321
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000066
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000285
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000316 and (lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012)
);  





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007 OR lp.id_lista=006 OR lp.id_lista=004 OR lp.id_lista=013,"001",IF(lp.id_lista=014,"002",IF(lp.id_lista=015,"003",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000017 AND lp.id_lista != 004
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000336
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006 or lp.id_lista=004,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000118
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 OR lp.id_lista=006 OR lp.id_lista=004 OR lp.id_lista=013,"001",IF(lp.id_lista=014,"002",IF(lp.id_lista=015,"003",lp.id_lista))) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000273 
); 







INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000067  
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000343  
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000344  
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000345  
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000346  

); 








INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000219 AND (lp.id_lista=003 or lp.id_lista=010) 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000317 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000022 
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,  
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010 ,"003",IF(lp.id_lista=012 ,"002",lp.id_lista)) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000054 
); 










INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    20 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000348  

); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    21 AS RANGO_CANTIDAD_INICIAL,
    75 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000349  

); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    75 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000350 

); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    "002" AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000353 

); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    0 AS IS_FNC,
    "001" AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000091 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    2 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000355 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    3 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000356 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000361 
); 



INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010 ,"003",IF(lp.id_lista=012 ,"002",lp.id_lista)) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000341 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000362 and lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010 ,"003",IF(lp.id_lista=012 ,"002",lp.id_lista)) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000342 and lp.id_lista!=007
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000364 and lp.id_lista!=007
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000366 
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000367 
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    2 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000368
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    3 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000369
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000370
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000371
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000372
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000373
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000374
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000375
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000376
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    50 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000377
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    10 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000378
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    3 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000313
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    3 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000312
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    2 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000217
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    2 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    0 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000216
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000045
); 








INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    2 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000271
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000381
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000382
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000058 and lp.id_lista=012
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000383
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000384
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000385
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000386
); 




INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000387
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000319
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000388
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000083
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000082
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000053
); 





INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",IF(lp.id_lista=010,"003",IF(lp.id_lista=012,"002",lp.id_lista) ) ) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000024 and lp.id_lista=012
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000390 
); 






INSERT INTO items_unoee_conexos (
SELECT
    NULL AS id,
    itu.codigo AS ITEMS_UNOEE,      
    itu.descripcion AS DESCRIPCION_ITEM, 
    lp.id_lista AS LISTA_PRECIO,
    lp.descripcion_lista AS DESCRIPCION_LISTA_PRECIO,
    1 AS RANGO_CANTIDAD_INICIAL,
    100000 AS RANGO_CANTIDAD_FINAL,   
    lp.precio AS TARIFA,
    itu.des_grupo_impositivo AS GRUPO_IMPOSITIVO,
    lp.unidad_medida AS UNIDAD_MEDIDA,
    itu.tipo_invent AS TIPO_INVENTARIO,
    itu.tipo_servicio AS TIPO_SERVICIO,
    1 AS UNIDAD_FACTURACION,
    1 AS USE_PILCAF,
    IF(lp.id_lista=007 or lp.id_lista=010 or lp.id_lista=012,1,0) AS IS_FNC,
    IF(lp.id_lista=007 or lp.id_lista=006,"001",lp.id_lista) AS CENTRO_OPERATION
FROM items_unoee AS itu INNER JOIN list_prices_unoee AS lp ON  itu.codigo=lp.referencia
WHERE itu.codigo=0000391 
); 











SET SQL_SAFE_UPDATES=0;
UPDATE items_unoee_conexos SET IS_FNC =1 WHERE ( LISTA_PRECIO=007 or LISTA_PRECIO=010 or LISTA_PRECIO=012);
UPDATE items_unoee_conexos SET CENTRO_OPERATION ='001' WHERE ( LISTA_PRECIO=001 or LISTA_PRECIO=006 OR LISTA_PRECIO=007 );
UPDATE items_unoee_conexos SET CENTRO_OPERATION ='002' WHERE ( LISTA_PRECIO=002 or LISTA_PRECIO=012 );
UPDATE items_unoee_conexos SET CENTRO_OPERATION ='003' WHERE ( LISTA_PRECIO=003 or LISTA_PRECIO=009 or LISTA_PRECIO=010 );
UPDATE items_unoee_conexos SET DESCRIPCION_ITEM = CONCAT(DESCRIPCION_ITEM," - SPB") WHERE ( LISTA_PRECIO=001 OR LISTA_PRECIO=007);
UPDATE items_unoee_conexos SET DESCRIPCION_ITEM = CONCAT(DESCRIPCION_ITEM," - SPIA") WHERE ( LISTA_PRECIO=006);
UPDATE items_unoee_conexos SET DESCRIPCION_ITEM = CONCAT(DESCRIPCION_ITEM," - CTG") WHERE ( LISTA_PRECIO=002 or LISTA_PRECIO=012 );
UPDATE items_unoee_conexos SET DESCRIPCION_ITEM = CONCAT(DESCRIPCION_ITEM," - STM") WHERE ( LISTA_PRECIO=003 or LISTA_PRECIO=009 or LISTA_PRECIO=010 );
SET SQL_SAFE_UPDATES=1;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MAIN_BILLING_PROCEDURES` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `MAIN_BILLING_PROCEDURES`()
BEGIN

   DECLARE num INT;
   SET num = 1;
   SET @COUNT = ( SELECT COUNT(id ) FROM billing_sql_schedule WHERE date(date_execute)=date(now()) and status_id=1 );

   WHILE num  <= @COUNT DO

		SET @id =( SELECT id FROM billing_sql_schedule WHERE date(date_execute)=date(now()) and status_id=1  limit 1 );    
		SET @type_query_sql =( SELECT type_query_sql_id FROM billing_sql_schedule WHERE date(date_execute) = date(now()) and status_id=1  limit 1 );    
		SET @date_start = ( SELECT date(date_start) FROM billing_sql_schedule WHERE date(date_execute)=date(now()) and status_id=1 limit 1 );
		SET @date_end = ( SELECT date(date_end) FROM billing_sql_schedule  WHERE date(date_execute)=date(now()) and status_id=1  limit 1 );
		
		truncate data_billing_full;		

		CASE @type_query_sql
			WHEN 1 THEN
				CALL Descargue_Cafe_Nacional(@date_start,@date_end);
				CALL Fumigacion_Por_Lote(@date_start,@date_end);
				CALL TRAZABILIDAD_RACAFE(@date_start,@date_end);
				CALL Facturacion_Embalaje_Sacos_STA(@date_start,@date_end);				
                CALL siscafe_allRows;
				UPDATE billing_sql_schedule SET status_id = 3, color = "#00A2FF", titulo = concat(titulo," (FACTURACIÓN COMPLETADA)") WHERE (id = @id);
			WHEN 2 THEN
				CALL Facturacion_Mensual_MITSUI(@date_start,@date_end);
                CALL siscafe_allRows;
				UPDATE billing_sql_schedule SET status_id = 3, color = "#00A2FF", titulo = concat(titulo," (FACTURACIÓN COMPLETADA)") WHERE (id = @id);
			WHEN 3 THEN
				CALL Facturacion_Mensual_MITSUBISHI(@date_start,@date_end);
				CALL Mantas_VDry(@date_start,@date_end);
                CALL siscafe_allRows;
				UPDATE billing_sql_schedule SET status_id = 3, color = "#00A2FF", titulo = concat(titulo," (FACTURACIÓN COMPLETADA)") WHERE (id = @id);
		
        END CASE; 
 
   SET  num = num + 1;
   END WHILE;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Mantas_VDry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `Mantas_VDry`(IN date_start date , IN date_end date)
BEGIN

SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    manta_termica AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE SPB" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*manta_termica AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'BUN' AND (vp.estado_embalaje="EMBALAJE COMPLETADO" or vp.estado_embalaje="EMBALAJE VACIADO")   AND vp.manta_termica != 0
ORDER BY vp.expotador_code ASC

); 





INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE SPB DE ISO 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.mantas_vdry_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'BUN' AND (vp.estado_embalaje="EMBALAJE COMPLETADO" or vp.estado_embalaje="EMBALAJE VACIADO")  AND vp.mantas_vdry_40 != 0
ORDER BY vp.expotador_code ASC

); 



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE CARTAGENA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO like '%CONTECA%' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND ifn.mantas_vdry_40!=0 AND vp.STATUS_ID IN (3,4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE CARTAGENA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO like '%CONTECA%' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND ifn.adictional_num_manta!=0 AND vp.STATUS_ID IN (3,4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','001')
WHERE   vp.PUERTO = 'SPIA' AND  ifn.adictional_num_manta!=0 AND vp.STATUS_ID IN (3,4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE STM" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','003')
WHERE   vp.PUERTO = 'STM' AND  ifn.adictional_num_manta!=0 AND vp.STATUS_ID IN (3,4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 






INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE STM" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','003')
WHERE   vp.PUERTO = 'STM' AND ifn.mantas_vdry_40!=0 AND vp.STATUS_ID IN (3,4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `siscafe_allRows` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `siscafe_allRows`()
begin

		insert temp_log_data_unoerp select 
        NULL,
        REMESA,
        COD_EXPORTADOR_SISCAFE,
        CODIGO_EXPORTADOR,
        NOMBRE_EXPORTADOR,
        LOTE,
        CANTIDAD_OPERATIVA,
        TIPO_SACOS,
        MODALIDAD,
        PLACA_VEHICULO,
        FECHA_OPERACION,
        UNIDAD_CAFE,
        UNIDAD_FACTURA,
        NUM_MICRO_LOTES,
        OIE,
        CONTENEDOR,
        ISOCTN,
        TIPO_PAPEL,
        CAPAS_ADICIONALES_KRAFT_20,
        CAPAS_ADICIONALES_CARTON_20,
        CAPAS_ADICIONALES_KRAFT_40,
        CAPAS_ADICIONALES_CARTON_40,
        ESTADO,
        MUESTRA,
        EMPRESA_SUMINISTRO,
        TIPO_ENVIO,
        TIPO_DOC,
        CO_DOC,
        CO_MOV,
		CCOSTO_MOV,
        UND_ITEM,
        SUCURSAL,
        BODEGA,
        TIPO_MUESTRA,
        IP_CONEXION,
        CLIENTE_UNOERP,
		cts.business_name as CLIENTE_NOMBRE_UNOERP,
        ITEM_UNOERP,
        NUM_FACTURA,
        LISTA_PRECIO,
        DESC_CONSULTA,
        DESCRIPCION_PRODUCTTO_UNOEE,
        VALOR_FACTURA_UNOEE,
        VALOR_UNIDAD,
		FECHA_CORTE,
        STATUS_TX_WS,
        FECHA_TX_WS
        from data_billing_full temp
		inner join clients cts on cts.nit=temp.CLIENTE_UNOERP;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `siscafe_bigClients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `siscafe_bigClients`()
begin
	declare totalRows int(11);
	
	set totalRows = (select count(*) from data_billing_full);
    
    if (totalRows != 0) then
		insert temp_log_data_unoerp select 
        NULL,
        REMESA,
        COD_EXPORTADOR_SISCAFE,
        CODIGO_EXPORTADOR,
        NOMBRE_EXPORTADOR,
        LOTE,
        CANTIDAD_OPERATIVA,
        TIPO_SACOS,
        MODALIDAD,
        PLACA_VEHICULO,
        FECHA_OPERACION,
        UNIDAD_CAFE,
        UNIDAD_FACTURA,
        NUM_MICRO_LOTES,
        OIE,
        CONTENEDOR,
        ISOCTN,
        TIPO_PAPEL,
        CAPAS_ADICIONALES_KRAFT_20,
        CAPAS_ADICIONALES_CARTON_20,
        CAPAS_ADICIONALES_KRAFT_40,
        CAPAS_ADICIONALES_CARTON_40,
        ESTADO,
        MUESTRA,
        EMPRESA_SUMINISTRO,
        TIPO_ENVIO,
        TIPO_DOC,
        CO_DOC,
        CO_MOV,
		CCOSTO_MOV,
        UND_ITEM,
        SUCURSAL,
        BODEGA,
        TIPO_MUESTRA,
        IP_CONEXION,
        CLIENTE_UNOERP,
		cts.business_name as CLIENTE_NOMBRE_UNOERP,
        ITEM_UNOERP,
        NUM_FACTURA,
        LISTA_PRECIO,
        DESC_CONSULTA,
        DESCRIPCION_PRODUCTTO_UNOEE,
        VALOR_FACTURA_UNOEE,
        VALOR_UNIDAD,
		FECHA_CORTE,
        STATUS_TX_WS,
        FECHA_TX_WS
        from data_billing_full temp
		inner join clients cts on cts.nit=temp.CLIENTE_UNOERP
        where cts.isBig=1 and cts.billing_manual=0 and cts.billing_dayly=0;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `siscafe_copyTableTmpToFull` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `siscafe_copyTableTmpToFull`()
begin
	declare totalRows int(11);
    declare totalMovBill int(11);
	
	set totalRows = (select count(*) from temp_log_data_unoerp);
	set totalMovBill = (select count(*) from temp_log_data_unoerp where NUM_FACTURA is not null);
    
    if (totalRows != 0 && totalRows = totalMovBill) then
		insert full_log_data_unoerp select 
        NULL,
        REMESA,
        COD_EXPORTADOR_SISCAFE,
        CODIGO_EXPORTADOR,
        NOMBRE_EXPORTADOR,
        LOTE,
        CANTIDAD_OPERATIVA,
        TIPO_SACOS,
        MODALIDAD,
        PLACA_VEHICULO,
        FECHA_OPERACION,
        UNIDAD_CAFE,
        UNIDAD_FACTURA,
        NUM_MICRO_LOTES,
        OIE,
        CONTENEDOR,
        ISOCTN,
        TIPO_PAPEL,
        CAPAS_ADICIONALES_KRAFT_20,
        CAPAS_ADICIONALES_CARTON_20,
        CAPAS_ADICIONALES_KRAFT_40,
        CAPAS_ADICIONALES_CARTON_40,
        ESTADO,
        MUESTRA,
        EMPRESA_SUMINISTRO,
        TIPO_ENVIO,
        TIPO_DOC,
        CO_DOC,
        CO_MOV,
		CCOSTO_MOV,
        UND_ITEM,
        SUCURSAL,
        BODEGA,
        TIPO_MUESTRA,
        IP_CONEXION,
        CLIENTE_UNOERP,
		cts.business_name as CLIENTE_NOMBRE_UNOERP,
        ITEM_UNOERP,
        NUM_FACTURA,
        LISTA_PRECIO,
        DESC_CONSULTA,
        DESCRIPCION_PRODUCTTO_UNOEE,
        VALOR_FACTURA_UNOEE,
        VALOR_UNIDAD,
		FECHA_CORTE,
        STATUS_TX_WS,
        FECHA_TX_WS
        from temp_log_data_unoerp temp
		inner join clients cts on cts.nit=temp.CLIENTE_UNOERP
		where (temp.CO_DOC,temp.NUM_FACTURA) NOT IN 
		(select fdata.CO_DOC, fdata.NUM_FACTURA
		from full_log_data_unoerp fdata);
	end if;
	truncate temp_log_data_unoerp;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `siscafe_smallClients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `siscafe_smallClients`()
begin
	declare totalRows int(11);
	
	set totalRows = (select count(*) from data_billing_full);
    
    if (totalRows != 0) then
		insert temp_log_data_unoerp select 
        NULL,
        REMESA,
        COD_EXPORTADOR_SISCAFE,
        CODIGO_EXPORTADOR,
        NOMBRE_EXPORTADOR,
        LOTE,
        CANTIDAD_OPERATIVA,
        TIPO_SACOS,
        MODALIDAD,
        PLACA_VEHICULO,
        FECHA_OPERACION,
        UNIDAD_CAFE,
        UNIDAD_FACTURA,
        NUM_MICRO_LOTES,
        OIE,
        CONTENEDOR,
        ISOCTN,
        TIPO_PAPEL,
        CAPAS_ADICIONALES_KRAFT_20,
        CAPAS_ADICIONALES_CARTON_20,
        CAPAS_ADICIONALES_KRAFT_40,
        CAPAS_ADICIONALES_CARTON_40,
        ESTADO,
        MUESTRA,
        EMPRESA_SUMINISTRO,
        TIPO_ENVIO,
        TIPO_DOC,
        CO_DOC,
        CO_MOV,
		CCOSTO_MOV,
        UND_ITEM,
        SUCURSAL,
        BODEGA,
        TIPO_MUESTRA,
        IP_CONEXION,
        CLIENTE_UNOERP,
		cts.business_name as CLIENTE_NOMBRE_UNOERP,
        ITEM_UNOERP,
        NUM_FACTURA,
        LISTA_PRECIO,
        DESC_CONSULTA,
        DESCRIPCION_PRODUCTTO_UNOEE,
        VALOR_FACTURA_UNOEE,
        VALOR_UNIDAD,
		FECHA_CORTE,
        STATUS_TX_WS,
        FECHA_TX_WS
        from data_billing_full temp
		inner join clients cts on cts.nit=temp.CLIENTE_UNOERP
        where cts.isBig=0 and cts.billing_manual=0 and cts.billing_dayly=1;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TRAZABILIDAD_RACAFE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `TRAZABILIDAD_RACAFE`(IN date_start date , IN date_end date)
BEGIN
SET @FECHA_INF= date_start;
SET @FECHA_SUP= date_end;



INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR RACAFE & CIA S.C.A" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','001')
WHERE rc.jetty=1 AND rc.client_id =6 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE ",ss.name_place," DEL EXPORTADOR RACAFE & CIA S.C.A") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','002')
WHERE (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) AND rc.client_id =6 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE ",ss.name_place," DEL EXPORTADOR RACAFE & CIA S.C.A") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','003')
WHERE ss.ref_departament=3 AND rc.client_id =6 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    pc.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    pc.id  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM packaging_caffee as pc
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = pc.id
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
INNER JOIN clients AS cts ON cts.id = rc.client_id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','001')
WHERE pc.jetty='BUN' AND cts.id=6 
        AND (  DATE(pc.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by pc.id,date(pc.packaging_date), pc.jetty
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    (select cls.nit from clients cls where cls.id=ivn.id_expo_pilcaf) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD EIR (INSPECTION AT THE TIME OF RECEIPT) PARA EL TERMINAL DE SPB EXPORTADOR RACAFE & CIA S.C.A") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','001')
WHERE  ivn.type_invoice= 2 AND pc.jetty='BUN' AND cts.id=6 AND
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    null AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    pc.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    pc.id  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit  AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM packaging_caffee as pc
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN view_packaging AS vp ON vp.oie = pc.id
INNER JOIN clients AS cts on cts.id=vp.expo_id1
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','003')
WHERE pc.jetty='STM' AND cts.id=6 and ifn.packaging_mode="GRANEL"
        AND (  DATE(pc.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by pc.id,date(pc.packaging_date), pc.jetty
); 





















INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR INVERPROYECTOS MAGNA S.A.S" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','001')
WHERE rc.jetty=1 AND rc.client_id =422
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE ",ss.name_place," DEL EXPORTADOR INVERPROYECTOS MAGNA S.A.S") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','002')
WHERE (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) AND rc.client_id =422
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id
ORDER BY rc.created_date ASC 
); 




INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE ",ss.name_place," DEL EXPORTADOR INVERPROYECTOS MAGNA S.A.S") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','003')
WHERE ss.ref_departament=3 AND rc.client_id =422
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 






END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `sisec_pro`
--

USE `sisec_pro`;

--
-- Dumping routines for database 'sisec_pro'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-24 15:26:10
